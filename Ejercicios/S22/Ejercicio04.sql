-- 4.1
SELECT p.NOMBRE
FROM peliculas p;

-- 4.2
SELECT p.CALIFICACIONEDAD
FROM peliculas p
GROUP BY p.CALIFICACIONEDAD;

-- 4.3
SELECT p.*
FROM peliculas p
WHERE p.CALIFICACIONEDAD IS NULL;

-- 4.4
SELECT s.*
FROM salas s
WHERE s.PELICULA IS NULL;

-- 4.5
SELECT *
FROM salas s
LEFT JOIN peliculas p
ON p.CODIGO = s.PELICULA;

-- 4.6
SELECT *
FROM peliculas p
LEFT JOIN salas s
ON p.CODIGO = s.PELICULA;

-- 4.7
SELECT p.NOMBRE
FROM peliculas p
LEFT JOIN salas s
ON p.CODIGO = s.PELICULA
WHERE s.CODIGO IS NULL;

-- 4.8
INSERT INTO peliculas VALUES (10, 'Uno, Dos, Tres', 'PG-7');

-- 4.9
UPDATE peliculas p
SET p.CALIFICACIONEDAD = 'PG-13'
WHERE p.CALIFICACIONEDAD IS NULL;

-- 4.10
DELETE s
FROM salas s
INNER JOIN peliculas p
ON s.PELICULA = p.CODIGO
WHERE p.CALIFICACIONEDAD LIKE 'G';