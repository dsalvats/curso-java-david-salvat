-- 2.1
SELECT e.APELLIDOS
FROM empleados e;

-- 2.2
SELECT e.APELLIDOS
FROM empleados e
GROUP BY e.APELLIDOS;

-- 2.3
SELECT e.*
FROM empleados e
WHERE e.APELLIDOS LIKE 'López';

-- 2.4
SELECT e.*
FROM empleados e
WHERE e.APELLIDOS LIKE 'López' OR e.APELLIDOS LIKE 'Pérez';

-- 2.5
SELECT e.*
FROM empleados e
WHERE e.DEPARTAMENTO = 14;

-- 2.6
SELECT e.*
FROM empleados e
WHERE e.DEPARTAMENTO = 37 OR e.DEPARTAMENTO = 77;

-- 2.7
SELECT e.*
FROM empleados e
WHERE e.APELLIDOS LIKE 'p%';

-- 2.8
SELECT SUM(d.PRESUPUESTO) PRESUPUESTO_TOTAL
FROM departamentos d;

-- 2.9
SELECT d.NOMBRE, COUNT(*) NUM_EMPLEADOS
FROM empleados e
INNER JOIN departamentos d
ON d.CODIGO  = e.DEPARTAMENTO
GROUP BY e.DEPARTAMENTO;

-- 2.10
SELECT e.*, d.*
FROM empleados e
INNER JOIN departamentos d
ON d.CODIGO  = e.DEPARTAMENTO;

-- 2.11
SELECT e.NOMBRE, e.APELLIDOS, d.NOMBRE, d.PRESUPUESTO
FROM empleados e
INNER JOIN departamentos d
ON d.CODIGO  = e.DEPARTAMENTO;

-- 2.12
SELECT e.NOMBRE, e.APELLIDOS
FROM empleados e
INNER JOIN departamentos d
ON d.CODIGO  = e.DEPARTAMENTO
WHERE d.PRESUPUESTO > 60000;

-- 2.13
SELECT d.NOMBRE
FROM departamentos d
WHERE d.PRESUPUESTO > (
	SELECT AVG(PRESUPUESTO)
	FROM departamentos
);

-- 2.14
SELECT d.NOMBRE
FROM departamentos d
WHERE (
	SELECT COUNT(*)
	FROM departamentos d
	INNER JOIN empleados e
	ON e.DEPARTAMENTO = d.CODIGO
) > 2;

-- 2.15
INSERT INTO departamentos VALUES (11, 'Calidad', 40000);
INSERT INTO empleados VALUES (89267109, 'Esther', 'Vázquez', 11);

-- 2.16
UPDATE departamentos d
SET d.PRESUPUESTO = (d.PRESUPUESTO * 0.9);

-- 2.17
UPDATE empleados e
SET e.DEPARTAMENTO = 14
WHERE e.DEPARTAMENTO = 77;

-- 2.18
DELETE FROM empleados e
WHERE e.DEPARTAMENTO = 14;

-- 2.19
DELETE e FROM empleados e
INNER JOIN departamentos d
ON e.DEPARTAMENTO = d.CODIGO
WHERE d.PRESUPUESTO > 60000;

-- 2.20
DELETE FROM empleados e;