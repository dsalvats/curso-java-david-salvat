package Ejercicio06;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class Ejercicio06 extends JFrame {

	private JPanel contentPane;
	private JTextField textAltura;
	private JTextField textPeso;
	private JTextField textIMC;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio06 frame = new Ejercicio06();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio06() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 383, 107);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblAlturametros = new JLabel("Altura (metros):");
		lblAlturametros.setBounds(10, 11, 98, 14);
		contentPane.add(lblAlturametros);
		
		textAltura = new JTextField();
		textAltura.addKeyListener(new KeyAdapter() {
			public void keyReleased (KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String lastChar  = "";
				
				if (text.length() == 0)
					return;
				
				if (text.length() > 1)
					lastChar = text.substring(text.length() - 1);
				else
					lastChar = text;
				
				if (!isDouble(lastChar)) {
					textField.setText(text.substring(0, text.length() -1));
				}
			}
		});
		textAltura.setBounds(105, 8, 86, 20);
		contentPane.add(textAltura);
		textAltura.setColumns(10);
		
		JLabel lblPesokg = new JLabel("Peso (kg.):");
		lblPesokg.setBounds(204, 11, 74, 14);
		contentPane.add(lblPesokg);
		
		textPeso = new JTextField();
		textPeso.addKeyListener(new KeyAdapter() {
			public void keyReleased (KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String lastChar  = "";
				
				if (text.length() == 0)
					return;
				
				if (text.length() > 1)
					lastChar = text.substring(text.length() - 1);
				else
					lastChar = text;
				
				if (!isDouble(lastChar)) {
					textField.setText(text.substring(0, text.length() -1));
				}
			}
		});
		textPeso.setBounds(268, 8, 86, 20);
		contentPane.add(textPeso);
		textPeso.setColumns(10);
		
		JLabel lblImc = new JLabel("IMC:");
		lblImc.setBounds(187, 39, 46, 14);
		contentPane.add(lblImc);
		
		textIMC = new JTextField();
		textIMC.setEditable(false);
		textIMC.setBounds(214, 36, 86, 20);
		contentPane.add(textIMC);
		textIMC.setColumns(10);
		
		JButton btnCalcularImc = new JButton("Calcular IMC");
		btnCalcularImc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Asignamos el formato deciaml que vamos a usar
				DecimalFormat df = new DecimalFormat("#.##");
				
				// Caputramos peso y altura
				double peso = Double.parseDouble(textPeso.getText());
				double altura = Double.parseDouble(textAltura.getText());
				
				// Calculamos IMC resultante
				double imc = peso / (Math.pow(altura, 2));
				
				// Mostramos el IMC resultante
				textIMC.setText(df.format(imc));
			}
		});
		btnCalcularImc.setBounds(58, 36, 119, 23);
		contentPane.add(btnCalcularImc);
	}
	
	public boolean isDouble(String s) {  
	    try {
	    	Double.parseDouble(s);
	    	return true;
	    } catch (Exception e){
			String lastChar = "";
			if (s.length() > 1)
				lastChar = s.substring(s.length() - 1);
			else
				lastChar = s;
			
			if (lastChar.equals("."))
				return true;
			
	    	return false;
	    }
	}
}
