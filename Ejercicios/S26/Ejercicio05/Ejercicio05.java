package Ejercicio05;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class Ejercicio05 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio05 frame = new Ejercicio05();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio05() {
		setTitle("Todos los eventos del rat\u00F3n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 48, 414, 202);
		contentPane.add(scrollPane);
		
				JTextArea textArea = new JTextArea();
				textArea.setEditable(false);
				scrollPane.setViewportView(textArea);
				textArea.setWrapStyleWord(true);
				textArea.addMouseWheelListener(new MouseWheelListener() {
					public void mouseWheelMoved(MouseWheelEvent event) {
						if (event.getWheelRotation() < 0)
							textArea.append("Scroll arriba\n");
						else
							textArea.append("Scroll abajo\n");
						
						textArea.setCaretPosition(textArea.getDocument().getLength());
					}
				});
				textArea.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						if (arg0.getButton() == 1)
							textArea.append("Click izquiero\n");
						if (arg0.getButton() == 2)
							textArea.append("Click ruleta\n");
						if (arg0.getButton() == 3)
							textArea.append("Click derecho\n");
						textArea.setCaretPosition(textArea.getDocument().getLength());
					}

					@Override
					public void mouseEntered(MouseEvent arg0) {
						textArea.append("Entra rat�n\n");
						textArea.setCaretPosition(textArea.getDocument().getLength());
					}

					@Override
					public void mouseExited(MouseEvent e) {
						textArea.append("Sale rat�n\n");
						textArea.setCaretPosition(textArea.getDocument().getLength());
					}
				});
				textArea.setLineWrap(true);
				


				JButton btnLimpiar = new JButton("Limpiar");
				btnLimpiar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						textArea.setText("");
					}
				});
				
				btnLimpiar.setBounds(171, 11, 89, 23);
				contentPane.add(btnLimpiar);
	}
}
