package Ejercicio03;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio03 extends JFrame {

	public int count1 = 0;
	public int count2 = 0;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio03 frame = new Ejercicio03();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio03() {
		setTitle("M\u00E1s interacci\u00F3n");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 274, 116);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bot\u00F3n 1:");
		lblNewLabel.setBounds(10, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel veces1 = new JLabel("0 veces");
		veces1.setBounds(66, 11, 56, 14);
		contentPane.add(veces1);
		
		JLabel lblBotn = new JLabel("Bot\u00F3n 2:");
		lblBotn.setBounds(143, 11, 46, 14);
		contentPane.add(lblBotn);
		
		JLabel veces2 = new JLabel("0 veces");
		veces2.setBounds(199, 11, 59, 14);
		contentPane.add(veces2);
		
		JButton button1 = new JButton("Bot\u00F3n 1");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				count1++;
				veces1.setText(count1 + " veces");				
			}
		});
		button1.setBounds(10, 36, 94, 23);
		contentPane.add(button1);
		
		JButton button2 = new JButton("Bot\u00F3n 2");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				count2++;
				veces2.setText(count2 + " veces");			
			}
		});
		button2.setBounds(143, 36, 94, 23);
		contentPane.add(button2);
	}
}
