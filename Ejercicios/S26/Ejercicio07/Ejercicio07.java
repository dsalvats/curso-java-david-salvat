package Ejercicio07;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class Ejercicio07 extends JFrame {
	
	private final static double CONV_EUR_PTAS = 166.386;
	
	private JPanel contentPane;
	private JTextField textInput;
	private JTextField textOutput; 
	private boolean conversionEurPtas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio07 frame = new Ejercicio07();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio07() {
		conversionEurPtas = true;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 117);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cantidad a convertir");
		lblNewLabel.setBounds(10, 11, 116, 14);
		contentPane.add(lblNewLabel);
		
		textInput = new JTextField();
		textInput.addKeyListener(new KeyAdapter() {
			public void keyReleased (KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String lastChar  = "";
				
				if (text.length() == 0)
					return;
				
				if (text.length() > 1)
					lastChar = text.substring(text.length() - 1);
				else
					lastChar = text;
				
				if (!isDouble(lastChar)) {
					textField.setText(text.substring(0, text.length() -1));
				}
			}
		});
		textInput.setBounds(141, 8, 100, 20);
		contentPane.add(textInput);
		textInput.setColumns(10);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(255, 11, 62, 14);
		contentPane.add(lblResultado);
		
		textOutput = new JTextField();
		textOutput.setEditable(false);
		textOutput.setBounds(338, 8, 86, 20);
		contentPane.add(textOutput);
		textOutput.setColumns(10);
		
		JButton btnConvertir = new JButton("EUR a Ptas");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Declaraci�n de variables
				double input = 0;
				double output = 0;
				// Asignamos el formato deciaml que vamos a usar
				DecimalFormat df = new DecimalFormat("#.##");
				
				// Intentamos caputar valor entrado a cambiar
				try {
					// Si el input tiene una coma como decimal 
					// Lo ponemos en punto para poder absorber el double
					String inputString = textInput.getText();
					inputString.replace(',', '.');
					input = Double.parseDouble(inputString);
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
					
				}
				
				// Comprobamos que tipo de cambio queremos hacer y calculamos
				if (conversionEurPtas)
					output = input * CONV_EUR_PTAS;
				else
					output = input / CONV_EUR_PTAS;
				
				// Mostramos el resultado del cambio
				textOutput.setText(df.format(output));
					
			}
		});
		btnConvertir.setBounds(141, 36, 100, 23);
		contentPane.add(btnConvertir);
		
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Comprobamos en que tipo de conversi�n estamos
				if (conversionEurPtas) {
					// Oponemos el tipo de conversi�n
					conversionEurPtas = false;
					// Ponemos el texto del bot�n coherentemente
					btnConvertir.setText("Ptas a EUR");
				} else {
					// Oponemos el tipo de conversi�n
					conversionEurPtas = true;
					// Ponemos el  texto del bot�n coherentemente
					btnConvertir.setText("EUR a Ptas");
				}
				
				// Asignamos el texto de input a output y viceversa
				String aux = textInput.getText();
				textInput.setText(textOutput.getText());
				textOutput.setText(aux);
			}
		});
		btnCambiar.setBounds(338, 36, 86, 23);
		contentPane.add(btnCambiar);
	}
	
	public boolean isDouble(String s) {  
	    try {
	    	Double.parseDouble(s);
	    	return true;
	    } catch (Exception e){
			String lastChar = "";
			if (s.length() > 1)
				lastChar = s.substring(s.length() - 1);
			else
				lastChar = s;
			
			if (lastChar.equals(".") || lastChar.contentEquals(","))
				return true;
			
	    	return false;
	    }
	}
}
