package Ejercicio09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JToggleButton;

import java.util.ArrayList;
import java.util.Collections;

public class Controller implements ActionListener {

	/**********************
	 * VARIABLES GLOBALES *
	 **********************/
	private int numCards;	// Guaramos el n�mero de cartas que vamos a tener en el juego
	private List<String> images;	// Guardamos todos los posibles nombre de cartas
	private int position;	// Contador para saber en que punto de las cartas nos encontramos
	private int clicks;		// Contador para saber cuantos clicks ha hecho el usuario en el intento
	private int correct;	// Contador para saber cuantas cartas lleva acertadas el usuario
	private Card[] cardsToCheck;	// Repositorio de cartas clickadas en el intento
	private ArrayList<Card> cards;	// Repositorio con todas las cartas generadas
	private Card lastCard;	// �ltima carta apretada

	
	/***************
	 * CONTROLADOR *
	 ***************/
	public Controller(int numCards) {
		this.numCards = numCards;
		this.images = new ArrayList<>();
		this.position = 0;
		this.clicks = 0;
		this.correct = 0;
		this.cardsToCheck = new Card[2];
		this.cardsToCheck[0] = null;
		this.cardsToCheck[1] = null;
		this.cards = null;
		this.lastCard = null;

		// Generamos ArrayList con las cartas que jugaremos duplicadas
		generateImages();
	}

	
	/*************
	 * LISTENERS *
	 *************/
	// Listener para cuando se pulsa alguna carta
	@Override
	public void actionPerformed(ActionEvent e) {
		// Declaracio�n e inicializaci�n de variables
		JToggleButton cardAux = (JToggleButton) e.getSource(); // Guaramos el bot�n que ha sido apretado
		Card selectedCard = null;	// Declaramos e inicializamos la carta que acabamos de seleccionar

		// Recorremos todas las cartas que tenemos en juego hasta comprobar cual es nuestra carta y la guardamos en selectedCard
		for (Card card : getCards())
			if (card.getCard().equals(cardAux))
				selectedCard = card;

		// Si la carta seleccionada es la misma que la anterior en el mismo turno
		if (selectedCard.equals(lastCard)) {
			// Deseleccionamos y esperamos nuevo click
			selectedCard.getCard().setSelected(false);
			return;
		}

		// Guardamos la carta reci�n seleccionada como la �ltimas
		lastCard = selectedCard;

		// A�adimos el click que hemos hecho en el contador
		clicks++;

		// Comprobamos que debemos hacer dependiendo de los clicks
		switch (clicks) {
		// Primer click del primer intento
		case 1:
			// A�adimos la carta seleccionada a las cartas a comparar al final del intento
			cardsToCheck[0] = selectedCard;
			break;
			
		// PRIMER CLICK - Segundo si es el primer intento
		case 2:
			// Guardamos la carta seleccionada a las cartas a comparar al final del intento
			cardsToCheck[1] = selectedCard;

			// Comprobamos si las cartas son iguales y era la �ltima combinaci�n
			if (isSameCard() && correct == (numCards / 2 - 1)) {
				// Mostramos mensaje de enhorabuena
				JOptionPane.showMessageDialog(null, "Enhorabuena!");
				// Reiniciamos el juego
				restart();
			}
			
			break;
			
		// SEGUNDO CLICK - Tercero si es el primer intento
		case 3:
			// Comprobamos si las cartas a comprobar son iguales
			// Si las cartas son iguales
			if (isSameCard()) {
				// Deshabilitamos esas cartas y sumamos un acierto
				disableCards();
				correct++;
				
			// En el caso contrario
			} else {
				// Giramos las cartas jugadas en el intento de nuevo
				unselectCards();
			}

			// Aprovechamos el mismo 3r click como una nueva selecci�n y guardamos la carta para comprobar
			cardsToCheck[0] = selectedCard;
			// Quitamos la segunda carta que teniamos para comprobar a la espera de un segundo click en el intento
			cardsToCheck[1] = null;
			// Dejamos los clicks del intento en 1
			clicks = 1;
			
			break;
			
		}

	}

	
	/***********
	 * M�TODOS *
	 ***********/
	// Comprueba si las cartas que tenemos para comprobar son iguales
	private boolean isSameCard() {
		// Si las cartas son iguales devolvemos true
		if (cardsToCheck[0].getNumCard().equals(cardsToCheck[1].getNumCard()))
			return true;
		
		// Si las cartas son diferentes devolvemos false
		return false;
	}

	// Gira las cartas que estan para comprobar
	private void unselectCards() {
		for (Card card : cardsToCheck) {
			card.getCard().setSelected(true);
		}
	}

	// Deshabilita las cartas que estan para comprobar
	private void disableCards() {
		for (Card card : cardsToCheck) {
			card.getCard().setEnabled(false);
		}
	}

	// Llena el array list de cartas
	private void generateImages() {
		// Declaraci�n de variables
		String[] numbers = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" }; // Almacenamos todos los n�meros posibles
		String[] symbols = { "C", "D", "H", "S" };	// Almacenamos todos los s�mbolos posibles
		List<String> imagesAux = new ArrayList<>();	// Inicializamos un array en la que generaremos todas las cartas posibles

		// Recorremos todos los numeros y s�mbolos a la vez
		for (String number : numbers) {
			for (String palo : symbols) {
				// Y vamos a�adiendo el resultado a nuestra arraylist de toda la baraja
				imagesAux.add(number + palo);
			}
		}

		// Mezclamos todas las cartas de la baraja a fin de tener aleatoriedad en las cartas escogidas
		Collections.shuffle(imagesAux);
		// Seleccionamos las primeras cartas que tenemos dependiendo de cuantas cartas hay para jugar en el juego
		imagesAux = imagesAux.subList(0, getNumCards() / 2);
		// Duplicamos las cartas en nuestro array list
		imagesAux.addAll(imagesAux);
		// Mezclamos por ultima vez para que no se nos dividan en dos grupos los resultados
		Collections.shuffle(imagesAux);

		// Almazenamos nuestro mazo de cartas con el que jugaremos
		setImages(imagesAux);
	}

	// Devuelve a la vez que incrementa la posici�n en la que se encuentra generando cartas para el juego
	public String getRandomImage() {
		String number = getImages().get(getPosition());
		
		setPosition(getPosition() + 1);

		return number;
	}

	// Reinicia el programa
	// Source "https://gist.github.com/Arvik/aae7cacd34b2d3ce483a"
	public void restart() {
		try {
			StringBuilder cmd = new StringBuilder();
			
			cmd.append(System.getProperty("java.home") + File.separator + "bin" + File.separator + "java ");
			
			for (String jvmArg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
				cmd.append(jvmArg + " ");
			}
			
			cmd.append("-cp ").append(ManagementFactory.getRuntimeMXBean().getClassPath()).append(" ");
			cmd.append(Ejercicio09.class.getName()).append(" ");
			
			Runtime.getRuntime().exec(cmd.toString());
		} catch (Exception e) {
			System.out.println("Error reiniciando apliaci�n");
		}
		
		Runtime.getRuntime().exit(1);
	}
	

	/*********************
	 * GETTERS & SETTERS *
	 *********************/

	/**
	 * @return the numCards
	 */
	public int getNumCards() {
		return numCards;
	}

	/**
	 * @param numCards the numCards to set
	 */
	public void setNumCards(int numCards) {
		this.numCards = numCards;
	}

	/**
	 * @return the images
	 */
	public List<String> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(List<String> images) {
		this.images = images;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * @return the clicks
	 */
	public int getClicks() {
		return clicks;
	}

	/**
	 * @param clicks the clicks to set
	 */
	public void setClicks(int clicks) {
		this.clicks = clicks;
	}

	/**
	 * @return the correct
	 */
	public int getCorrect() {
		return correct;
	}

	/**
	 * @param correct the correct to set
	 */
	public void setCorrect(int correct) {
		this.correct = correct;
	}

	/**
	 * @return the cardsToCheck
	 */
	public Card[] getCardsToCheck() {
		return cardsToCheck;
	}

	/**
	 * @param cardsToCheck the cardsToCheck to set
	 */
	public void setCardsToCheck(Card[] cardsToCheck) {
		this.cardsToCheck = cardsToCheck;
	}

	/**
	 * @return the cards
	 */
	public ArrayList<Card> getCards() {
		return cards;
	}

	/**
	 * @param cards the cards to set
	 */
	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}

}
