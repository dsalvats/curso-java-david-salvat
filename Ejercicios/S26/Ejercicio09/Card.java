package Ejercicio09;

import java.awt.Image;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

public class Card {

	/**********************
	 * VARIABLES GLOBALES *
	 **********************/
	private int x;	// Posici�n horizontal en que va a estar la carta
	private int y;	// Posici�n vertical en que va a estar la carta
	private int cardWidth;	// Anchura que va a tener la carta
	private int cardHeight;	// Altura que va a tener la carta
	private int name;	// Posici�n absoluta que le vamos a dar a nuestra carta para darle nombre
	private ImageIcon backImage; // Imagen que va a tener de reverso la carta
	private ImageIcon image; // Imagen de la carta que va a tener
	private Controller controller; // Controlador para controlador para a�adir eventos
	private JToggleButton card; // Bot�n que va a tener la carta
	private String numCard; // N�mero y s�mbolo de nuestra carta

	/***************
	 * CONSTRUCTOR *
	 ***************/
	public Card(int x, int y, int cardWidth, int cardHeight, int name, Controller controller, ImageIcon backImage) {
		// A�adimos posici�n horizontal de carta
		setX(x);
		// A�adimos posici�n vertical de carta
		setY(y);
		// A�adimos anchura de carta
		setCardWidth(cardWidth);
		// A�adimos altura de carta
		setCardHeight(cardHeight);
		// A�adimos n�mero y s�mbolo de nuestra carta generados aleatoriamente
		setNumCard(controller.getRandomImage());
		// Con nuestra carta generada aleatoriamente conseguimos su imagen
	    fillImage();
	    // Asignamos reverso de imagen
		setBackImage(backImage);
		// Asignamos controlador
		setController(controller);
		// Inicializamos el bot�n
		setCard(null);
		// A�adimos n�mero de carta para poder crear su nombre posteriormente 
		setName(name);
		
		// Creamos carta asignada aleatoriamente
		createCard();
	}

	
	/***********
	 * M�TODOS *
	 ***********/
	// Crea el bot�n de carta con toda la informaci�n registrada
	private void createCard() {
		// Crea bot�n nuevo con su nombre
		JToggleButton card = new JToggleButton("card_" + getName(), true);
		// Posiciona el bot�n en x e y; le da una anchura y altura
		card.setBounds(getX(), getY(), getCardWidth(), getCardHeight());
		// Deja el texto del bot�n vac�o
		card.setText("");
		// A�ade imagen de reverso de la imagen
		card.setSelectedIcon(getBackImage());
		// A�ade imagen de la carta
		card.setIcon(getImage());
		// Deja la carta seleccionada para que se nos muestre el reverso
		card.setSelected(true);
		// A�ade listener de eventos con nuestro controlador pasado
		card.addActionListener(getController());

		// A�ade el bot�n a nuestro objeto carta
		setCard(card);
	}

	// Consigue la imagen de la carta generada aleatoriamente
	public void fillImage() {
		// Declaraci�n de variables
		ImageIcon image = null; // Cremaos un icono para nuestra imagen de carta

		// Intentamos conseguir la imagen desde nuestros assets y la redimensionamos a nuestro placer
		try {
			String rootDir = System.getProperty("user.dir");
			String path = rootDir + "\\src\\Ejercicio09\\assets\\deck\\" + getNumCard() + ".png";
			image = new ImageIcon(ImageIO
					.read(new File(path))
					.getScaledInstance(getCardWidth(), getCardHeight(), Image.SCALE_DEFAULT));
			
		// Si hay alg�n error mandamos mensaje y cerramos programa
		} catch (Exception e) {
			System.out.println("Error cr�tico: " + e.getMessage());
			Runtime.getRuntime().exit(1);
		}

		// A�adismo nuestra imagen a nuestro objeto carta
		setImage(image);
	}

	
	/*********************
	 * GETTERS & SETTERS *
	 *********************/

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the cardWidth
	 */
	public int getCardWidth() {
		return cardWidth;
	}

	/**
	 * @param cardWidth the cardWidth to set
	 */
	public void setCardWidth(int cardWidth) {
		this.cardWidth = cardWidth;
	}

	/**
	 * @return the cardHeight
	 */
	public int getCardHeight() {
		return cardHeight;
	}

	/**
	 * @param cardHeight the cardHeight to set
	 */
	public void setCardHeight(int cardHeight) {
		this.cardHeight = cardHeight;
	}

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * @return the card
	 */
	public JToggleButton getCard() {
		return card;
	}

	/**
	 * @param card the card to set
	 */
	public void setCard(JToggleButton card) {
		this.card = card;
	}

	/**
	 * @return the image
	 */
	public ImageIcon getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(ImageIcon image) {
		this.image = image;
	}

	/**
	 * @return the backImage
	 */
	public ImageIcon getBackImage() {
		return backImage;
	}

	/**
	 * @param backImage the backImage to set
	 */
	public void setBackImage(ImageIcon backImage) {
		this.backImage = backImage;
	}

	/**
	 * @return the numCard
	 */
	public String getNumCard() {
		return numCard;
	}

	/**
	 * @param numCard the numCard to set
	 */
	public void setNumCard(String numCard) {
		this.numCard = numCard;
	}

	/**
	 * @return the name
	 */
	public int getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(int name) {
		this.name = name;
	}

}
