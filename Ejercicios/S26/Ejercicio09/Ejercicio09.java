/**
 * 
 */
package Ejercicio09;

import javax.swing.JOptionPane;

/**
 * @author David Salvat
 *
 */
public class Ejercicio09 {
	
	/**********************
	 * VARIABLES GLOBALES *
	 **********************/
	private static int numFilas;
	private static int numColumnas;

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos variables
		numFilas = 0; // Inicializamos las filas que va a tener nuestro juego
		numColumnas = 0; // Inicializamos las columnas que va a tener nuestro juego
		
		// Pedimos el numero de filas y columnas que tendr� el juego
		askDimension();
		
		// Creamos escena con sus columnas y filas
		SceneBuilder scene = new SceneBuilder(numColumnas, numFilas);
		scene.setVisible(true);

	}
	
	
	/***********
	 * M�TODOS *
	 ***********/
	// Pide el n�mero de filas y columnas que tendr� el juego
	private static void askDimension() {
		// Declaraci�n de variables
		boolean correct = false;	// Nos dar� la informaci�n de si la informaci�n introducida por el usuario es correcta

		// Mientras la informaci�n entrada por el usuario sea incorrecta vamos pidiendo la informaci�n
		do {
			try {
				// Pedimos el n�mero de columnas que va a tener nuestro juego
				numColumnas = Integer.parseInt(JOptionPane.showInputDialog("Cuantas columnas?"));
				// Pedimos el n�mero de filas que va a tener nuestro juego
				numFilas = Integer.parseInt(JOptionPane.showInputDialog("Cu�ntas filas?"));
				
				// Si filas es mayor de 0
				// Si columnas es mayor de 0
				// Si el producto de filas y columnas es menor que 52
				// Si el producto de filas y columnas es par
				// Dejamos pasar
				if (numFilas * numColumnas <= 52 && numFilas > 0 && numColumnas > 0 && (numFilas * numColumnas) % 2 == 0)
					// Comprobamos precalculando si nos van a caber todas columnas y filas entradas
					if (SceneBuilder.checkSize(numColumnas, numFilas))
						// Aceptamos informaci�n entrada por el usuario
						correct = true;
				
					// Si no caben todas las cartas enviamos mensaje de error
					else
						JOptionPane.showMessageDialog(null,
								"El n�mero de filas o columnas no dejar� que se muestren todas las cartas.");
				
				// Si los checkeos no han sido satisfactorio mandamos mensaje de error
				else
					JOptionPane.showMessageDialog(null,
							"Parece que no has introducido un n�mero total de cartas v�lido (" + numFilas * numColumnas
									+ ").\n" + "Debes introducir un total de cartas entre 2 y 52 y que sean pares.");
			
			// Si el usuario ha introducido alg�n valor inesperado por el teclado se lo hacemos saber
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Debes introducir valores num�ricos");
			}
		} while (!correct);
	}
	
	
	/*********************
	 * GETTERS & SETTERS *
	 *********************/
	/**
	 * @return the numFilas
	 */
	public static int getNumFilas() {
		return numFilas;
	}


	/**
	 * @param numFilas the numFilas to set
	 */
	public static void setNumFilas(int numFilas) {
		Ejercicio09.numFilas = numFilas;
	}


	/**
	 * @return the numColumnas
	 */
	public static int getNumColumnas() {
		return numColumnas;
	}


	/**
	 * @param numColumnas the numColumnas to set
	 */
	public static void setNumColumnas(int numColumnas) {
		Ejercicio09.numColumnas = numColumnas;
	}
	

}
