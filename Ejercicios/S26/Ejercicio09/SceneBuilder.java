package Ejercicio09;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SceneBuilder extends JFrame {

	/***********************
	 * CONSTANTES GLOBALES *
	 ***********************/
	private static final long serialVersionUID = 1L;	// Serial Version UID
	private final static int CARD_HEIGHT = 150;	// Alto de carta
	private final static int CARD_WIDTH = 100;	// Ancho de carta
	private final static int OFFSET_X = 16;	// Distancia horizontal que tenemos que aplicar a la ventana para que se nos cuadre bien
	private final static int OFFSET_Y = 39; // Distancia vertical que tenemos que aplicar a la ventana para que se nos cuadre bien
	private final static int MARGIN_X = 20;	// Distancia horizontal entre cartas
	private final static int MARGIN_Y = 20;	// Distancia vertical entre cartas

	
	/**********************
	 * VARIABLES GLOBALES *
	 **********************/
	private Controller controller;
	private JPanel contentPane;
	private ArrayList<Card> cards;
	private int numColumnas;
	private int numFilas;

	
	/**********************
	 *     CONSTRUCTOR	  *
	 * @param numColumnas *
	 * @param numFilas    *
	 **********************/
	public SceneBuilder(int numColumnas, int numFilas) {
		// Asiganamos par�metros de constructor a variables globales
		this.numColumnas = numColumnas;
		this.numFilas = numFilas;

		// Inicializamos el controlador que usaremos durante todo el programa
		controller = new Controller(numColumnas * numFilas);
		// Inicializamos el arraylist de nuestras cartas que van a estar en juego
		cards = new ArrayList<>();

		// Llamamos a nuestro controlador de la escena
		sceneHandler();
	}

	
	/***********
	 * M�TODOS *
	 ***********/
	// Controla los aspectos basicos de la creaci�n de escena
	public void sceneHandler() {
		// Inicializamos la ventana principal de nuestra escena
		initializeWindow();
		// A�adimos el panel a esa ventana
		addPanel();
		// A�adimos todas las cartas creadas previamente a nuestro panel
		createDeck();
		// Pasamos las cartas a nuestro controlador
		controller.setCards(getCards());
	}

	// A�ade un panel a nuestra ventana previamente generada
	public void addPanel() {
		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);
	}

	// Crea una ventana teniendo en cuenta las dimensiones que va a necesitar esta
	public void initializeWindow() {
		// Calcula el ancho de ventana que necesitaremos
		int windowWidth = numColumnas * CARD_WIDTH + OFFSET_X + MARGIN_X * (numColumnas + 1);
		// Calcula el alto de ventana que necesitaremos
		int windowHeight = numFilas * CARD_HEIGHT + OFFSET_Y + MARGIN_Y * (numFilas + 1);

		// Consigue la informaci�n de las dimensiones de nuestra resoluci�n de pantalla
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// Calculamos por resoluci�n el punto x en que deber� situarse nuestra nueva ventana
		int screenWidth = (int) (screenSize.getWidth() - windowWidth) / 2;
		// Calculamos por resoluci�n el punto y en que deber� situarse nuestra nueva ventana
		int screenHeight = (int) (screenSize.getHeight() - windowHeight) / 2;

		// A�adimos un t�tulo a nuestra ventana
		setTitle("Memory");

		// Aplicamos las posici�nes y dimensiones calculadas previamente a la ventana
		setBounds(screenWidth, screenHeight, windowWidth, windowHeight);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	// Damos paso a crar nuestro mazo para jugar
	public void createDeck() {
		// Declaraci�n de variables
		int sumX = MARGIN_X;	// Empezamos la posici�n x con el margen ya establecido entre carta y carta
		int sumY = MARGIN_Y;	// Empezamos la posici�n y con el margen ya establecido entre carta y carta
		int count = 1;			// Contador de cuantas cartas llevamos creadas para darle nombre a estas
		
		// Recorremos en horizontal y vertical para ir creando nuestras cartas
		for (int i = 0; i < numFilas; i++) {
			for (int j = 0; j < numColumnas; j++) {
				// Creamos nuestra carta en una posici�n determinada
				createCard(count, sumX, sumY);

				// A�adimos el ancho de carta y margen para la posici�n de la sigueinte carta
				sumX += CARD_WIDTH + MARGIN_X;
				// Sumamos una carta en nuestro contador
				count++;
			}

			// Reiniciamos la posici�n x de la siguiente carta pero teniendo en cuenta nuestra el margen
			sumX = MARGIN_X;
			// Sumamos el alto de carta y el margen vertical para la siguiente fila de cartas
			sumY += CARD_HEIGHT + MARGIN_Y;
		}

	}

	// Crea una carta con un nombre y en una posici�n x e y determinadas
	public void createCard(int num, int x, int y) {
		// Creamos un proceso para cada carta que vayamos a crear para que podamos iniciar el programa m�s r�pido
		Thread t1 = new Thread() {
			public void run() {
				// Instanciamos y cremos la carta en cuesti�n
				Card card = new Card(x, y, CARD_WIDTH, CARD_HEIGHT, num, getController(), getBackImage());
				// A�adimos la carta a nuestro panel
				contentPane.add(card.getCard());
				// A�adios la carta a nuestro array list de cartas
				cards.add(card);
				// Como estamos trabajando con subprocesos necesitamos ir regenerando la vista
				revalidate();
				repaint();
			}
		};
		// Iniciamos el proceso
		t1.start();
		// Cuando este proceso ya se ha ejecutado lo detenemos ya que no necesitamos que este corriendo todo el tiempo
		t1.interrupt();
	}

	// Consegue la imagen del reverso de nuestra baraja
	public ImageIcon getBackImage() {
		// Declaraci�n de variables
		ImageIcon image = null;	// Crea un icono para poder mostrarse en nuestro bot�n

		// Intentamos conseguir la imagen de nuestra carpeta y la redimensionamos a nuestras medidas establecidas
		try {
			String rootDir = System.getProperty("user.dir");
			String path = rootDir + "\\src\\Ejercicio09\\assets\\deck\\blank.png";
			image = new ImageIcon(
					ImageIO.read(new File(path))
							.getScaledInstance(CARD_WIDTH, CARD_HEIGHT, Image.SCALE_DEFAULT));
		// Si ha habido algun error al conseguir la imagen imprimimos mensaje de error y cerramos juego 
		} catch (Exception e) {
			System.out.println("Error cr�tico: " + e.getMessage());
			JOptionPane.showMessageDialog(null, "No se pudo obtener el reverso de la baraja.");
			Runtime.getRuntime().exit(1);
		}

		// Devolvemos la imagen conseguida
		return image;
	}

	// Comprueba que nuestra scartas quepan vertical y horizontalmente
	public static boolean checkSize(int numColumnas, int numFilas) {
		int windowWidth = numColumnas * CARD_WIDTH + OFFSET_X + MARGIN_X * (numColumnas + 1); // Calculamos el ancho de ventana que vamos a tener
		int windowHeight = numFilas * CARD_HEIGHT + OFFSET_Y + MARGIN_Y * (numFilas + 1); // Calculamos el alto de ventana que vamos a tener

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();	// Obtenemos las dimensiones de nuestra resoluci�n
		int screenWidth = (int) screenSize.getWidth();	// Obtenemos el ancho de resoluci�n
		int screenHeight = (int) screenSize.getHeight(); // Obtenemos el alto de resoluci�n

		// Si el alto o ancho de nuestra ventana excede en alg�n punto nuestra resoluci�n devolvemos falso
		if (windowWidth > screenWidth || windowHeight > screenHeight)
			return false;

		// Si todo es correcto devolvemos que esta todo correcto
		return true;
	}

	
	/*********************
	 * GETTERS & SETTERS *
	 *********************/

	/**
	 * @return the controller
	 */
	public Controller getController() {
		return controller;
	}

	/**
	 * @param controller the controller to set
	 */
	public void setController(Controller controller) {
		this.controller = controller;
	}

	/**
	 * @return the cards
	 */
	public ArrayList<Card> getCards() {
		return cards;
	}

	/**
	 * @param cards the cards to set
	 */
	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}

	/**
	 * @return the numColumnas
	 */
	public int getNumColumnas() {
		return numColumnas;
	}

	/**
	 * @param numColumnas the numColumnas to set
	 */
	public void setNumColumnas(int numColumnas) {
		this.numColumnas = numColumnas;
	}

	/**
	 * @return the numFilas
	 */
	public int getNumFilas() {
		return numFilas;
	}

	/**
	 * @param numFilas the numFilas to set
	 */
	public void setNumFilas(int numFilas) {
		this.numFilas = numFilas;
	}

}
