package Ejercicio02;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio01 extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio01 frame = new Ejercicio01();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio01() {
		setTitle("Interacci\u00F3n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 365, 80);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel texto = new JLabel("Has pulsado:");
		texto.setBounds(10, 14, 145, 14);
		contentPane.add(texto);
		
		JButton boton1 = new JButton("Bot\u00F3n 1");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				texto.setText("Has pulsado: " + boton1.getText());
			}
		});
		boton1.setBounds(139, 11, 89, 23);
		contentPane.add(boton1);
		
		JButton boton2 = new JButton("Bot\u00F3n 2");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				texto.setText("Has pulsado: " + boton2.getText());
			}
		});
		boton2.setBounds(249, 11, 89, 23);
		contentPane.add(boton2);
	}

}
