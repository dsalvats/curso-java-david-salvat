package Ejercicio04;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;

public class Ejercicio04 extends JFrame {
	
	public String string;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio04 frame = new Ejercicio04();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio04() {
		string = "Texto inicial\n";
		
		setTitle("Ventana oyente");
		string += "T�tulo ventana a�adido\n";
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 436, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		string += "JPanel a�adido";
				
		JLabel lblEventos = new JLabel("Eventos");
		lblEventos.setBounds(10, 117, 46, 14);
		contentPane.add(lblEventos);
		string += "label a�adido\n";
		
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setBounds(60, 11, 350, 239);
		contentPane.add(textArea);
		string += "text area a�adido\n";

		setVisible(true);
		string += "ventana visible\n";
		
		textArea.setText(string);
		
	}
}
