CREATE DATABASE academia;

USE academia;

CREATE TABLE profesores (
	nombre VARCHAR(100) NOT NULL,
	apellido1 VARCHAR(100) NOT NULL,
	apellido2 VARCHAR(100) NOT NULL,
	dni VARCHAR(8) NOT NULL,
	direccion VARCHAR(255),
	titulo VARCHAR(100),
	gana INT NOT NULL,
	PRIMARY KEY (dni),
	UNIQUE(nombre, apellido1, apellido2)
) ENGINE=InnoDB;

CREATE TABLE cursos (
	nombre_curso VARCHAR(100) NOT NULL,
	cod_curso INT NOT NULL,
	dni_profesor VARCHAR(8),
	maximo_alumnos INT,
	fecha_inicio DATE NOT NULL,
	fecha_fin DATE NOT NULL,
	num_horas INT NOT NULL,
	PRIMARY KEY (cod_curso),
	FOREIGN KEY (dni_profesor)
	REFERENCES profesores(dni)
	ON DELETE SET NULL
	ON UPDATE NO ACTION,
	UNIQUE(nombre_curso),
	CONSTRAINT chk_fecha_fin 
	CHECK (fecha_fin > fecha_inicio)
) ENGINE=InnoDB;

CREATE TABLE alumnos (
	nombre VARCHAR(100) NOT NULL,
	apellido1 VARCHAR(100) NOT NULL,
	apellido2 VARCHAR(100) NOT NULL,
	dni VARCHAR(8) NOT NULL,
	direccion VARCHAR(255),
	sexo CHAR(1),
	fecha_nacimiento datetime,
	curso INT NOT NULL,
	PRIMARY KEY (dni),
	FOREIGN KEY (curso)
	REFERENCES cursos(cod_curso)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	CONSTRAINT chk_sexo 
	CHECK (sexo IN ('H', 'M'))
) ENGINE=InnoDB;

INSERT INTO profesores VALUES
	('Martí', 'Folguera', 'Font', '12345678', NULL, 'Ingeniería Informática', 24000),
	('Carlos', 'Sánchez', 'Hernández', '23456789', NULL, 'Graduado en ADE', 23000);
	
INSERT INTO cursos VALUES
	('DAM', 1, '12345678', 20, '2019-09-10', '2020-05-20', 870),
	('Marketing Internacional', 2, '23456789', 30, '2019-09-10', '2020-05-22', 880);
	
INSERT INTO alumnos VALUES
	('Juan', 'Manchón', 'Fernández', '34567890', NULL, 'H', '1998-04-12', 1),
	('Miriam', 'Díaz', 'Marín', '45678901', NULL, 'M', '1994-10-02', 2);