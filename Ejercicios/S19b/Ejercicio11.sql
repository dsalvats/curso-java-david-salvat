CREATE DATABASE investigadores;

USE investigadores;

CREATE TABLE facultad (
	codigo INT NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE investigadores (
	dni VARCHAR(8) NOT NULL,
	nom_apels VARCHAR(255) NOT NULL,
	facultad INT,
	PRIMARY KEY (dni),
	FOREIGN KEY (facultad)
	REFERENCES facultad(codigo)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE equipos (
	num_serie CHAR(4) NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	facultad INT,
	PRIMARY KEY (num_serie),
	FOREIGN KEY (facultad)
	REFERENCES facultad(codigo)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE reserva (
	dni VARCHAR(8) NOT NULL,
	num_serie CHAR(4) NOT NULL,
	comienzo datetime NOT NULL,
	fin datetime,
	PRIMARY KEY (dni, num_serie),
	FOREIGN KEY (dni)
	REFERENCES investigadores(dni)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (num_serie)
	REFERENCES equipos(num_serie)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO facultad VALUES
	(1, 'Matemáticas'),
	(2, 'Telecomunicaciones');
	
INSERT INTO investigadores VALUES
	('12345678', 'Susana Díaz Manchón', 1),
	('23456789', 'Carmen Sánchez Hernández', 1),
	('34567890', 'Luís García Fernández', 2);
	
INSERT INTO equipos VALUES
	(1, 'Desarrollo', 1),
	(2, 'Investigación', 1),
	(3, 'Desarrollo', 2);
	
INSERT INTO reserva VALUES
	('12345678', 1, '2019-05-12 09:00:00', '2019-05-12 11:00:00'),
	('23456789', 2, '2019-05-12 12:00:00', '2019-05-12 17:00:00'),
	('34567890', 3, '2019-05-14 14:00:00', '2019-05-14 19:00:00');