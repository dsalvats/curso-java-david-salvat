CREATE DATABASE tienda_informatica;

USE tienda_informatica;

CREATE TABLE fabricantes (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE articulos (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100),
	precio INT,
	fabricante INT,
	PRIMARY KEY (codigo),
	FOREIGN KEY (fabricante)
	REFERENCES fabricantes(codigo)
	ON DELETE CASCADE
	ON UPDATE CASCADE
) ENGINE=InnoDB;

INSERT INTO fabricantes VALUES
	(1, 'BOSCH'),
	(2, 'FAGOR');
	
INSERT INTO articulos VALUES
	(1, 'Nevera', 350, 1),
	(2, 'Campana', 200, 2),
	(3, 'Vitroceramica', 185, 2);