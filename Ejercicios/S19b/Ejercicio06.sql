CREATE DATABASE peliculas_salas;

USE peliculas_salas;

CREATE TABLE peliculas (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	calificacion_edad INT,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE salas (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	pelicula INT,
	PRIMARY KEY (codigo),
	FOREIGN KEY (pelicula)
	REFERENCES peliculas(codigo)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO peliculas (nombre, calificacion_edad) VALUES
	('Deadpool 2', 18),
	('La Sirenita', 0);
	
INSERT INTO salas (nombre, pelicula) VALUES
	('SALA 1', 1),
	('SALA 2', 2),
	('SALA 3', 2);