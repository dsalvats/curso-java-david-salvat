CREATE DATABASE biblio;

USE biblio;

CREATE TABLE editorial (
	claveeditorial SMALLINT NOT NULL,
	nombre VARCHAR(60),
	direccion VARCHAR(60),
	telefono VARCHAR(15),
	PRIMARY KEY (claveeditorial)
) ENGINE=InnoDB;

CREATE TABLE libro(
	clavelibro INT NOT NULL,
	titulo VARCHAR(60),
	idioma VARCHAR(15),
	formato VARCHAR(15),
	claveeditorial SMALLINT,
	PRIMARY KEY (clavelibro),
	KEY (claveeditorial),
	FOREIGN KEY (claveeditorial)
	REFERENCES editorial (claveeditorial)
	ON DELETE SET NULL
	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE tema (
	clavetema SMALLINT NOT NULL,
	nombre VARCHAR(40),
	PRIMARY KEY (clavetema)
) ENGINE=InnoDB;

CREATE TABLE autor (
	claveautor INT NOT NULL,
	nombre VARCHAR(60),
	PRIMARY KEY (claveautor)
) ENGINE=InnoDB;

CREATE TABLE ejemplar (
	claveejemplar INT NOT NULL,
	clavelibro INT NOT NULL,
	numeroorden SMALLINT NOT NULL,
	edicion SMALLINT,
	ubicacion VARCHAR(15),
	categoria CHAR,
	PRIMARY KEY (claveejemplar),
	FOREIGN KEY	(clavelibro)
	REFERENCES libro(clavelibro)
	ON DELETE CASCADE
	ON UPDATE CASCADE
) Engine=InnoDB;

CREATE TABLE socio (
	clavesocio INT NOT NULL,
	nombre VARCHAR(60),
	direccion VARCHAR(60),
	telefono VARCHAR(15),
	categoria CHAR,
	PRIMARY KEY (clavesocio)
) ENGINE=InnoDB;

CREATE TABLE prestamo (
	clavesocio INT,
	claveejemplar INT,
	numeroorden SMALLINT,
	fecha_prestamo DATE NOT NULL,
	fecha_devolucion DATE DEFAULT NULL,
	notas BLOB,
	FOREIGN KEY (clavesocio)
	REFERENCES socio(clavesocio)
	ON DELETE SET NULL
	ON UPDATE CASCADE,
	FOREIGN KEY(claveejemplar)
	REFERENCES ejemplar(claveejemplar)
	ON DELETE SET NULL
	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE trata_sobre (
	clavelibro INT NOT NULL,
	clavetema SMALLINT NOT NULL,
	FOREIGN KEY (clavelibro)
	REFERENCES libro(clavelibro)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	FOREIGN KEY (clavetema)
	REFERENCES tema(clavetema)
	ON DELETE CASCADE
	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE escrito_por (
	clavelibro INT NOT NULL,
	claveautor INT NOT NULL,
	FOREIGN KEY (clavelibro)
	REFERENCES libro(clavelibro)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	FOREIGN KEY (claveautor)
	REFERENCES autor(claveautor)
	ON DELETE CASCADE
	ON UPDATE CASCADE
) ENGINE=InnoDB;

INSERT INTO editorial VALUES
	(1, 'Grupo Planeta', 'Barcelona Passeig de Gracia 1', '34 934 123 123'),
	(2, 'Randome House Mondadori', 'Madrid Gran Via 1', '34 901 890 890'),
	(3, 'Grupo Urano', 'Reus Carrer Ample 12', '34 977 321 321'),
	(4, 'Grupo Anaya', 'Tarragona Carrer Sant Francesc 8', '34 977 111 222'),
	(5, 'Roca Editorial', 'Guadalajar Plaza Mayor 3', '34 916 549 871');
	
INSERT INTO libro VALUES
	(1, 'La conquista de América contada para escépticos', 'Español', 'papel', 1),
	(2, 'La mujer del pelo rojo', 'Español', 'papel', 2),
	(3, 'El mejor lugar del mundo es aquí mismo', 'Español', 'papel', 3),
	(4, 'Faycán Memorias de un perro vagabundo', 'Español', 'papel', 4),
	(5, 'El eco de la piel', 'Español', 'papel', 5);
	
INSERT INTO tema VALUES
	(1, 'Novela contemporánea'),
	(2, 'Novela histórica'),
	(3, 'Novela literaria'),
	(4, 'Novela negra'),
	(5, 'Novela romántica'),
	(6, 'Poesía'),
	(7, 'Teatro'),
	(8, 'Novela erótica'),
	(9, 'Regalos Día de la Madre'),
	(10, 'Ciencia ficción'),
	(11, 'Cómic y manga'),
	(12, 'Fantasía'),
	(13, 'Infantil'),
	(14, 'Juvenil'),
	(15, 'Para padres'),
	(16, 'Actualidad'),
	(17, 'Economía'),
	(18, 'Empresa'),
	(19, 'Arte'),
	(20, 'Ciencia'),
	(21, 'Ciencias humanas y sociales'),
	(22, 'Diccionarios y referencia'),
	(23, 'Esoterismo'),
	(24, 'Filosofía'),
	(25, 'Historia'),
	(26, 'Psicología'),
	(27, 'Religión'),
	(28, 'Cocina'),
	(29, 'Autoayuda'),
	(30, 'Estilo de vida'),
	(31, 'Humor'),
	(32, 'Ocio y entretenimiento'),
	(33, 'Viajes'),
	(34, 'Agendas'),
	(35, 'Calendarios');

INSERT INTO autor VALUES
	(1, 'Juan Eslava Galán'),
	(2, 'Orhan Pamuk'),
	(3, 'Francesc Miralles'),
	(4, 'Care Santos Torres'),
	(5, 'Víctor Doreste'),
	(6, 'Elia Barceló');
	
INSERT INTO ejemplar (claveejemplar, clavelibro, numeroorden, edicion) VALUES
	(1, 1, 1, 5),
	(2, 1, 2, 5),
	(3, 2, 1, 3),
	(4, 2, 2, 4),
	(5, 3, 1, 2),
	(6, 3, 2, 2),
	(7, 4, 1, 1),
	(8, 4, 2, 1),
	(9, 5, 1, 1),
	(10, 5, 2, 1);
	
INSERT INTO socio VALUES
	(1, 'Luís Alcántara', 'Reus Passeig Prim 2 1º 4ª', '34 621 211 122', 'S'),
	(2, 'Sofia Tello', 'Tarragona Rambla Nova 7 3º 2ª', '34 654 121 444', 'A');
	
INSERT INTO prestamo VALUES
	(1, 2, 1, '2019-05-12', NULL, NULL),
	(2, 4, 1, '2019-05-02', '2019-05-13', 'devuelto en mal estado');
	
INSERT INTO trata_sobre VALUES
	(1, 12),
	(1, 11),
	(2, 24),
	(3, 2),
	(4, 7),
	(4, 18),
	(5, 30);
	
INSERT INTO escrito_por VALUES
	(1, 1),
	(2, 2),
	(2, 3),
	(3, 4),
	(4, 5),
	(5, 6);