CREATE DATABASE almacenes;

USE almacenes;

CREATE TABLE almacenes (
	codigo INT NOT NULL AUTO_INCREMENT,
	lugar VARCHAR(100),
	capacidad INT,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE cajas (
	num_referencia CHAR(5) NOT NULL,
	contenido VARCHAR(100),
	valor INT,
	almacen INT,
	PRIMARY KEY (num_referencia),
	FOREIGN KEY (almacen)
	REFERENCES almacenes(codigo)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO almacenes (lugar, capacidad) VALUES
	('Barcelona', 30000),
	('Madrid', 20000);
	
INSERT INTO cajas VALUES
	(1, 'Boligrafos', 200, 1),
	(2, 'Rotuladores', 320, 1),
	(3, 'Ratones', 1200, 2),
	(4, 'Alfombrillas', 900, 2); 