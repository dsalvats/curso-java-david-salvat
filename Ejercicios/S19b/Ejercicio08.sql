CREATE DATABASE piezas_proveedores;

USE piezas_proveedores;

CREATE TABLE piezas (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100),
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE proveedores (
	id CHAR(4) NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	PRIMARY KEY(id)
) ENGINE=InnoDB;

CREATE TABLE suministra (
	codigo_pieza INT NOT NULL,
	id_proveedor CHAR(4) NOT NULL,
	precio INT NOT NULL,
	PRIMARY KEY (codigo_pieza, id_proveedor),
	FOREIGN KEY (codigo_pieza)
	REFERENCES piezas(codigo)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (id_proveedor)
	REFERENCES proveedores(id)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO piezas (nombre) VALUES
	('pomo'),
	('tabla de madera'),
	('visagra');
	
INSERT INTO proveedores VALUES
	(1, 'Pomos Sánchez SL'),
	(2, 'Maderas Pascual SA'),
	(3, 'Visagras Vicente');
	
INSERT INTO suministra VALUES
	(1, 1, 200),
	(2, 2, 300),
	(3, 3, 400);