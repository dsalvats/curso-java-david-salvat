CREATE DATABASE directores;

USE directores;

CREATE TABLE despachos (
	numero INT NOT NULL,
	capacidad INT,
	PRIMARY KEY (numero)
) ENGINE=InnoDB;

CREATE TABLE directores (
	dni VARCHAR(8) NOT NULL,
	nom_apels VARCHAR(255) NOT NULL,
	dni_jefe VARCHAR(8),
	despacho INT,
	PRIMARY KEY (dni),
	FOREIGN KEY (dni_jefe)
	REFERENCES directores(dni)
	ON DELETE SET NULL
	ON UPDATE NO ACTION,
	FOREIGN KEY (despacho)
	REFERENCES despachos(numero)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO despachos VALUES
	(1, 10),
	(2, 5),
	(3, 1);
	
INSERT INTO directores VALUES
	('12345678', 'Miriam Sánchez Fernández', NULL, 3),
	('23456789', 'Pablo Sacristán Hernández', '12345678', 2),
	('34567890', 'Jose Luís Font González', '23456789', 1),
	('45678901', 'Susana García Vaquero', '23456789', 1);