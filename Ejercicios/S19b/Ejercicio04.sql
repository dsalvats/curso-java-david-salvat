CREATE DATABASE empleados;

USE empleados;

CREATE TABLE departamentos (
	codigo INT NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	presupuesto INT,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE empleados (
	dni VARCHAR(8) NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	apellidos VARCHAR(255) NOT NULL,
	departamento INT,
	PRIMARY KEY (dni),
	FOREIGN KEY (departamento)
	REFERENCES departamentos(codigo)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO departamentos VALUES
	(1, 'Ventas', 10000),
	(2, 'RRHH', 3000);
	
INSERT INTO empleados VALUES
	('12345678', 'Juan', 'Asensio Maldonado', 1),
	('23456789', 'Lucía', 'Pérez de Juan', 1),
	('34567890', 'Anselmo', 'Fernández Marín', 2);