CREATE DATABASE grandes_almacenes;

USE grandes_almacenes;

CREATE TABLE cajeros (
	codigo INT NOT NULL AUTO_INCREMENT,
	nom_apels VARCHAR(255) NOT NULL,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE productos (
	codigo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	precio INT NOT NULL,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE maquinas_registradoras (
	codigo INT NOT NULL AUTO_INCREMENT,
	piso INT NOT NULL,
	PRIMARY KEY (codigo)
) ENGINE=InnoDB;

CREATE TABLE venta (
	cajero INT NOT NULL,
	maquina INT NOT NULL,
	producto INT NOT NULL,
	PRIMARY KEY (cajero, maquina, producto),
	FOREIGN KEY (cajero)
	REFERENCES cajeros(codigo)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (maquina)
	REFERENCES maquinas_registradoras(codigo)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (producto)
	REFERENCES productos(codigo)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO cajeros (nom_apels) VALUES
	('Susana Díaz Fernández'),
	('Juan Sánchez Calvo'),
	('Luís Miranda Hernández');
	
INSERT INTO maquinas_registradoras (piso) VALUES (1), (2), (3);

INSERT INTO productos (nombre, precio) VALUES
	('Bolígrafo', 60),
	('Rotulador', 120), 
	('Borrador', 400);
	
INSERT INTO venta VALUES
	(1, 1, 1),
	(1, 1, 2),
	(2, 2, 2),
	(2, 2, 3),
	(3, 3, 2),
	(3, 3, 3);