CREATE DATABASE cientificos;

USE cientificos;

CREATE TABLE cientificos (
	dni VARCHAR(8) NOT NULL,
	nom_apels VARCHAR(255) NOT NULL,
	PRIMARY KEY (dni)
) ENGINE=InnoDB;

CREATE TABLE proyecto (
	id CHAR(4) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	horas INT,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE asignado_a (
	cientifico VARCHAR(8) NOT NULL,
	proyecto CHAR(4) NOT NULL,
	PRIMARY KEY (cientifico, proyecto),
	FOREIGN KEY (cientifico)
	REFERENCES cientificos (dni)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (proyecto)
	REFERENCES proyecto(id)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO cientificos VALUES
	('12345678', 'Miriam Díaz García'),
	('23456789', 'Luís Cáceres Fernández'),
	('34567890', 'Carmen Hernández Cimas');
	
INSERT INTO proyecto VALUES 
	(1, 'Desarrollo CRISPR', 300),
	(2, 'Genoma Humano', 2000);

INSERT INTO asignado_a VALUES
	('12345678', 1),
	('23456789', 1),
	('23456789', 2),
	('34567890', 2);