CREATE DATABASE meteo;

USE meteo;

CREATE TABLE estacion (
	identificador MEDIUMINT UNSIGNED NOT NULL,
	latitud VARCHAR(14) NOT NULL, 
	longitud VARCHAR(15) NOT NULL,
	altitud MEDIUMINT NOT NULL,
	PRIMARY KEY (identificador)
) ENGINE=InnoDB;

CREATE TABLE muestra (
	identificadorestacion MEDIUMINT UNSIGNED NOT NULL,
	fecha DATE NOT NULL,
	temperaturaminima TINYINT,
	temperaturamaxima TINYINT,
	precipitaciones SMALLINT UNSIGNED,
	humedadminima TINYINT UNSIGNED,
	humedadmaxima TINYINT UNSIGNED,
	velocidadminima SMALLINT UNSIGNED,
	velocidadmaxima SMALLINT UNSIGNED,
	KEY (identificadorestacion),
	FOREIGN KEY (identificadorestacion)
	REFERENCES estacion(identificador)
	ON DELETE NO ACTION
	ON UPDATE CASCADE
) ENGINE=InnoDB;

INSERT INTO estacion (latitud, longitud, altitud) VALUES 
	('40°25′13″N', '3°42′21″O', 320),	# Madrid
	('41°23′20″N', '02°09′32″E', 100),	# Barcelona
	('38°20′43″N', '00°28′53″W', 80),	# Alacant
	('41°58′59″N', '02°49′30″E', 480);	# Girona
	
INSERT INTO muestra VALUES
	(1, '2019-04-01 00:00:00', 10, 22, 0, 25, 40, 20, 45),
	(1, '2019-05-01 00:00:00', 14, 20, 20, 80, 100, 10, 25),
	(2, '2019-04-01 00:00:00', 12, 20, 0, 75, 90, 40, 65),
	(2, '2019-05-01 00:00:00', 18, 25, 0, 78, 92, 30, 40),
	(3, '2019-04-01 00:00:00', 8, 17, 0, 80, 85, 40, 65),
	(3, '2019-05-01 00:00:00', 14, 23, 0, 70, 90, 50, 85),
	(4, '2019-04-01 00:00:00', 10, 22, 0, 65, 80, 10, 35),
	(4, '2019-05-01 00:00:00', 16, 26, 40, 80, 100, 10, 65);
