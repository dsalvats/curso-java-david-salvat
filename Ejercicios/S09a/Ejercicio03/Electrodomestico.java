package Ejercicio03;

public class Electrodomestico {

	// CONSTANTES
	private final static double DEFAULT_PRECIO_BASE = 100;
	private final static String DEFAULT_COLOR = "BLANCO";
	private final static char DEFAULT_CONSUMO_ENERGETICO = 'F';
	private final static int DEFAULT_PESO = 5;

	private final static char[] POSIBLES_CONSUMO_ENERGETICO = {'A', 'B', 'C', 'D', 'E', 'F'};
	private final static String[] POSIBLES_COLOR = {"BLANCO", "NEGRO", "ROJO", "AZUL", "GRIS"};
	
	// ATRIBUTOS
	private double precioBase;
	private String color;
	private char consumoEnergetico;
	private int peso;
	
	// CONSTRUCTORES
	public Electrodomestico(double precioBase, String color, char consumoEnergetico, int peso) {
		this.precioBase = precioBase;
		setColor(color);
		setConsumoEnergetico(consumoEnergetico);
		this.peso = peso;
	}
	
	public Electrodomestico(double precioBase, int peso) {
		this(precioBase, DEFAULT_COLOR, DEFAULT_CONSUMO_ENERGETICO, peso);
	}
	
	public Electrodomestico() {
		this(DEFAULT_PRECIO_BASE, DEFAULT_PESO);
	}

	// Devuelve una string para dar formato al objecto
	public String toString() {
		String string = "";
		
		string += "Precio base: " + getPrecioBase() + "€\n";
		string += "Color: " + getColor() + "\n";
		string += "Consumo energético: " + getConsumoEnergetico() + "\n";
		string += "Peso: " + getPeso() + "Kg\n";
		
		return string;
	}
	
	/**
	 * @return the precioBase
	 */
	public double getPrecioBase() {
		return precioBase;
	}

	/**
	 * @param precioBase the precioBase to set
	 */
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		boolean exists = false;
		color = color.toUpperCase();
		
		for (int i = 0; i < POSIBLES_COLOR.length; i++) {
			if (POSIBLES_COLOR[i] == color) {
				exists = true;
				break;
			}
		}
		
		if (exists)
			this.color = color;
		else {
			System.out.println(color + " no es un color correcto. Se ha cambiado por " + DEFAULT_COLOR);
			this.color = DEFAULT_COLOR;
		}
	}

	/**
	 * @return the consumoEnergetico
	 */
	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	/**
	 * @param consumoEnergetico the consumoEnergetico to set
	 */
	public void setConsumoEnergetico(char consumoEnergetico) {
		boolean exists = false;
		for (int i = 0; i < POSIBLES_CONSUMO_ENERGETICO.length; i++) {
			if (POSIBLES_CONSUMO_ENERGETICO[i] == consumoEnergetico) {
				exists = true;
				break;
			}
		}
		
		if (exists)
			this.consumoEnergetico = consumoEnergetico;
		else {
			System.out.println(consumoEnergetico + " no es un tipo de consumo energético correcto. Se ha cambiado por " + DEFAULT_CONSUMO_ENERGETICO);
			this.consumoEnergetico = DEFAULT_CONSUMO_ENERGETICO;
		}
	}

	/**
	 * @return the peso
	 */
	public int getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}
	
	
	
	
}
