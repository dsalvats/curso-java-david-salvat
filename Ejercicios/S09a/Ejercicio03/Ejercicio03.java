/**
 * 
 */
package Ejercicio03;

/* 3) Crearemos una clase llamada Electrodomestico con las siguientes características:
 * 	• Sus atributos son precio base, color, consumo energético (letras entre A y F) ypeso.
 * 	Indica que se podrán heredar.
 * 	• Por defecto, el color sera blanco, el consumo energético sera F, el precioBase es de 100
 * 	€ y el peso de 5 kg. Usa constantes para ello.
 * 	• Los colores disponibles son blanco, negro, rojo, azul y gris. No importa si el nombre esta
 *	en mayúsculas o en minúsculas.
 * 	• Los constructores que se implementaran serán
 * 		o Un constructor por defecto.
 * 		o Un constructor con el precio y peso. El resto por defecto.
 * 		o Un constructor con todos los atributos.

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		Electrodomestico elec1 = new Electrodomestico();
		Electrodomestico elec2 = new Electrodomestico(200.0, 10);
		Electrodomestico elec3 = new Electrodomestico(300.0, "Purpurina", 'Z', 15);

		// Imprimimos la información de  todos los electrodomésticos
		System.out.println("Electrodoméstico 1\n" + elec1.toString()
				+ "_____________________________\n");
		System.out.println("Electrodoméstico 2\n" + elec2.toString()
				+ "_____________________________\n");
		System.out.println("Electrodoméstico 3\n" + elec3.toString()
				+ "_____________________________\n");
	}

}
