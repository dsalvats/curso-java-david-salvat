/**
 * 
 */
package Ejercicio01;



/**
 * @author Programacion
 *
 */
public class Persona {
	
	// CONSTANTES GLOBALES
	private final static char DEFAULT_GENDER = 'H';
	
	// ATRIBUTOS
	private String nombre;
	private int edad;
	private String dni;
	private char sexo;
	private int peso;
	private int altura;
	
	// CONSTRUCTORES
	public Persona (String dni) {
		this ("", 0, dni, DEFAULT_GENDER);
	}
	
	public Persona (String nombre, int edad, String dni, char sexo) {
		this (nombre, edad, dni, sexo, 0, 0);
	}
	
	public Persona (String nombre, int edad, String dni, char sexo, int peso, int altura) {
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		setSexo(sexo);
		this.peso = peso;
		this.altura = altura;
	}

	public String toString() {
		String string = "";
		
		string += "Nombre: " + getNombre() + "\n";
		string += "Edad: " + getEdad() + " a�os\n";
		string += "DNI: " + getDni() + "\n";
		string += "Sexo: " + getSexo() + "\n";
		string += "Peso: " + getPeso() + "Kg.\n";
		string += "Altura: " + getAltura() + "cm.\n";
		
		return string;
	}
	
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the sexo
	 */
	public char getSexo() {
		return sexo;
	}

	/**
	 * @param sexo the sexo to set
	 */
	public void setSexo(char sexo) {
		if (sexo == 'H' || sexo == 'M')
			this.sexo = sexo;
		else {
			this.sexo = DEFAULT_GENDER;
			System.out.println(sexo + " no es correcto. Se ha introducido " + DEFAULT_GENDER + " por defecto.");
		}
	}

	/**
	 * @return the peso
	 */
	public int getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}

	/**
	 * @return the altura
	 */
	public int getAltura() {
		return altura;
	}

	/**
	 * @param altura the altura to set
	 */
	public void setAltura(int altura) {
		this.altura = altura;
	}
	
	
	
}
