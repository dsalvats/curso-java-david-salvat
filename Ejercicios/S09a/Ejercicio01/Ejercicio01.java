/**
 * 
 */
package Ejercicio01;

/**
 * @author David Salvat Sedano
 *
 */

/* 1) Haz una clase llamada Persona que siga las siguientes condiciones:
 * 	� Sus atributos son: nombre, edad, DNI, sexo (H hombre, M mujer), peso y altura. No
 * queremos que se accedan directamente a ellos. Piensa que modificador de acceso es el
 * m�s adecuado, tambi�n su tipo. Si quieres a�adir alg�n atributo puedes hacerlo.
 * 	� Por defecto, todos los atributos menos el DNI ser�n valores por defecto seg�n su tipo (0
 * n�meros, cadena vac�a para String, etc.). Sexo sera hombre por defecto, usa una
 * constante para ello.
 * 	� Se implantaran varios constructores:
 * 		o Un constructor por defecto.
 * 		o Un constructor con el nombre, edad y sexo, el resto por defecto.
 * 		o Un constructor con todos los atributos como par�metro.
 */

public class Ejercicio01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		Persona persona1 = new Persona("12345678A");
		Persona persona2 = new Persona("Romeo", 30, "23456789B", 'J');
		Persona persona3 = new Persona("Julieta", 28, "34567890C", 'M', 58, 162);
		
		// Imprimimos personas
		System.out.println("Persona 1\n" + persona1.toString()
		+ "__________________________\n");
		System.out.println("Persona 2\n" + persona2.toString()
		+ "__________________________\n");
		System.out.println("Persona 3\n" + persona3.toString()
		+ "__________________________\n");
	}

}
