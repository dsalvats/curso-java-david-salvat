/**
 * 
 */
package Ejercicio04;

/* 4) Crearemos una clase llamada Serie con las siguientes caracter�sticas:
 *	� Sus atributos son titulo, numero de temporadas, entregado, genero y creador.
 *	� Por defecto, el numero de temporadas es de 3 temporadas y entregado false. El resto
 *	de atributos ser�n valores por defecto seg�n el tipo del atributo.
 *	� Los constructores que se implementaran ser�n:
 *		o Un constructor por defecto.
 * 		o Un constructor con el titulo y creador. El resto por defecto.
 * 		o Un constructor con todos los atributos, excepto de entregado.
 */

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		Serie defaultSerie = new Serie();
		Serie juegoDeTronos = new Serie("Juego de tronos", "HBO");
		Serie hospitalCentral = new Serie("Hospital Central", 14, "MEDICOS", "Telecinco");
		
		// Asignamos que 
		juegoDeTronos.setEntregado(true);
		
		// Imprimimos las series
		System.out.println("Serie 1\n" + defaultSerie.toString()
		+ "________________________________\n");
		System.out.println("Serie 2\n" + juegoDeTronos.toString()
		+ "________________________________\n");
		System.out.println("Serie 3\n" + hospitalCentral.toString()
		+ "________________________________\n");

	}

}
