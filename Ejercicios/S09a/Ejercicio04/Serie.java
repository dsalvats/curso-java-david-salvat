package Ejercicio04;

public class Serie {

	private final static boolean DEFAULT_ENTREGADO = false;
	private final static int DEFAULT_NUMERO_TEMPORADAS = 3;
	
	// ATRIBUTOS
	private String titulo;
	private int numeroTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	// CONSTRUCTORES
	public Serie(String titulo, int numeroTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.numeroTemporadas = numeroTemporadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = genero;
		this.creador = creador;
	}
	
	public Serie(String titulo, String creador) {
		this(titulo, DEFAULT_NUMERO_TEMPORADAS, "", creador);
	}
	
	public Serie() {
		this("", "");
	}

	// Devolvemos una String pasando la informaci�n de la serie
	public String toString() {
		String string = "";
		
		string += "T�tulo: " + getTitulo() + "\n";
		string += "N�mero de temporadas: " + getNumeroTemporadas() + "\n";
		
		if (entregado)
			string += "Entregado: S�\n";
		else
			string += "Entregado: No\n";
		
		string += "G�nero: " + getGenero() + "\n";
		string += "Creador:  " + getCreador() + "\n";
		
		return string;
	}
	
	// SETTERS & GETTERS
	
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * @return the numeroTemporadas
	 */
	public int getNumeroTemporadas() {
		return numeroTemporadas;
	}

	/**
	 * @param numeroTemporadas the numeroTemporadas to set
	 */
	public void setNumeroTemporadas(int numeroTemporadas) {
		this.numeroTemporadas = numeroTemporadas;
	}

	/**
	 * @return the entregado
	 */
	public boolean isEntregado() {
		return entregado;
	}

	/**
	 * @param entregado the entregado to set
	 */
	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}

	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	 * @return the creador
	 */
	public String getCreador() {
		return creador;
	}

	/**
	 * @param creador the creador to set
	 */
	public void setCreador(String creador) {
		this.creador = creador;
	}
	
}
