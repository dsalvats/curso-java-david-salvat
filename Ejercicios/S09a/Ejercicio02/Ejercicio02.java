/**
 * 
 */
package Ejercicio02;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Declaración de variables
		Password pass1 = new Password();
		Password pass2 = new Password(16);
		
		// Imprimimos contraseñas generadas
		System.out.println("Contraseña 1: " + pass1.toString());
		System.out.println("Contraseña 2: " + pass2.toString());

	}

}
