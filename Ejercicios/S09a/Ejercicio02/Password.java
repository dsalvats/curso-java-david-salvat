/**
 * 
 */
package Ejercicio02;

/* 2) Haz una clase llamada Password que siga las siguientes condiciones:
 * 	� Que tenga los atributos longitud y contrase�a . Por defecto, la longitud sera de 8.
 * 	� Los constructores ser�n los siguiente:
 * 		o Un constructor por defecto.
 * 	� Un constructor con la longitud que nosotros le pasemos. Generara una contrase�a
 * aleatoria con esa longitud.
 */

/**
 * @author David Salvat Sedano
 *
 */
public class Password {

	// CONSTANTES
	private final static int DEFAULT_LENGTH = 8;
	
	// ATRIBUTOS
	private char[] password;
	private int longitud;
	
	// CONSTRUCTOR
	public Password (int longitud) {
		this.longitud = longitud;
		this.password = new char[longitud];
		
		// Llamamos al m�todo que generar� una contrase�a aleatoria
		this.generatePassword();
	}
	
	// CONSTRUCTOR DEFAULT
	public Password () {
		this(DEFAULT_LENGTH);
	}
	
	// Genera una contrase�a aleatoria con caracteres ASCII
	public void generatePassword() {
		for (int i = 0; i < getLongitud(); i++)
			this.password[i] = (char) (int) (Math.random() * (126 - 33) + 33);
	}
	
	public String toString() {
		String string = "";
		
		for (char c: getPassword())
			string += c;
		
		return string;
	}

	// GETTERS
	public char[] getPassword() {
		return password;
	}
	
	public int getLongitud() {
		return longitud;
	}

	// SETTERS
	public void setPassword(char[] password) {
		this.password = password;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	
	
}
