// Declaramos la variable meses y lo llenamos con las strings de todos los meses
var meses = [	"Enero", 
				"Febrero", 
				"Marzo", 
				"Abril", 
				"Mayo", 
				"Junio", 
				"Julio", 
				"Agosto", 
				"Septiembre", 
				"Octubre", 
				"Noviembre", 
				"Diciembre"];

// Mostramos por consola los meses
console.log(meses);