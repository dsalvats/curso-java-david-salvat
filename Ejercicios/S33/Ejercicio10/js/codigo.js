$(document).ready(function() {
	var isPalindromo = function() {
		var texto = $("#texto").val();
		var auxLength = Math.floor(texto.length/2);
		var parte1 = texto.substring(0,auxLength);
		var parte2 = texto.substring(texto.length - auxLength, texto.length);
		
		console.log(auxLength);
		console.log(parte1);
		parte2 = reverseString(parte2)
		console.log(parte2);
		
		if (parte1 === parte2)
			$("#resultado").html("Es palindromo");
		else
			$("#resultado").html("No es palindromo");
		
	};
	
	var reverseString = function(str) {
		var newString = "";
		for (var i = str.length - 1; i >= 0; i--) {
			newString += str[i];
		}
		return newString;
	}
	
	$("#boton").click(isPalindromo);
	$("#texto").change(isPalindromo);
});

