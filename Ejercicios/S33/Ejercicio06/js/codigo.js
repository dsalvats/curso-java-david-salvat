

$(document).ready(function() {
	var comprobar = function() {
		console.log("entra");
		if($("#letra").val() !== "" && $("#dni").val() !== "")
			if (calcularLetra() === $("#letra").val().toUpperCase())
				$("#resultado").html("La letra es correcta");
			else
				$("#resultado").html("La letra es incorrecta");
		else
			$("#resultado").html("Debe introducir toda la información");
	};

	var calcularLetra = function() {
		var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
		var dni = $("#dni").val();
		var letra = undefined;
		var regex = /^\d{8}$/;

		if (regex.test(dni)) {
			var pos = +dni % 23;
			letra = letras[pos];
			
			return letra;
		}
		
		return undefined;
	};
	
	$("#boton").click(comprobar);
	$("#dni").change(comprobar);
	$("#letra").change(comprobar);
});

