$(document).ready(function() {
	var calcular = function() {
		var input = parseInt($("#factorial").val());
		var resultado = factorial(input);
		
		if (resultado > 0)
			$("#resultado").html(resultado);
		else
			$("#resultado").html("El numero introducido no es correcto");
	};

	var factorial = function(num) {
		var resultado = 1;
		
		if (!(num >= 0))
			return 0;
		
		for (var i = 2;  i <= num; i++) {
			resultado *= i;
		}
		
		return resultado;
	};
	
	$("#boton").click(calcular);
	$("#factorial").change(calcular);
});

