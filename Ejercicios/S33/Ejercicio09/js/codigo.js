$(document).ready(function() {
	var comprobar = function() {
		var input = $("#frase").val();
		var result = check(input);
		var resultString = "";
		
		switch(result) {
			case -2:
				resultString = "No se ha introducido nada a comprobar";
			break;
			case -1:
				resultString = "La frase contiene numeros";
			break;
			case 0:
				resultString = "La frase contiene solo minusculas";
			break;
			case 1:
				resultString = "La frase contiene solo mayusculas";
			break;
			case 2:
				resultString = "La frase contiene mayusculas y minusculas";
			break;
			default:
				resultString = "Error inesperado";
		};
		
		$("#resultado").html(resultString);
	};

	var check = function(input) {
		var containsUpper = false;
		var containsLower = false;
		
		if (input === "")
			return -2;
		
		for (character of input) {
			console.log(character);
			if(Number.isInteger(parseInt(character)))
				return -1;
			
			if (character === character.toLowerCase())
				containsLower = true;
			
			else if (character === character.toUpperCase())
				containsUpper = true;
			
			if (containsUpper && containsLower)
				return 2;
		}
		
		if (containsLower)
			return 0;
		
		if (containsUpper)
			return 1;
		
		return undefined;
	};
	
	$("#boton").click(comprobar);
	$("#frase").change(comprobar);
});

