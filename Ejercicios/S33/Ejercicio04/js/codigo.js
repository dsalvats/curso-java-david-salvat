var valores = [	true,
				5,
				false,
				"hola",
				"adios",
				2];

// Determinamos cual de los dos elementos de texto es mayor
if (valores[3] >= valores[4])
	console.log("El texto mayor es " + valores[3]);
else if (valores[3] <= valores[4])
	console.log("El texto mayor es " +  valores[4]);
else
	console.log("Los dos textos son iguales");

// Determinamos los operadores necesarios para obtener ambos booleanos
console.log(valores[0] || valores[2]);
console.log(valores[0] && valores[2]);

// Determinamos el resultado de las 5 operaciones posibles
console.log(valores[1] + " + " + valores[5] + ": " + (valores[1] + valores[5]));
console.log(valores[1] + " - " + valores[5] + ": " + (valores[1] - valores[5]));
console.log(valores[1] + " x " + valores[5] + ": " + (valores[1] * valores[5]));
console.log(valores[1] + " / " + valores[5] + ": " + (valores[1] / valores[5]));
console.log(valores[1] + " % " + valores[5] + ": " + (valores[1] % valores[5]));