$(document).ready(function() {
	var comprobar = function() {
		var input = parseInt($("#numero").val());
		
		if (!Number.isInteger(input)) {
			$("#resultado").html("Solo se aceptan numeros.");
			
			return;
		}
		
		
		if (isEven(input))
			$("#resultado").html("El numero introducido es par.");
		else
			$("#resultado").html("El numero introducido es impar.");
	};

	var isEven = function(num) {
		if (num % 2 === 0)
			return true;
		
		return false;
	};
	
	$("#boton").click(comprobar);
	$("#numero").change(comprobar);
});

