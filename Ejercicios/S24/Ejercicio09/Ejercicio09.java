/**
 * 
 */
package Ejercicio09;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio09 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos investigadores si existe
		mysql.dropDatabase("investigadores");

		// Crea una base de datos y lo deja preparado para futura selecci�n
		mysql.createDatabase("investigadores");

		// Crea tabla facultad
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE facultad (\r\n" + 
					"	codigo INT NOT NULL,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla facultad");
			System.out.println(e.getMessage());
		}

		// Crea tabla investigadores
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE investigadores (\r\n" + 
					"	dni VARCHAR(8) NOT NULL,\r\n" + 
					"	nom_apels VARCHAR(255) NOT NULL,\r\n" + 
					"	facultad INT,\r\n" + 
					"	PRIMARY KEY (dni),\r\n" + 
					"	FOREIGN KEY (facultad)\r\n" + 
					"	REFERENCES facultad(codigo)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla investigadores");
			System.out.println(e.getMessage());
		}

		// Crea tabla equipos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE equipos (\r\n" + 
					"	num_serie CHAR(4) NOT NULL,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	facultad INT,\r\n" + 
					"	PRIMARY KEY (num_serie),\r\n" + 
					"	FOREIGN KEY (facultad)\r\n" + 
					"	REFERENCES facultad(codigo)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla equipos");
			System.out.println(e.getMessage());
		}

		// Crea tabla reserva
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE reserva (\r\n" + 
					"	dni VARCHAR(8) NOT NULL,\r\n" + 
					"	num_serie CHAR(4) NOT NULL,\r\n" + 
					"	comienzo datetime NOT NULL,\r\n" + 
					"	fin datetime,\r\n" + 
					"	PRIMARY KEY (dni, num_serie),\r\n" + 
					"	FOREIGN KEY (dni)\r\n" + 
					"	REFERENCES investigadores(dni)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (num_serie)\r\n" + 
					"	REFERENCES equipos(num_serie)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla reserva");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		Object[] facultad1 = { 1, "Matem�ticas" };
		Object[] facultad2 = { 2, "Telecomunicaciones" };

		mysql.insertData("facultad", facultad1, null);
		mysql.insertData("facultad", facultad2, null);

		// Inserta varios registros en la tabla
		Object[] investigador1 = { "12345678", "Susana D�az Manch�n", 1 };
		Object[] investigador2= { "23456789", "Carmen S�nchez Hern�ndez", 1 };
		Object[] investigador3 = { "34567890", "Lu�s Garc�a Fern�ndez", 2 };

		mysql.insertData("investigadores", investigador1, null);
		mysql.insertData("investigadores", investigador2, null);
		mysql.insertData("investigadores", investigador3, null);

		// Inserta varios registros en la tabla
		Object[] equipo1 = { 1, "Desarrollo", 1 };
		Object[] equipo2 = { 2, "Investigaci�n", 1 };
		Object[] equipo3 = { 3, "Desarrollo", 2 };

		mysql.insertData("equipos", equipo1, null);
		mysql.insertData("equipos", equipo2, null);
		mysql.insertData("equipos", equipo3, null);

		// Inserta varios registros en la tabla
		Object[] reserva1 = { "12345678", 1, "2019-05-12 09:00:00", "2019-05-12 11:00:00" };
		Object[] reserva2 = { "23456789", 2, "2019-05-12 12:00:00", "2019-05-12 17:00:00" };
		Object[] reserva3 = { "34567890", 3, "2019-05-14 14:00:00", "2019-05-14 19:00:00" };

		mysql.insertData("reserva", reserva1, null);
		mysql.insertData("reserva", reserva2, null);
		mysql.insertData("reserva", reserva3, null);

		// Imprime la informaci�n de facultad
		System.out.println("***************\n" + "* FACULTAD *\n" + "***************");
		mysql.getData("facultad");

		// Imprime la informaci�n de investigadores
		System.out.println("***************\n" + "* INVESTIGADORES *\n" + "***************");
		mysql.getData("investigadores");

		// Imprime la informaci�n de equipos
		System.out.println("*************\n" + "* EQUIPOS *\n" + "*************");
		mysql.getData("equipos");

		// Imprime la informaci�n de reserva
		System.out.println("*************\n" + "* RESERVA *\n" + "*************");
		mysql.getData("reserva");
		
	}

}
