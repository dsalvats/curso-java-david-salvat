/**
 * 
 */
package Ejercicio08;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio08 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos grandes_almacenes si existe
		mysql.dropDatabase("grandes_almacenes");

		// Crea una base de datos y lo deja preparado para futura selecci�n
		mysql.createDatabase("grandes_almacenes");

		// Crea tabla cajeros
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE cajeros (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	nom_apels VARCHAR(255) NOT NULL,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla cajeros");
			System.out.println(e.getMessage());
		}

		// Crea tabla productos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE productos (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	precio INT NOT NULL,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla productos");
			System.out.println(e.getMessage());
		}

		// Crea tabla maquinas_registradoras
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE maquinas_registradoras (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	piso INT NOT NULL,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla maquinas_registradoras");
			System.out.println(e.getMessage());
		}

		// Crea tabla venta
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE venta (\r\n" + 
					"	cajero INT NOT NULL,\r\n" + 
					"	maquina INT NOT NULL,\r\n" + 
					"	producto INT NOT NULL,\r\n" + 
					"	PRIMARY KEY (cajero, maquina, producto),\r\n" + 
					"	FOREIGN KEY (cajero)\r\n" + 
					"	REFERENCES cajeros(codigo)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (maquina)\r\n" + 
					"	REFERENCES maquinas_registradoras(codigo)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (producto)\r\n" + 
					"	REFERENCES productos(codigo)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla venta");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		String[] cajeroFields = { "nom_apels" };
		Object[] cajero1 = { "Susana D�az Fern�ndez" };
		Object[] cajero2 = { "Juan S�nchez Calvo" };
		Object[] cajero3 = { "Lu�s Miranda Hern�ndez" };

		mysql.insertData("cajeros", cajero1, cajeroFields);
		mysql.insertData("cajeros", cajero2, cajeroFields);
		mysql.insertData("cajeros", cajero3, cajeroFields);

		// Inserta varios registros en la tabla
		String[] productoFields = { "nombre", "precio" };
		Object[] producto1 = { "Bol�grafo", 60 };
		Object[] producto2 = { "Rotulador", 120 };
		Object[] producto3 = { "Borrador", 400 };

		mysql.insertData("productos", producto1, productoFields);
		mysql.insertData("productos", producto2, productoFields);
		mysql.insertData("productos", producto3, productoFields);

		// Inserta varios registros en la tabla
		String[] maquinasRegistradorasFields = { "piso" };
		Object[] maquinasregistradoras1 = { 1 };
		Object[] maquinasregistradoras2 = { 2 };
		Object[] maquinasregistradoras3 = { 3 };

		mysql.insertData("maquinas_registradoras", maquinasregistradoras1, maquinasRegistradorasFields);
		mysql.insertData("maquinas_registradoras", maquinasregistradoras2, maquinasRegistradorasFields);
		mysql.insertData("maquinas_registradoras", maquinasregistradoras3, maquinasRegistradorasFields);

		// Inserta varios registros en la tabla
		Object[] venta1 = { 1, 1, 1 };
		Object[] venta2 = { 1, 1, 2 };
		Object[] venta3 = { 2, 2, 2 };
		Object[] venta4 = { 2, 2, 3 };
		Object[] venta5 = { 3, 3, 2 };
		Object[] venta6 = { 3, 3, 3 };

		mysql.insertData("venta", venta1, null);
		mysql.insertData("venta", venta2, null);
		mysql.insertData("venta", venta3, null);
		mysql.insertData("venta", venta4, null);
		mysql.insertData("venta", venta5, null);
		mysql.insertData("venta", venta6, null);

		// Imprime la informaci�n de cajeros
		System.out.println("***************\n" + "* CAJEROS *\n" + "***************");
		mysql.getData("cajeros");

		// Imprime la informaci�n de productos
		System.out.println("***************\n" + "* PRODUCTOS *\n" + "***************");
		mysql.getData("productos");

		// Imprime la informaci�n de maquinas_registradoras
		System.out.println("*************\n" + "* MAQUINAS REGISTRADORAS *\n" + "*************");
		mysql.getData("maquinas_registradoras");

		// Imprime la informaci�n de venta
		System.out.println("*************\n" + "* VENTAS *\n" + "*************");
		mysql.getData("venta");

	}

}
