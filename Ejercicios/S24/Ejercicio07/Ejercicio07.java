/**
 * 
 */
package Ejercicio07;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio07 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos cientificos si existe
		mysql.dropDatabase("cientificos");

		// Crea una base de datos y lo deja preparado para futura selecci�n
		mysql.createDatabase("cientificos");

		// Crea tabla cientificos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE cientificos (\r\n" + 
					"	dni VARCHAR(8) NOT NULL,\r\n" + 
					"	nom_apels VARCHAR(255) NOT NULL,\r\n" + 
					"	PRIMARY KEY (dni)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla cientificos");
			System.out.println(e.getMessage());
		}

		// Crea tabla proyecto
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE proyecto (\r\n" + 
					"	id CHAR(4) NOT NULL,\r\n" + 
					"	nombre VARCHAR(255) NOT NULL,\r\n" + 
					"	horas INT,\r\n" + 
					"	PRIMARY KEY (id)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla proyecto");
			System.out.println(e.getMessage());
		}

		// Crea tabla asignado_a
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE asignado_a (\r\n" + 
					"	cientifico VARCHAR(8) NOT NULL,\r\n" + 
					"	proyecto CHAR(4) NOT NULL,\r\n" + 
					"	PRIMARY KEY (cientifico, proyecto),\r\n" + 
					"	FOREIGN KEY (cientifico)\r\n" + 
					"	REFERENCES cientificos (dni)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (proyecto)\r\n" + 
					"	REFERENCES proyecto(id)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla asignado_a");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		Object[] cientifico1 = { "12345678", "Miriam D�az Garc�a" };
		Object[] cientifico2 = { "23456789", "Lu�s C�ceres Fern�ndez" };
		Object[] cientifico3 = { "34567890", "Carmen Hern�ndez Cimas" };

		mysql.insertData("cientificos", cientifico1, null);
		mysql.insertData("cientificos", cientifico2, null);
		mysql.insertData("cientificos", cientifico3, null);

		// Inserta varios registros en la tabla
		Object[] proyecto1 = { 1, "Desarrollo CRISPR", 300 };
		Object[] proyecto2 = { 2, "Genoma Humano", 2000 };

		mysql.insertData("proyecto", proyecto1, null);
		mysql.insertData("proyecto", proyecto2, null);

		// Inserta varios registros en la tabla
		Object[] asignadoA1 = { "12345678", 1 };
		Object[] asignadoA2 = { "23456789", 1 };
		Object[] asignadoA3 = { "23456789", 2 };
		Object[] asignadoA4 = { "34567890", 2 };

		mysql.insertData("asignado_a", asignadoA1, null);
		mysql.insertData("asignado_a", asignadoA2, null);
		mysql.insertData("asignado_a", asignadoA3, null);
		mysql.insertData("asignado_a", asignadoA4, null);

		// Imprime la informaci�n de cientificos
		System.out.println("***************\n" + "* CIENTIFICOS *\n" + "***************");
		mysql.getData("cientificos");

		// Imprime la informaci�n de proyecto
		System.out.println("***************\n" + "* PROYECTO *\n" + "***************");
		mysql.getData("proyecto");

		// Imprime la informaci�n de asignado_a
		System.out.println("*************\n" + "* ASIGNADO A *\n" + "*************");
		mysql.getData("asignado_a");

	}

}
