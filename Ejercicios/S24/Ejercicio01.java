/**
 * 
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author Grupo 2 Java
 *
 */
public class Ejercicio01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MySQLHandler mysql = new MySQLHandler();
		
		try {
			// Crea una base de datos y lo deja preparado para futura selecci�n
			mysql.createDatabase("prueba_java");
			// Crea tabla en la �ltima  base de datos seleccionada
			mysql.createTable("tabla_prueba");
			
			// Inserta varios registros en la tabla
			Object[] prueba1 = { "1", "Susana", "D�az", "10", "M" };
			Object[] prueba2 = { "2", "Ram�n", "Hern�ndez", "12", "H" };
			Object[] prueba3 = { "3", "Silvia", "Fern�ndez", "14", "M" };
			Object[] prueba4 = { "4", "Juan", "S�nchez", "16", "H" };
			Object[] prueba5 = { "5", "Cristina", "Manch�n", "18", "M" };
			
			mysql.insertData("tabla_prueba", prueba1, null);
			mysql.insertData("tabla_prueba", prueba2, null);
			mysql.insertData("tabla_prueba", prueba3, null);
			mysql.insertData("tabla_prueba", prueba4, null);
			mysql.insertData("tabla_prueba", prueba5, null);
			
			// Imprime la informaci�n en tabla
			mysql.getData("tabla_prueba");
			
			// Borra registros, tabla y base de datos utilizados
			mysql.removeAllData("tabla_prueba");
			mysql.dropTable("tabla_prueba");
			mysql.dropDatabase("prueba_java");
			
			mysql.closeConnection();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

}
