/**
 * 
 */
package Ejercicio04;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio04 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos peliculas salas si existe
		mysql.dropDatabase("peliculas_salas");

		// Crea una base de datos y lo deja preparado para futura selección
		mysql.createDatabase("peliculas_salas");

		// Crea tabla peliculas
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE peliculas (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	calificacion_edad INT,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla peliculas");
			System.out.println(e.getMessage());
		}

		// Crea tabla salas
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE salas (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	pelicula INT,\r\n" + 
					"	PRIMARY KEY (codigo),\r\n" + 
					"	FOREIGN KEY (pelicula)\r\n" + 
					"	REFERENCES peliculas(codigo)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla salas");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		String[] peliculaFields = { "nombre", "calificacion_edad" };
		Object[] pelicula1 = { "Deadpool 2", 18 };
		Object[] pelicula2 = { "La Sirenita", 0 };

		mysql.insertData("peliculas", pelicula1, peliculaFields);
		mysql.insertData("peliculas", pelicula2, peliculaFields);

		// Inserta varios registros en la tabla
		String[] salaFields = { "nombre", "pelicula" };
		Object[] sala1 = { "SALA 1", 1 };
		Object[] sala2 = { "SALA 2", 2 };
		Object[] sala3 = { "SALA 3", 2 };

		mysql.insertData("salas", sala1, salaFields);
		mysql.insertData("salas", sala2, salaFields);
		mysql.insertData("salas", sala3, salaFields);

		// Imprime la información de peliculas
		System.out.println("***************\n" + "* PELICULAS *\n" + "***************");
		mysql.getData("peliculas");

		// Imprime la información de salas
		System.out.println("*************\n" + "* SALAS *\n" + "*************");
		mysql.getData("salas");

	}

}
