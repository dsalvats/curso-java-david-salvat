/**
 * 
 */
package Ejercicio03;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos almacenes si existe
		mysql.dropDatabase("almacenes");

		// Crea una base de datos y lo deja preparado para futura selección
		mysql.createDatabase("almacenes");

		// Crea tabla almacenes
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE almacenes (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	lugar VARCHAR(100),\r\n" + 
					"	capacidad INT,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla almacenes");
			System.out.println(e.getMessage());
		}

		// Crea tabla cajas
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE cajas (\r\n" + 
					"	num_referencia CHAR(5) NOT NULL,\r\n" + 
					"	contenido VARCHAR(100),\r\n" + 
					"	valor INT,\r\n" + 
					"	almacen INT,\r\n" + 
					"	PRIMARY KEY (num_referencia),\r\n" + 
					"	FOREIGN KEY (almacen)\r\n" + 
					"	REFERENCES almacenes(codigo)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla cajas");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		String[] almacenFields = { "lugar", "capacidad" };
		Object[] almacen1 = { "Barcelona", 30000 };
		Object[] almacen2 = { "Madrid", 20000 };

		mysql.insertData("almacenes", almacen1, almacenFields);
		mysql.insertData("almacenes", almacen2, almacenFields);

		// Inserta varios registros en la tabla
		Object[] caja1 = { 1, "Boligrafos", 200, 1 };
		Object[] caja2 = { 2, "Rotuladores", 320, 1 };
		Object[] caja3 = { 3, "Ratones", 1200, 2 };
		Object[] caja4 = { 4, "Alfombrillas", 900, 2 };

		mysql.insertData("cajas", caja1, null);
		mysql.insertData("cajas", caja2, null);
		mysql.insertData("cajas", caja3, null);
		mysql.insertData("cajas", caja4, null);

		// Imprime la información de almacenes
		System.out.println("***************\n" + "* ALMACENES *\n" + "***************");
		mysql.getData("almacenes");

		// Imprime la información de cajas
		System.out.println("*************\n" + "* CAJAS *\n" + "*************");
		mysql.getData("cajas");

	}

}
