/**
 * 
 */
package Ejercicio02;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos empleados si existe
		mysql.dropDatabase("empleados");

		// Crea una base de datos y lo deja preparado para futura selecci�n
		mysql.createDatabase("empleados");

		// Crea tabla departamentos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE departamentos (\r\n" + 
					"	codigo INT NOT NULL,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	presupuesto INT,\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla departamentos");
			System.out.println(e.getMessage());
		}

		// Crea tabla empleados
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE empleados (\r\n" + 
					"	dni VARCHAR(8) NOT NULL,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	apellidos VARCHAR(255) NOT NULL,\r\n" + 
					"	departamento INT,\r\n" + 
					"	PRIMARY KEY (dni),\r\n" + 
					"	FOREIGN KEY (departamento)\r\n" + 
					"	REFERENCES departamentos(codigo)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla empleados");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		Object[] departamento1 = { 1, "Ventas", 10000 };
		Object[] departamento2 = { 2, "RRHH", 3000 };

		mysql.insertData("departamentos", departamento1, null);
		mysql.insertData("departamentos", departamento2, null);

		// Inserta varios registros en la tabla
		Object[] empleado1 = { "12345678", "Juan", "Asensio Maldonado", 1 };
		Object[] empleado2 = { "23456789", "Luc�a", "P�rez de Juan", 1 };
		Object[] empleado3 = { "34567890", "Anselmo", "Fern�ndez Mar�n", 2 };

		mysql.insertData("empleados", empleado1, null);
		mysql.insertData("empleados", empleado2, null);
		mysql.insertData("empleados", empleado3, null);

		// Imprime la informaci�n de departamentos
		System.out.println("***************\n" + "* DEPARTAMENTOS *\n" + "***************");
		mysql.getData("departamentos");

		// Imprime la informaci�n de empleados
		System.out.println("*************\n" + "* EMPLEADOS *\n" + "*************");
		mysql.getData("empleados");

	}

}
