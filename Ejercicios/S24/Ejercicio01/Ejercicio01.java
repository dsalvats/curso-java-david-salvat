package Ejercicio01;
/**
 * 
 */

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author Grupo 2 Java
 *
 */
public class Ejercicio01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos tienda informatica si existe
		mysql.dropDatabase("tienda_informatica");

		// Crea una base de datos y lo deja preparado para futura selección
		mysql.createDatabase("tienda_informatica");

		// Crea tabla fabricantes
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE IF NOT EXISTS fabricantes (\r\n" 
						+ "	codigo INT NOT NULL AUTO_INCREMENT,\r\n"
						+ "	nombre VARCHAR(100),\r\n" 
						+ "	PRIMARY KEY (codigo)\r\n" 
					+ ") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla fabricantes");
			System.out.println(e.getMessage());
		}

		// Crea tabla articulos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE IF NOT EXISTS articulos (\r\n"
						+ "	codigo INT NOT NULL AUTO_INCREMENT,\r\n" 
						+ "	nombre VARCHAR(100),\r\n" 
						+ "	precio INT,\r\n"
						+ "	fabricante INT,\r\n" 
						+ "	PRIMARY KEY (codigo),\r\n" 
						+ "	FOREIGN KEY (fabricante)\r\n"
						+ "	REFERENCES fabricantes(codigo)\r\n" 
						+ "	ON DELETE CASCADE\r\n" 
						+ "	ON UPDATE CASCADE\r\n"
					+ ") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla articulos");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		String[] fabricanteFields = { "nombre" };
		Object[] fabricante1 = { "BOSCH" };
		Object[] fabricante2 = { "FAGOR" };

		mysql.insertData("fabricantes", fabricante1, fabricanteFields);
		mysql.insertData("fabricantes", fabricante2, fabricanteFields);

		// Inserta varios registros en la tabla
		String[] articuloFields = { "nombre", "precio", "fabricante" };
		Object[] articulo1 = { "Nevera", 350, 1 };
		Object[] articulo2 = { "Campana", 200, 2 };
		Object[] articulo3 = { "Vitrocerámica", 185, 2 };

		mysql.insertData("articulos", articulo1, articuloFields);
		mysql.insertData("articulos", articulo2, articuloFields);
		mysql.insertData("articulos", articulo3, articuloFields);

		// Imprime la información de fabricantes
		System.out.println("***************\n" + "* FABRICANTES *\n" + "***************");
		mysql.getData("fabricantes");

		// Imprime la información de artículos
		System.out.println("*************\n" + "* ARTICULOS *\n" + "*************");
		mysql.getData("articulos");
	}

}
