/**
 * 
 */
package Utils;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import com.mysql.cj.jdbc.result.ResultSetMetaData;
import com.mysql.cj.xdevapi.Result;

/**
 * @author David Salvat Sedano
 *
 */
public class MySQLHandler {

	// CONSTANTES
	private final static String DEFAULT_HOST = "localhost";
	private final static String DEFAULT_DB = "";
	private final static String DEFAULT_USER = "david";
	private final static String DEFAULT_PASSWD = "JAVA1";
	private final static int DEFAULT_PORT = 3306;
	
	private static Connection connection;
	private String host;
	private String database;
	private String user;
	private String passwd;
	private int port;
	
	public MySQLHandler(String host, String database, String user, String passwd, int port) {
		this.host = host;
		this.database = database;
		this.user = user;
		this.passwd = passwd;
		this.port = port;
	}
	
	public MySQLHandler() {
		this(DEFAULT_HOST, DEFAULT_DB, DEFAULT_USER, DEFAULT_PASSWD, DEFAULT_PORT);
	}
	
	// Crea base de datos
	public void createDatabase(String name) {
		try {
			executeUpdateQuery("CREATE DATABASE IF NOT EXISTS " + name + ";");
			setDatabase(name);
		} catch (SQLException e) {
			System.out.println("Error al crear la base de datos " + name);
			System.out.println(e.getMessage());
		}		
		
	}
	
	// Crea una tabla
	public void createTable(String name) {
		
		String query = "CREATE TABLE IF NOT EXISTS " + name + "("
				+ "ID VARCHAR(25),"
				+ "Nombre VARCHAR(50),"
				+ "Apellido VARCHAR(50),"
				+ "Edad VARCHAR(3),"
				+ "Sexo VARCHAR(1));";
		
		try {
			executeUpdateQuery(query);
		} catch (SQLException e) {
			System.out.println("Error al crear la tabla " + name);
		}
	}
	
	// Inserta datos a la tabla
	public void insertData(String name, Object[] values, String[] fields) {
		String query = "INSERT INTO " + name;
		String auxComma = "";
		
		if (fields != null) {
			query += "(";
			for (String field: fields) {
				query += auxComma + field;
				
				auxComma = ",";
			}
			query += ")";
		}
		
		auxComma = "";
	    query += " VALUES (";
		
		for (Object value: values) {
			if (value instanceof String) {
				query += auxComma + "'" + value + "'";
			} else {
				query += auxComma + value;
			}
			
			auxComma = ",";
		}
		
		query += ");";
		
		try {
			executeUpdateQuery(query);
		} catch (SQLException e) {
			System.out.println("Error al insertar registro en la tabla " + name);
			System.out.println(e.getMessage());
		}
	}
	
	// Consulta todos los datos de una tabla
	public void getData(String name) {
		String query = "SELECT * FROM " +  name + ";";
		String result = "";
		ResultSet rs = null;
		
		try {
			rs = executeQuery(query);
		} catch (SQLException e) {
			System.out.println("No se pueden conseguir los registros de la  tabla " + name);
			System.out.println(e.getMessage());
		}
		
		try {
			ResultSetMetaData metaData = (ResultSetMetaData) rs.getMetaData();
			
			int count = metaData.getColumnCount();
			
			ArrayList<String> columns = new ArrayList<>();
			
			for (int i = 1; i <= count; i++) {
				columns.add(metaData.getColumnName(i));
			}
			
			while (rs.next()) {
				for (int i = 0; i < count; i++)
					result += columns.get(i) + ": " + rs.getString(columns.get(i)) + "\n";
				
				result += "___________________________________\n\n";
			}
			
			System.out.println(result);

		} catch (SQLException e) {
			System.out.println("No se ha podido parsear la informaci�n de la tabla " + name);
			System.out.println(e.getMessage());
		}
			
		closeConnection();
	}
	
	// Borra todos los registros de una tabla
	public void removeAllData(String name) throws SQLException {
		String query = "DELETE FROM " + name + ";";
		
		executeUpdateQuery(query);
	}
	
	// Establece conexi�n
	private void establishConnection() {
		try {
			String jdbcString = "jdbc:mysql://" + getHost() + ":" + getPort()
			+ "/" + getDatabase() + "?useTimezone=true&serverTimezone=UTC";
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			connection = DriverManager.getConnection(jdbcString, getUser(), getPasswd());
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("No se ha podido conectar con mi base de datos");
			System.out.println(e.getMessage());
		}
	}
	
	// Drop database
	public void dropDatabase (String name) {
		String query = "DROP DATABASE IF EXISTS " + name + ";";
		
		try {
			executeUpdateQuery(query);
		} catch (SQLException e) {
			System.out.println("Error al borrar la base de datos " + name);
			System.out.println(e.getMessage());
		}
	}
	
	// Drop table
	public void dropTable (String name) {
		String query = "DROP TABLE IF EXISTS " + name + ";";
		
		try {
			executeUpdateQuery(query);
		} catch (SQLException e) {
			System.out.println("Error al borrar la tabla " + name);
			System.out.println(e.getMessage());
		}
	}
	
	// Ejecuta query
	private ResultSet executeQuery(String query) throws SQLException {
		establishConnection();
		Statement st = getConnection().createStatement();
		ResultSet resultSet = st.executeQuery(query);
		return resultSet;
	}
	
	// Ejectua update query
	public void executeUpdateQuery(String query) throws SQLException {
		establishConnection();
		Statement st = getConnection().createStatement();
		st.executeUpdate(query);
		closeConnection();
	}
	
	// Cierra  conexi�n MySQL abierta
	public void closeConnection() {
		try {
			getConnection().close();
		} catch (SQLException e) {
			System.out.println("Error al cerrar la conexi�n MySQL");
			System.out.println(e.getMessage());
		}
	}

	/*********************
	 * SETTERS & GETTERS
	 *********************/
	
	/**
	 * @return the connection
	 */
	public static Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection the connection to set
	 */
	public static void setConnection(Connection connection) {
		MySQLHandler.connection = connection;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the passwd
	 */
	public String getPasswd() {
		return passwd;
	}

	/**
	 * @param passwd the passwd to set
	 */
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the database
	 */
	public String getDatabase() {
		return database;
	}

	/**
	 * @param database the database to set
	 */
	public void setDatabase(String database) {
		this.database = database;
	}
	
	
	
	
}
