/**
 * 
 */
package Ejercicio05;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos directores existe
		mysql.dropDatabase("directores");

		// Crea una base de datos y lo deja preparado para futura selecci�n
		mysql.createDatabase("directores");

		// Crea tabla despachos
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE despachos (\r\n" + 
					"	numero INT NOT NULL,\r\n" + 
					"	capacidad INT,\r\n" + 
					"	PRIMARY KEY (numero)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla despachos");
			System.out.println(e.getMessage());
		}

		// Crea tabla directores
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE directores (\r\n" + 
					"	dni VARCHAR(8) NOT NULL,\r\n" + 
					"	nom_apels VARCHAR(255) NOT NULL,\r\n" + 
					"	dni_jefe VARCHAR(8),\r\n" + 
					"	despacho INT,\r\n" + 
					"	PRIMARY KEY (dni),\r\n" + 
					"	FOREIGN KEY (dni_jefe)\r\n" + 
					"	REFERENCES directores(dni)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (despacho)\r\n" + 
					"	REFERENCES despachos(numero)\r\n" + 
					"	ON DELETE SET NULL\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla directores");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		Object[] despacho1 = { 1, 10 };
		Object[] despacho2 = { 2, 5 };
		Object[] despacho3 = { 3, 1 };

		mysql.insertData("despachos", despacho1, null);
		mysql.insertData("despachos", despacho2, null);
		mysql.insertData("despachos", despacho3, null);

		// Inserta varios registros en la tabla
		Object[] director1 = { "12345678", "Miriam S�nchez Fern�ndez", null, 3 };
		Object[] director2 = { "23456789", "Pablo Sacrist�n Hern�ndez", "12345678", 2 };
		Object[] director3 = { "34567890", "Jose Lu�s Font Gonz�lez", "23456789", 1 };
		Object[] director4 = { "45678901", "Susana Garc�a Vaquero", "23456789", 1 };

		mysql.insertData("directores", director1, null);
		mysql.insertData("directores", director2, null);
		mysql.insertData("directores", director3, null);
		mysql.insertData("directores", director4, null);

		// Imprime la informaci�n de despachos
		System.out.println("***************\n" + "* DESPACHOS *\n" + "***************");
		mysql.getData("despachos");

		// Imprime la informaci�n de directores
		System.out.println("*************\n" + "* DIRECTORES *\n" + "*************");
		mysql.getData("directores");
		
	}

}
