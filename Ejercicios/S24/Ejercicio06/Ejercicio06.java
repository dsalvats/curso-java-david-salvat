/**
 * 
 */
package Ejercicio06;

import java.sql.SQLException;

import Utils.MySQLHandler;

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Inicializamos nuestro controlador mysql
		MySQLHandler mysql = new MySQLHandler();

		// Borramos la base de datos piezas_proveedores si existe
		mysql.dropDatabase("piezas_proveedores");

		// Crea una base de datos y lo deja preparado para futura selección
		mysql.createDatabase("piezas_proveedores");

		// Crea tabla piezas
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE piezas (\r\n" + 
					"	codigo INT NOT NULL AUTO_INCREMENT,\r\n" + 
					"	nombre VARCHAR(100),\r\n" + 
					"	PRIMARY KEY (codigo)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla piezas");
			System.out.println(e.getMessage());
		}

		// Crea tabla proveedores
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE proveedores (\r\n" + 
					"	id CHAR(4) NOT NULL,\r\n" + 
					"	nombre VARCHAR(100) NOT NULL,\r\n" + 
					"	PRIMARY KEY(id)\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla proveedores");
			System.out.println(e.getMessage());
		}

		// Crea tabla suministra
		try {
			mysql.executeUpdateQuery(
					"CREATE TABLE suministra (\r\n" + 
					"	codigo_pieza INT NOT NULL,\r\n" + 
					"	id_proveedor CHAR(4) NOT NULL,\r\n" + 
					"	precio INT NOT NULL,\r\n" + 
					"	PRIMARY KEY (codigo_pieza, id_proveedor),\r\n" + 
					"	FOREIGN KEY (codigo_pieza)\r\n" + 
					"	REFERENCES piezas(codigo)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION,\r\n" + 
					"	FOREIGN KEY (id_proveedor)\r\n" + 
					"	REFERENCES proveedores(id)\r\n" + 
					"	ON DELETE CASCADE\r\n" + 
					"	ON UPDATE NO ACTION\r\n" + 
					") ENGINE=InnoDB;");
		} catch (SQLException e) {
			System.out.println("No se ha podido crear la tabla suministra");
			System.out.println(e.getMessage());
		}

		// Inserta varios registros en la tabla
		String[] piezaFields = { "nombre" };
		Object[] pieza1 = { "pomo" };
		Object[] pieza2 = { "tabla de madera" };
		Object[] pieza3 = { "visagra" };

		mysql.insertData("piezas", pieza1, piezaFields);
		mysql.insertData("piezas", pieza2, piezaFields);
		mysql.insertData("piezas", pieza3, piezaFields);

		// Inserta varios registros en la tabla
		Object[] proveedor1 = { 1, "Pomos Sánchez SL" };
		Object[] proveedor2 = { 2, "Maderas Pascual SA" };
		Object[] proveedor3 = { 3, "Visagras Vicente" };

		mysql.insertData("proveedores", proveedor1, null);
		mysql.insertData("proveedores", proveedor2, null);
		mysql.insertData("proveedores", proveedor3, null);

		// Inserta varios registros en la tabla
		Object[] suministra1 = { 1, 1, 200 };
		Object[] suministra2 = { 2, 2, 300 };
		Object[] suministra3 = { 3, 3, 400 };

		mysql.insertData("suministra", suministra1, null);
		mysql.insertData("suministra", suministra2, null);
		mysql.insertData("suministra", suministra3, null);

		// Imprime la información de piezas
		System.out.println("***************\n" + "* PIEZAS *\n" + "***************");
		mysql.getData("piezas");

		// Imprime la información de proveedores
		System.out.println("***************\n" + "* PROVEEDORES *\n" + "***************");
		mysql.getData("proveedores");

		// Imprime la información de suministra
		System.out.println("*************\n" + "* SUMINISTRA *\n" + "*************");
		mysql.getData("suministra");

	}

}
