
$(document).ready(function() {

    /**********************
     * VARIABLES GLOBALES *
     **********************/
    let _input_ = $("#input p");
    let _output_ = $("#output p");
    let _numbers_ = $("#numeros div div ");
    let _plus_ = $("#plus");
    let _product_ = $("#product");
    let _difference_ = $("#difference");
    let _division_ = $("#division");
    let _result_ = $("#result");
    let _deleteAll_ = $("#deleteAll");
    
    let firstInput = undefined;
    let secondInput = undefined;
    let operador = undefined;

    /***********
     * MÉTODOS *
     ***********/
    // Añadimos el valor deseado al input
    let addNumber = function(element) {
        let number = element.text();
        _input_.text(_input_.text() + number);

        // Miramos si hemos pulsado algún operador para ver si
        // Calculamos en el primer input o segundo
        if (operador === undefined)
            firstInput = _input_.text();
        else
            secondInput = _input_.text();
    };

    // Añadimos la suma al input y a operador
    let plus = function() {
        if (firstInput !== undefined && operador === undefined) {
            _input_.text(_input_.text() + "+");
            operador = "+";
        }
    };

    // Añadimos la diferencia al input y a operador
    let difference = function() {
        if (firstInput !== undefined && operador === undefined) {
            _input_.text(_input_.text() + "-");
            operador = "-";
        }
    };

    // Añadimos el producto al input y a operador
    let product = function() {
        if (firstInput !== undefined && operador === undefined) {
            _input_.text(_input_.text() + "*");
            operador = "*";
        }
    };

    // Añadimos la division al input y a operador
    let division = function() {
        if (firstInput !== undefined && operador === undefined) {
            _input_.text(_input_.text() + "/");
            operador = "/";
        }
    };

    // Calculamos el resultado del input
    let result = function() {
        let resultado = undefined;

        // Si aún no hemos puesto el segundo valor no hacemos nada
        if (secondInput !== undefined) {

            // Cogemos el segundo valor real del input y lo pasamos a float para poder operar con el
            secondInput = parseFloat(secondInput.replace(firstInput + operador, ""));
            // Pasamos el primer valor del input a float para poder operar con el
            firstInput = parseFloat(firstInput);

            // Miramos que operación tenemos guarda y la realizamos guardandolo en la variable resultado
            switch (operador) {
                case "+":
                    resultado = firstInput + secondInput;
                    break;
                case "-":
                    resultado = firstInput - secondInput;
                    break;
                case "*":
                    resultado = firstInput * secondInput;
                    break;
                case "/":
                    resultado = firstInput / secondInput;
                    break;
            }

            // Añadimos el resultado al output
            _output_.text(resultado);
        }
    };

    // Reiniciamos todas las variables y los inputs
    let deleteAll = function() {
        firstInput = undefined;
        operador = undefined;
        secondInput = undefined;
        _output_.text(0);
        _input_.html("&nbsp;");
    };

    /***********
     * EVENTOS *
     ***********/
    // Detecta cuando se ha pulsado algun botón de tipo número
    _numbers_.click(function() {
        // Añadimos el número al input
        addNumber($(this));
    });

    _plus_.click(plus);
    _difference_.click(difference);
    _division_.click(division);
    _product_.click(product);
    _result_.click(result);
    _deleteAll_.click(deleteAll);

});