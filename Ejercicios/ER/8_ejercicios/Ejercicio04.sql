CREATE DATABASES IF NOT EXISTS bomberos;

USE bomberos;

CREATE TABLE IF NOT EXISTS parque_bomberos (
	cod_parque INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(255) NOT NULL,
	direccion VARCHAR(255),
	telefono VARCHAR(15),
	categoria VARCHAR(20),
	PRIMARY KEY (cod_parque)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS coche (
	num_coche INT NOT NULL AUTO_INCREMENT,
	marcha VARCHAR(40) NOT NULL,
	modelo VARCHAR(40) NOT NULL,
	num_matricula VARCHAR(9) NOT NULL,
	fecha_compra DATE,
	fecha_ult_rev DATE,
	cod_parque INT,
	PRIMARY KEY (num_coche),
	FOREIGN KEY (cod_parque)
	REFERENCES parque_bomberos(cod_parque)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS equipo (
	cod_eq INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(255) NOT NULL,
	PRIMARY KEY (cod_eq)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS peticion_servicio (
	cod_pet_serv INT NOT NULL AUTO_INCREMENT,
	tipo_serv VARCHAR(40),
	grado_urgencia TINYINT,
	cod_eq INT,
	PRIMARY KEY (cod_pet_serv),
	FOREIGN KEY (cod_eq)
	REFERENCES equipo(cod_eq)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS recibe (
	recibe_id INT NOT NULL AUTO_INCREMENT,
	fecha DATE NOT NULL,
	hora TIME NOT NULL,
	cod_parque INT NOT NULL,
	cod_eq INT NOT NULL,
	PRIMARY KEY (recibe_id),
	FOREIGN KEY (cod_parque)
	REFERENCES parque_bomberos(cod_parque)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (cod_eq)
	REFERENCES equipo(cod_eq)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS (

) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS (

) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS (

) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS (

) ENGINE=InnoDB;


Parque_Bomberos		(PK_cod_parque, nombre, direccion, telefono, categoria)
Coche				(PK_num_coche, marca, modelo, num_matricula, fecha_compra, fecha_ult_rev, FK_cod_parque)
Equipo				(PK_cod_eq, nombre)
Peticion_Servicio	(PK_cod_pet_serv, tipo_serv, grado_urgencia, FK_cod_eq)
Recibe 				(PK_recibe_id, fecha, hora, FK_cod_parque, FK_cod_pet_serv)
Bombero				(PK_cod_bom, nombre, apellidos, fecha_nac, dni, direccion, telefono, puesto, fk_cod_parque, fk_cod_eq)
Turno				(PK_cod_turno, descripcion)
Periodo				(PK_fecha_inicio, PK_fecha_fin)
Trabaja				(PK_trabaja_id, FK_cod_bom, FK_cod_turno, FK_fecha_inicio, FK_fecha_fin)