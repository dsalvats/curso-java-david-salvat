CREATE DATABASE IF NOT EXISTS bancos;

USE bancos;

CREATE TABLE IF NOT EXISTS cliente (
	dni VARCHAR(8) NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	apellidos VARCHAR(255) NOT NULL,
	direccion VARCHAR(255),
	ciudad VARCHAR(100) NOT NULL,
	PRIMARY KEY(dni)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS cuenta (
	n_cuenta VARCHAR(8) NOT NULL,
	saldo DECIMAL(10,2) NOT NULL DEFAULT 0,
	PRIMARY KEY (n_cuenta)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS transaccion (
	n_transaccion INT NOT NULL AUTO_INCREMENT,
	n_cuenta VARCHAR(8) NOT NULL,
	fecha DATE NOT NULL,
	tipo_operacion VARCHAR(20) NOT NULL,
	cantidad DECIMAL(10,2) NOT NULL,
	PRIMARY KEY (n_transaccion),
	FOREIGN KEY (n_cuenta)
	REFERENCES cuenta(n_cuenta)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS sucursal (
	n_sucursal INT NOT NULL,
	ciudad VARCHAR(100) NOT NULL,
	activo BOOLEAN NOT NULL DEFAULT TRUE,
	PRIMARY KEY (n_sucursal)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS abrir (
	abrir_id INT NOT NULL AUTO_INCREMENT,
	dni VARCHAR(8) NOT NULL,
	n_cuenta VARCHAR(8) NOT NULL,
	n_sucursal INT,
	PRIMARY KEY (abrir_id),
	FOREIGN KEY (dni)
	REFERENCES cliente(dni)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (n_cuenta)
	REFERENCES cuenta(n_cuenta)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (n_sucursal)
	REFERENCES sucursal(n_sucursal)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;
	
INSERT INTO cliente VALUES
	('12345678', 'Susana', 'Díaz García', NULL, 'Valls'),
	('23456789', 'Fernando', 'Fernández Manchón', NULL, 'Tarragona'),
	('34567890', 'Filadelfio', 'Hernández González', NULL, 'Reus');

INSERT INTO cuenta VALUES
	('36418514', 1000.0000),
	('45618746', 0.0000),
	('85643175', 2500.0000);

INSERT INTO sucursal VALUES
	(40, 'Reus', 1),
	(78, 'Tarragona', 0),
	(118, 'Valls', 1);

INSERT INTO abrir VALUES
	(1, '23456789', '36418514', 118),
	(2, '34567890', '45618746', 78),
	(3, '12345678', '85643175', 40);

INSERT INTO transaccion (n_cuenta, fecha, tipo_operacion, cantidad) VALUES
	('36418514', '2019-05-15', 'INGRESO', 100.0000),
	('45618746', '2019-05-13', 'INGRESO', 50.0000),
	('85643175', '2019-05-11', 'CAJERO', 20.0000);