CREATE DATABASE IF NOT EXISTS elementos;

USE elementos;

CREATE TABLE IF NOT EXISTS elemento (
	nombre_elemento VARCHAR(100) NOT NULL,
	peso_atomico DECIMAL(10,4) NOT NULL,
	simbolo VARCHAR(2) NOT NULL,
	n_atomico INT(2) NOT NULL,
	PRIMARY KEY(nombre_elemento)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS gaseoso (
	nombre_compuesto VARCHAR(100) NOT NULL,
	coef_expan DECIMAL(10,4) NOT NULL,
	temp_lic DECIMAL(10,4) NOT NULL,
	PRIMARY KEY(nombre_compuesto)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS liquido (
	nombre_compuesto VARCHAR(100) NOT NULL,
	densidad DECIMAL(10,4) NOT NULL,
	temp_evap DECIMAL(10,4) NOT NULL,
	PRIMARY KEY (nombre_compuesto)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS solido (
	nombre_compuesto VARCHAR(100) NOT NULL,
	color VARCHAR(40),
	olor VARCHAR(40),
	dureza DECIMAL(10,4) NOT NULL,
	PRIMARY KEY (nombre_compuesto)
) ENGINE=InnoDB;

CREATE TABLE compuesto_por (
	compuesto_por_id INT NOT NULL AUTO_INCREMENT,
	nombre_elemento VARCHAR(100) NOT NULL,
	nombre_compuesto_gaseoso VARCHAR(100),
	nombre_compuesto_liquido VARCHAR(100),
	nombre_compuesto_solido VARCHAR(100),
	proporcion INT,
	PRIMARY KEY (compuesto_por_id),
	FOREIGN KEY (nombre_elemento)
	REFERENCES elemento(nombre_elemento)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY(nombre_compuesto_gaseoso)
	REFERENCES gaseoso(nombre_compuesto)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY(nombre_compuesto_liquido)
	REFERENCES liquido(nombre_compuesto)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY(nombre_compuesto_solido)
	REFERENCES solido(nombre_compuesto)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO elemento VALUES
	('carbono', 12.0110, 'C', 6),
	('cloro', 35.4500, 'Cl', 17),
	('hidrogeno', 1.0080, 'H', 1),
	('oxigeno', 15.9990, 'O', 8),
	('plata', 107.8700, 'Ag', 47);	
	
INSERT INTO gaseoso VALUES
	('acetileno', 2.5000, -183.0000);
	
INSERT INTO liquido VALUES
	('metanol', 0.7920, 65.0000);
	
INSERT INTO solido VALUES
	('cloruro de plata', NULL, NULL, 3000.0000);
	
INSERT INTO compuesto_por (nombre_elemento, nombre_compuesto_gaseoso, nombre_compuesto_liquido, nombre_compuesto_solido, proporcion) VALUES
	('carbono', 'acetileno', NULL, NULL, 2),
	('hidrogeno', 'acetileno', NULL, NULL, 2),
	('carbono', NULL, 'metanol', NULL, 3),
	('hidrogeno', NULL, 'metanol', NULL, 6),
	('oxigeno', NULL, 'metanol', NULL, 1),
	('plata', NULL, NULL, 'cloruro de plata', 1),
	('cloro', NULL, NULL, 'cloruro de plata', 1);

	
