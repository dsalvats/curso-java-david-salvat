CREATE DATABASE carrera;

USE carrera;

CREATE TABLE atleta (
	n_dorsal INT NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	releva_a INT,
	PRIMARY KEY(n_dorsal),
	FOREIGN KEY(releva_a)
	REFERENCES atleta(n_dorsal)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

INSERT INTO atleta VALUES
	(1, 'Carlos Sánchez Marín', NULL),
	(2, 'Martín Fernández García', 1);