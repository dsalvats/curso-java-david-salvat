/*
 * aulas 				(PK_cod_aula, U1_nombre, U2_n_aula, metros)
 * hora					(PK_hora, PK_dia)
 * profesores			(PK_cod_profesor, U1_nombre, U1_apellidos, telefono, direccion)
 * ciclos				(PK_cod_ciclo, U_nombre, horas)
 * cursos 				(PK_cod_curso, FK_cod_ciclo, FK_cod_profesor, antiguedad)
 * asignaturas 			(PK_cod_asig, cod_euro, nombre, FK_requiere)
 * periodos_impartidos 	(PK_cod_asig, PK_fecha_inicio, fecha_fin, FK_cod_profesor, antiguedad)
 * ocupa 				(PK_ocupa_id,  FK_cod_aula, FK_hora, FK_dia, FK_cod_asig)
 */


CREATE DATABASE IF NOT EXISTS aulas;

USE aulas;

CREATE TABLE IF NOT EXISTS aulas (
	cod_aula INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	n_aula INT NOT NULL,
	metros INT,
	PRIMARY KEY (cod_aula),
	UNIQUE (nombre),
	UNIQUE (n_aula)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS horas (
	hora INT NOT NULL,
	dia INT NOT NULL,
	PRIMARY KEY (hora, dia)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS profesores (
	cod_profesor INT NOT NULL,
	nombre VARCHAR(100),
	apellidos VARCHAR(200),
	telefono VARCHAR(15),
	direccion VARCHAR(200),
	PRIMARY KEY(cod_profesor),
	UNIQUE(nombre, apellidos)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ciclos (
	cod_ciclo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
	horas INT NOT NULL,
	PRIMARY KEY(cod_ciclo)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS cursos (
	cod_curso INT NOT NULL AUTO_INCREMENT,
	cod_ciclo INT NOT NULL,
	cod_profesor INT,
	antiguedad INT,
	PRIMARY KEY (cod_curso),
	FOREIGN KEY (cod_ciclo)
	REFERENCES ciclos(cod_ciclo)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (cod_profesor)
	REFERENCES profesores(cod_profesor)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS asignaturas (
	cod_asignatura INT NOT NULL AUTO_INCREMENT,
	cod_euro INT NOT NULL,
	nombre VARCHAR(100) NOT NULL,
	req_asignatura INT,
	PRIMARY KEY (cod_asignatura),
	FOREIGN KEY (req_asignatura)
	REFERENCES asignaturas(cod_asignatura)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS periodos_impartidos (
	periodo_id INT NOT NULL,
	fecha_inicio DATE NOT NULL,
	fecha_fin DATE,
	cod_asignatura INT NOT NULL,
	cod_profesor INT,
	PRIMARY KEY (periodo_id, fecha_inicio),
	FOREIGN KEY (cod_asignatura)
	REFERENCES asignaturas(cod_asignatura)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (cod_profesor)
	REFERENCES profesores(cod_profesor)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ocupa (
	ocupa_id INT NOT NULL AUTO_INCREMENT,
	cod_aula INT NOT NULL,
	hora INT NOT NULL,
	dia INT NOT NULL,
	cod_asignatura INT NOT NULL,
	PRIMARY KEY (ocupa_id),
	FOREIGN KEY (cod_aula)
	REFERENCES aulas(cod_aula)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (dia, hora)
	REFERENCES horas(dia, hora)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (hora)
	REFERENCES horas(hora)
	ON DELETE CASCADE
	ON UPDATE NO ACTION,
	FOREIGN KEY (cod_asignatura)
	REFERENCES asignaturas(cod_asignatura)
	ON DELETE CASCADE
	ON UPDATE NO ACTION
) ENGINE=InnoDB;







