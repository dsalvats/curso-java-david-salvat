/**
 * 
 */
package Ejercicio4;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int n = 1;
		
		// Operaciones
		n += 77;
		n -= 3;
		n *= 2;
		
		// Imprime resultados
		System.out.println("El valor final de n es: " + n);

	}

}
