/**
 * 
 */
package Ejercicio1;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int num1, num2, suma, resta, multiplicacion;
		float division;
		
		// Asignación de valores
		num1 = 5;
		num2 = 4;
		
		// Operaciones
		suma = num1 + num2;
		resta = num1 - num2;
		multiplicacion = num1 * num2;
		division = (float)num1 / (float)num2;
		
		// Impresión de resultados
		System.out.println(num1 + " + " + num2 + " = " + suma);
		System.out.println(num1 + " - " + num2 + " = " + resta);
		System.out.println(num1 + " x " + num2 + " = " + multiplicacion);
		System.out.println(num1 + " / " + num2 + " = " + division);
	}

}
