/**
 * 
 */
package Ejercicio2;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n y asignaci�n de variables
		int n = 5;
		double a = 4.56;
		char c = 'a';
		
		// Impresi�n de resultados
		System.out.println("El valor de N es: " + n);
		System.out.println("El valor de A es: " + a);
		System.out.println("El valor de C es: " + c);
		System.out.println("La suma de N + A es: " + (n + a));
		System.out.println("La diferencia de A - n es: " + (a - n));
		System.out.println("El valor num�rico correspondiente al car�cter que contiene la variable C es: " + (int)c);
		
	}

}
