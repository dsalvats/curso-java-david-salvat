/**
 * 
 */
package Ejercicio3;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int x, y;
		double n, m;
		
		// Asignación de valores
		x = 1;
		y = 2;
		n = 3.4;
		m = 5.6;
		
		// Impresión de resultados
		System.out.println("El valor de x es: " + x);
		System.out.println("El valor de y es: " + y);
		System.out.println("El valor de n es: " + n);
		System.out.println("El valor de m es: " + m);
		System.out.println("La suma de x + y es: " + (x + y));
		System.out.println("La diferencia de x - y es: " + (x - y));
		System.out.println("El producto de x * y es: " + (x * y));
		System.out.println("El cociente de x / y es: " + (x / y));
		System.out.println("El resto de x % y es: " + (x % y));
		System.out.println("La suma de n + m es: " + (n + m));
		System.out.println("La diferencia de n - m es: " + (n - m));
		System.out.println("El producto de n * m es: " + (n * m));
		System.out.println("El cociente de n / m es: " + (n / m));
		System.out.println("El resto de n % m es: " + (n % m));
		System.out.println("El doble de x es: " + (x * 2));
		System.out.println("El doble de y es: " + (y* 2));
		System.out.println("El doble de n es: " + (n * 2));
		System.out.println("El doble de m es: " + (m * 2));
		System.out.println("La suma de todas las variables es: " + (x + y + n + m));
		System.out.println("El producto de todas las variables es: " + (x * y * n * m));

	}

}
