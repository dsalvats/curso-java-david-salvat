/**
 * 
 */
package Ejercicio5;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int a, b, c, d,  aux;
		
		// Asignación de valores
		a = 1;
		b = 2;
		c = 3;
		d = 4;
		
		// Operaciones
		aux = b;
		b = c;
		c = a;
		a = d;
		d = aux;		

	}

}
