package Ejercicio04;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.InputMethodEvent;
import javax.swing.JLabel;

public class Ejercicio04 extends JFrame {

	private JPanel contentPane;
	private JTextField field1;
	private JTextField field2;
	private JTextField result;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio04 frame = new Ejercicio04();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio04() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 242, 198);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		field1 = new JTextField();
		field1.addKeyListener(new KeyAdapter() {
			public void keyReleased (KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String lastChar  = "";
				
				if (text.length() > 1)
					lastChar = text.substring(text.length() - 1);
				else
					lastChar = text;
				
				if (!isDouble(lastChar)) {
					textField.setText(text.substring(0, text.length() -1));
				}
			}
		});
		field1.setBounds(10, 36, 52, 20);
		contentPane.add(field1);
		field1.setColumns(10);
		
		field2 = new JTextField();
		field2.addKeyListener(new KeyAdapter() {
			public void keyReleased (KeyEvent e) {
				JTextField textField = (JTextField) e.getSource();
				String text = textField.getText();
				String lastChar  = "";
				
				if (text.length() > 1)
					lastChar = text.substring(text.length() - 1);
				else
					lastChar = text;
				
				if (!isDouble(lastChar)) {
					textField.setText(text.substring(0, text.length() -1));
				}
			}
		});
		field2.setColumns(10);
		field2.setBounds(72, 36, 52, 20);
		contentPane.add(field2);
		
		result = new JTextField();
		result.setEditable(false);
		result.setColumns(10);
		result.setBounds(134, 36, 76, 20);
		contentPane.add(result);
		
		JButton operPlus = new JButton("+");
		operPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Conseguimos primer valor
				double value1 = Double.parseDouble(field1.getText());
				// Conseguimos segundo valor
				double value2 = Double.parseDouble(field2.getText());
				// Calculamos resultado
				double valueResult = value1 + value2;
				// Introducimos resultado en campo resultado
				result.setText(String.valueOf(valueResult));
			}
		});
		operPlus.setBounds(10, 67, 52, 23);
		contentPane.add(operPlus);
		
		JButton operDiff = new JButton("-");
		operDiff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Conseguimos primer valor
				double value1 = Double.parseDouble(field1.getText());
				// Conseguimos segundo valor
				double value2 = Double.parseDouble(field2.getText());
				// Calculamos resultado
				double valueResult = value1 - value2;
				// Introducimos resultado en campo resultado
				result.setText(String.valueOf(valueResult));
			}
		});
		operDiff.setBounds(72, 67, 52, 23);
		contentPane.add(operDiff);
		
		JButton operProd = new JButton("x");
		operProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Conseguimos primer valor
				double value1 = Double.parseDouble(field1.getText());
				// Conseguimos segundo valor
				double value2 = Double.parseDouble(field2.getText());
				// Calculamos resultado
				double valueResult = value1 * value2;
				// Introducimos resultado en campo resultado
				result.setText(String.valueOf(valueResult));
			}
		});
		operProd.setBounds(10, 104, 52, 23);
		contentPane.add(operProd);
		
		JButton operDiv = new JButton("/");
		operDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Conseguimos primer valor
				double value1 = Double.parseDouble(field1.getText());
				// Conseguimos segundo valor
				double value2 = Double.parseDouble(field2.getText());
				// Calculamos resultado
				double valueResult = value1 / value2;
				// Introducimos resultado en campo resultado
				result.setText(String.valueOf(valueResult));
			}
		});
		operDiv.setBounds(72, 104, 52, 23);
		contentPane.add(operDiv);
		
		JButton btnAbout = new JButton("About");
		btnAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, "Esta calculadora ha sido creada por David Salvat.");
			}
		});
		btnAbout.setBounds(134, 67, 78, 23);
		contentPane.add(btnAbout);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Runtime.getRuntime().exit(1);
			}
		});
		btnSalir.setBounds(132, 104, 78, 23);
		contentPane.add(btnSalir);
		
		JLabel lblValor = new JLabel("Valor 1");
		lblValor.setBounds(10, 11, 46, 14);
		contentPane.add(lblValor);
		
		JLabel lblValor_1 = new JLabel("Valor 2");
		lblValor_1.setBounds(72, 11, 46, 14);
		contentPane.add(lblValor_1);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(134, 11, 76, 14);
		contentPane.add(lblResultado);
	}
	
	public boolean isDouble(String s) {  
	    try {
	    	Double.parseDouble(s);
	    	return true;
	    } catch (Exception e){
			String lastChar = "";
			if (s.length() > 1)
				lastChar = s.substring(s.length() - 1);
			else
				lastChar = s;
			
			if (lastChar.equals("."))
				return true;
			
	    	return false;
	    }
	}  
}
