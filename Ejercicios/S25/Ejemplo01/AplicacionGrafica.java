/**
 * 
 */
package Ejemplo01;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;

/**
 * @author David Salvat Sedano
 *
 */
public class AplicacionGrafica extends JFrame {

	private JPanel contentPane;
	public AplicacionGrafica() {
		
		setTitle("T�tulo de la ventana");
		
		setBounds(400, 200, 500, 420);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		
		contentPane.setLayout(null);
		
		setContentPane(contentPane);
		
		// Etiqueta Hola Mundo
		JLabel etiqueta = new JLabel("�Hola mundo!");
		etiqueta.setBounds(60, 20, 100, 20);
		contentPane.add(etiqueta);
		
		// Text field
		JTextField textField = new JTextField();
		textField.setBounds(43, 67, 86, 26);
		contentPane.add(textField);
		
		// Bot�n
		JButton btnPulsame = new JButton("Pulsame");
		btnPulsame.setBounds(43, 133, 89, 23);
		contentPane.add(btnPulsame);
		
		// Radio button group
		// Radio 1
		JRadioButton rdbtnOpcion = new JRadioButton("Opcion 1", true);
		rdbtnOpcion.setBounds(43, 194, 109, 23);
		contentPane.add(rdbtnOpcion);
		
		// Radio 2
		JRadioButton rdbtnOpcion_1 = new JRadioButton("Opcion 2", false);
		rdbtnOpcion_1.setBounds(43, 220, 109, 23);
		contentPane.add(rdbtnOpcion_1);
		
		// Radio 3
		JRadioButton rdbtnOpcion_2 = new JRadioButton("Opcion 3", false);
		rdbtnOpcion_2.setBounds(43, 246, 109, 23);
		contentPane.add(rdbtnOpcion_2);
		
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(rdbtnOpcion);
		bgroup.add(rdbtnOpcion_1);
		bgroup.add(rdbtnOpcion_2);
		
		// Checkboxes
		// Checkbox 1
		JCheckBox chckbxOption = new JCheckBox("Opcion 1", true);
		chckbxOption.setBounds(43, 305, 97, 23);
		contentPane.add(chckbxOption);
		
		// Checkbox 2
		JCheckBox chckbxOption_1 = new JCheckBox("Opcion 1", true);
		chckbxOption_1.setBounds(43, 325, 97, 23);
		contentPane.add(chckbxOption_1);
		
		// Checkbox 3
		JCheckBox chckbxOption_2 = new JCheckBox("Opcion 1", true);
		chckbxOption_2.setBounds(43, 346, 97, 23);
		contentPane.add(chckbxOption_2);
		
		// Text area
		JTextArea textArea = new JTextArea("prueba");
		textArea.setBounds(189, 18, 141, 117);
		textArea.setWrapStyleWord(true);
		textArea.setLineWrap(true);
		contentPane.add(textArea);
		
		// Scroll para textarea
		JScrollPane scroll = new JScrollPane(textArea);
		scroll.setBounds(189, 18, 141, 117);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll);
		
		// Password field
		JPasswordField pwd = new JPasswordField("fernando");
		pwd.setBounds(189, 171, 139, 20);
		contentPane.add(pwd);
		
		// Combo box
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.setBounds(189, 221, 141, 22);
		contentPane.add(comboBox);
		
		comboBox.addItem("Fernando");
		comboBox.addItem("Alberto");
		comboBox.addItem("Arturo");
		
		// Interruptor
		JToggleButton tglbtnNewToggleButton = new JToggleButton("Interruptor", true);
		tglbtnNewToggleButton.setBounds(189, 291, 121, 23);
		contentPane.add(tglbtnNewToggleButton);
		
		// Spinner
		JSpinner spinner = new JSpinner();
		spinner.setBounds(371, 20, 29, 20);
		contentPane.add(spinner);
		
		String pelis[] = { "Star wars", "Revolution", "007: Todo o Nada"};
		
		JList<String> list = new JList<>(pelis);
		list.setBounds(371, 72, 86, 80);
		contentPane.add(list);
		

		// Scroll para textarea
		JScrollPane scroll_v = new JScrollPane(list);
		scroll_v.setBounds(371, 72, 86, 80);
		scroll_v.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll_v);
		setVisible(true);
		
	}
	
}
