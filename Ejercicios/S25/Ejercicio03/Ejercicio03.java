package Ejercicio03;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class Ejercicio03 extends JFrame {

	private JPanel contentPane;
	private JTextField textHoras;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio03 frame = new Ejercicio03();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio03() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 235, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaOperativo = new JLabel("Sistema operativo:");
		lblSistemaOperativo.setBounds(10, 11, 131, 14);
		contentPane.add(lblSistemaOperativo);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Windows");
		rdbtnNewRadioButton.setBounds(6, 32, 109, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Linux");
		rdbtnNewRadioButton_1.setBounds(6, 58, 109, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Mac");
		rdbtnNewRadioButton_2.setBounds(6, 84, 109, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		ButtonGroup sistemaOperativo = new ButtonGroup();
		sistemaOperativo.add(rdbtnNewRadioButton);
		sistemaOperativo.add(rdbtnNewRadioButton_1);
		sistemaOperativo.add(rdbtnNewRadioButton_2);
		
		JLabel lblEspecialidad = new JLabel("Especialidad:");
		lblEspecialidad.setBounds(14, 125, 105, 14);
		contentPane.add(lblEspecialidad);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Programaci\u00F3n");
		chckbxNewCheckBox.setBounds(10, 146, 97, 23);
		contentPane.add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Dise\u00F1o gr\u00E1fico");
		chckbxNewCheckBox_1.setBounds(10, 172, 97, 23);
		contentPane.add(chckbxNewCheckBox_1);
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Administraci\u00F3n");
		chckbxNewCheckBox_2.setBounds(10, 198, 97, 23);
		contentPane.add(chckbxNewCheckBox_2);
		
		JLabel lblNewLabel = new JLabel("Horas dedicadas en el ordenador:");
		lblNewLabel.setBounds(10, 236, 224, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblHoras = new JLabel("Horas:");
		lblHoras.setBounds(10, 298, 46, 14);
		contentPane.add(lblHoras);
		
		textHoras = new JTextField();
		textHoras.setEditable(false);
		textHoras.setBounds(55, 295, 30, 20);
		contentPane.add(textHoras);
		textHoras.setColumns(10);
		
		JSlider slider = new JSlider();
		slider.addChangeListener(new ChangeListener() {
			// Cuando cambia el valor del slider...
			public void stateChanged(ChangeEvent arg0) {
				// Mostramos el valor seleccionado en el textField
				textHoras.setText(String.valueOf(slider.getValue()));
			}
		});
		slider.setMaximum(10);
		slider.setValue(0);
		slider.setBounds(10, 261, 200, 26);
		contentPane.add(slider);
		
		JButton btnIntroducirDatos = new JButton("Introducir datos");
		btnIntroducirDatos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Declaramos e inicializamos variables
				String datos = "";	// Cadena donde iremos concatenamos los resultados
				int count = 0;		// contador para saber cuantos checkbox han sido seleccionados 
				
				// Concatenamos sistema operativo
				datos += "Sistema operativo:\n";
				
				// Recorremos los sistema operativos y concatenamos el seleccionado
				Enumeration<AbstractButton> enumeration = sistemaOperativo.getElements();
				
				while (enumeration.hasMoreElements()) {
					AbstractButton button = enumeration.nextElement();
					
					if (button.isSelected()) {
						datos += "· " + button.getText();
						count++;
					}
				}
				
				if (count <= 0) {
					JOptionPane.showMessageDialog(null, "Debes introducir almenos un sistema operativo.");
					return;
				}
				
				datos += "\n\nEspecializaciones:\n";

				count = 0;
				
				// Concatenamos las especializaciones que han sido seleccionadas
				if (chckbxNewCheckBox.isSelected()) {
					datos += "· " + chckbxNewCheckBox.getText() + "\n";
					count++;
				}

				if (chckbxNewCheckBox_1.isSelected()) {
					datos += "· " + chckbxNewCheckBox_1.getText() + "\n";
					count++;
				}

				if (chckbxNewCheckBox_2.isSelected()) {
					datos += "· " + chckbxNewCheckBox_2.getText() + "\n";
					count++;
				}
				
				// Si no hay ninguna especialización mostramos mensaje de error 
				if (count <= 0) {
					JOptionPane.showMessageDialog(null, "Debes introducir almenos una especialización.");
					return;
				}
				
				// Concatenamos las horas dedicadas
				datos += "\nHoras dedicadas en el ordenador: " + slider.getValue();
				
				// Mostramos resultados
				JOptionPane.showMessageDialog(null, datos);
				
				
			}
		});
		btnIntroducirDatos.setBounds(10, 333, 199, 23);
		contentPane.add(btnIntroducirDatos);
	}
}
