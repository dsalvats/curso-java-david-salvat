/**
 * 
 */
package Ejemplo02;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;

/**
 * @author David Salvat Sedano
 *
 */
public class AplicacionGrafica extends JFrame {

	private JPanel contentPane;
	public AplicacionGrafica() {
		
		setTitle("T�tulo de la ventana");
		
		setBounds(400, 200, 500, 420);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		contentPane = new JPanel();
		
		contentPane.setLayout(null);
		
		setContentPane(contentPane);
		
		// Creamos barra menu principal
		JMenuBar barraMenu = new JMenuBar();
		
		// Creamos men�s
		JMenu archivo = new JMenu("Archivo");
		JMenu editar = new JMenu("Editar");
		barraMenu.add(archivo);
		barraMenu.add(editar);

		// Creamos y asignamos contenido del men� archivo
		JMenuItem abir = new JMenuItem("Abrir");
		JMenuItem guardar = new JMenuItem("Guardar");
		JMenuItem cargar = new JMenuItem("Cargar");
		JMenuItem salir = new JMenuItem("Salir");
		archivo.add(abir);
		archivo.add(guardar);
		archivo.add(cargar);
		archivo.add(salir);

		// Creamos y asignamos contenido del men� editar
		JMenuItem modificar = new JMenuItem("Modificar");
		JMenuItem copiar = new JMenuItem("Copiar");
		JMenuItem pegar = new JMenuItem("Pegar");
		editar.add(modificar);
		editar.add(copiar);
		editar.add(pegar);
		
		// A�adimos la barra de men� al programa
		setJMenuBar(barraMenu);
		
		setVisible(true);
		
	}
	
}
