package Ejercicio02;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio02 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio02 frame = new Ejercicio02();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio02() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 169);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Escribe el t\u00EDtulo de una pel\u00EDcula");
		lblNewLabel.setBounds(10, 11, 177, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(10, 36, 149, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblPelculas = new JLabel("Pel\u00EDculas");
		lblPelculas.setBounds(233, 11, 163, 14);
		contentPane.add(lblPelculas);
		
		JComboBox<String> comboBox = new JComboBox<>();
		comboBox.setBounds(233, 36, 163, 20);
		contentPane.add(comboBox);
		comboBox.addItem("Star Wars");
		comboBox.addItem("Avengers");
		comboBox.addItem("Hulk");
		
		JButton btnAadir = new JButton("A\u00F1adir");
		btnAadir.setBounds(145, 88, 89, 23);
		contentPane.add(btnAadir);
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String newMovie = textField.getText();
				// A�adimos la pel�cula del textField al comboBox
				comboBox.addItem(newMovie);
				// Seleccionamos la pel�cula a�adida en el comboBox
				comboBox.setSelectedItem(newMovie);
			}
		});
	}
}
