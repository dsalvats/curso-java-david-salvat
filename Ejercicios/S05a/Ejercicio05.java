/**
 * 
 */
package Ejercicio05;

/**
 * @author David Salvat Sedano
 *
 */

/* Crea una aplicaci�n que nos convierta un n�mero en base decimal a binario. Esto lo
 * realizara un m�todo al que le pasaremos el numero como par�metro, devolver� un String
 * con el numero convertido a binario. Para convertir un numero decimal a binario, debemos
 * dividir entre 2 el numero y el resultado de esa divisi�n se divide entre 2 de nuevo hasta que
 * no se pueda dividir mas, el resto que obtengamos de cada divisi�n formara el numero
 * binario, de abajo a arriba.
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int numeroDecimal;
		String numeroBinario;
		
		// Pedimos al usuario que entre un numero decimal
		numeroDecimal = Integer.parseInt(JOptionPane.showInputDialog("N�mero decimal a convertir a binario"));
		
		// Convertimos el numero decimal a binario
		numeroBinario = decimalToBinary(numeroDecimal);
		
		//Imprimimos resultado
		JOptionPane.showMessageDialog(null, "El equivalente binario de " + numeroDecimal + " es: " + numeroBinario);
	}
	
	// Convertimos el par�metro int pasado a una String que contiene el valor en binario
	private static String decimalToBinary(int numeroDecimal) {
		
		// Declaramos variables
		StringBuilder sb = new StringBuilder("");
		
		// Mientras numero decimal no se pueda dividir m�s
		while (numeroDecimal > 0) {
			// A�adimos el resto de numero decimal entre 2 al principio
			sb.insert(0, numeroDecimal % 2);
			
			// Dividimos el numero decimal entre 2
			numeroDecimal /= 2;
		}
		
		// Retornamos la variable
		return sb.toString();
	}

}
