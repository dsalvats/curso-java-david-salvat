/**
 * 
 */
package Ejercicio01;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int opcion = pedirFigura();
		
		// Comprobamos que figura queremos calcular
		switch (opcion) {
			// AREA CIRCULO
			case 1:
				JOptionPane.showMessageDialog(null, calcularAreaCirculo(pedirCirculo()));
				break;
			// AREA TRIANGULO
			case 2:
				JOptionPane.showMessageDialog(null, calcularAreaTriangulo(pedirTriangulo()));
				break;
			// AREA CUADRADO
			case 3:
				JOptionPane.showMessageDialog(null, calcularAreaCuadrado(pedirCuadrado()));
				break;
			// No contemplado
			default:
				JOptionPane.showMessageDialog(null, "Figura no contemplada.");
		}
	}
	
	// Pedimos la figura a calcular
	private static int pedirFigura() {
		return Integer.parseInt(JOptionPane.showInputDialog("Selecciona la figura a calcular:\n"
				+ "1) C�rculo\n"
				+ "2) Triangulo\n"
				+ "3) Cuadrado"));
	}
	
	// Obtenemos los valores para calcular la �rea del c�rculo
	private static int pedirCirculo() {
		return Integer.parseInt(JOptionPane.showInputDialog("Introduce radio a calcular"));
	}

	// Obtenemos los valores para calcular la �rea del tri�ngulo
	private static int[] pedirTriangulo() {
		int valores[] = new int[2];
		
		valores[0] = Integer.parseInt(JOptionPane.showInputDialog("Introduce base a calcular"));
		valores[1] = Integer.parseInt(JOptionPane.showInputDialog("Introduce altura a calcular"));
		
		return valores;
	}
	
	// Obtenemos los valores para calcular la �rea del  cuadrado
	private static int pedirCuadrado( ) {
		return Integer.parseInt(JOptionPane.showInputDialog("Introduce lado a calcular"));
	}
	
	// Retornamos la �rea del c�rculo
	private static double calcularAreaCirculo(int radio) {
		return (radio * Math.pow(Math.PI, 2));
	}
	
	// Retornamos la �rea del tri�ngulo
	private static double calcularAreaTriangulo(int[] valores) {
		return ((double)(valores[0]) * valores[1]) / 2;
	}
	
	// Retornamos la �rea del cuadrado
	private static int calcularAreaCuadrado(int lado) {
		return ((int)Math.pow(lado, 2));
	}
	
}
