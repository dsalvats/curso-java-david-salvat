/**
 * 
 */
package Ejercicio07;

/**
 * @author David Salvat Sedano
 *
 */

/*
Crea un aplicaci�n que nos convierta una cantidad de euros introducida por teclado a otra
moneda, estas pueden ser a dolares, yenes o libras. El m�todo tendr� como par�metros, la
cantidad de euros y la moneda a pasar que sera una cadena, este no devolver� ning�n valor,
mostrara un mensaje indicando el cambio (void)

El cambio de divisas son:
�
�
�
0.86 libras es un 1 �
1.28611 $ es un 1 �
129.852 yenes es un 1 �
 */

import javax.swing.JOptionPane;

public class Main {

	// Declaraci�n de constantes globales
	final static double CONV_EUR_LIB = 0.86;
	final static double CONV_EUR_DOL = 1.28611;
	final static double CONV_EUR_YEN = 129.852;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		double cantidadAConvertir;
		String monedaAConvertir;

		// Pedir cantidad
		cantidadAConvertir = Double.parseDouble(JOptionPane.showInputDialog("Cantidad a convertir en euros:"));
		
		// Pedir moneda de conversi�n y repetir hasta obtener 
		do {
		monedaAConvertir = JOptionPane.showInputDialog("Moneda a convertir:\n"
				+ "- LIBRAS\n"
				+ "- DOLARES\n"
				+ "- YENES").toUpperCase().trim();
		} while(!monedaAConvertir.equals("LIBRAS") 
				&& !monedaAConvertir.equals("DOLARES") 
				&& !monedaAConvertir.equals("YENES"));
		
		// Llamamos a convertir la cantidad de euros con la moneda introducida
		convertirCantidad(cantidadAConvertir, monedaAConvertir);
		
	}
	
	// Hacer conversi�n de euros a moneda seleccionada con la cantidad especificada
	private static void convertirCantidad(double cantidad, String monedaAConvertir) {
		
		// Declaramos variables
		double resultado = 0;
		boolean correcto = true;
		
		switch (monedaAConvertir) {
			// LIBRAS
			case "LIBRAS":
				resultado = cantidad * CONV_EUR_LIB;
				break;
			// DOLARES
			case "DOLARES":
				resultado = cantidad * CONV_EUR_DOL;
				break;
			// YENES
			case "YENES":
				resultado = cantidad * CONV_EUR_YEN;
				break;
			// No contemplado, definimos que el proceso ha sido err�neo
			default:
				correcto = false;
		}
		
		// Comprobamos si la introducci�n ha sido correcta
		if (correcto)
			// Imprimir resultado final
			JOptionPane.showMessageDialog(null, cantidad + " euros son " + resultado +  " " + monedaAConvertir);
		else
			// Imprimir resultado err�neo
			JOptionPane.showMessageDialog(null, "Esa moneda no la podemos convertir");
		
	}

}
