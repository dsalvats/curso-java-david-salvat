/**
 * 
 */
package Ejercicio08;

/**
 * @author David Salvat Sedano
 *
 */

/*
 * Crea un array de 10 posiciones de n�meros con valores pedidos por teclado. Muestra por
 * consola el indice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y
 * otro para mostrar.
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int[] array = fillArray();
		int pos;
		
		// Pedir constantemente nuevas posiciones
		while(true) {
			// Pide posici�n al usuario
			pos = Integer.parseInt(JOptionPane.showInputDialog("De que posici�n quieres saber el valor?"));
			
			// Imprimimos valor en posi�n x
			printValueAtPos(array, pos);
		}
		
	}
	
	// Rellenamos array
	private static int[] fillArray() {
		
		// Declaraci�n de variables
		int[] array = new int[10];
		
		// Pedimos 10 valores al usuario y los introducimos en el array
		for (int i = 0; i < 10; i++)
			array[i] = Integer.parseInt(JOptionPane.showInputDialog("N�mero " + (i + 1) + " a introducir: "));
		
		return array;
	}
	
	private static void printValueAtPos(int[] array, int pos) {
		// Imprimimos el valor en la posici�n x
		JOptionPane.showMessageDialog(null, "El valor del array en la posici�n " + pos + " es " + array[pos]);
	}

}
