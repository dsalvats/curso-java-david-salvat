/**
 * 
 */
package Ejercicio06;

/**
 * @author David Salvat Sedano
 *
 */

/*Crea una aplicaci�n que nos cuente el n�mero de cifras de un n�mero entero positivo
(hay que controlarlo) pedido por teclado. Crea un m�todo que realice esta acci�n, pasando
el n�mero por par�metro, devolver� el n�mero de cifras.*/

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int numero;
		int lenght;
		
		// Pedimos al usuario un n�mero positivo
		// Si este n�mero no es positivo se  volver� a pedir hasta que entre un n�mero deseado
		do {
			numero = Integer.parseInt(JOptionPane.showInputDialog("Entra un n�mero positivo: "));
		} while (numero < 0);

		// Obtenemos la longitud del n�mero entrado
		lenght = getLenght(numero);
		
		// Imprimimos la longitud del n�mero entrado
		JOptionPane.showMessageDialog(null, "La longitud de " + numero + " es: " + lenght);
		
	}
	
	// A partir del entero pasado por par�metro obtiene su longitud
	private static int getLenght(int numero) {
		// Declaraci�n de variables
		int lenght = 0;
		
		// Mientras numero sea divisible entre 10
		// A�adimos 1 a la longitud
		while(numero > 0) {
			lenght++;
			numero /= 10;
		}
		
		// Devolvemos la longitud
		return lenght;
	}

}
