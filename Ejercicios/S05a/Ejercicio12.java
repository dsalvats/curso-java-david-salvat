/**
 * 
 */
package Ejercicio12;

import javax.swing.JOptionPane;

/**
 * @author David Salvat Sedano
 *
 */

/* Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
 * n�meros aleatorios entre 1 y 300 y mostrar aquellos n�meros que acaben en un d�gito que
 * nosotros le indiquemos por teclado (debes controlar que se introduce un numero correcto),
 * estos deben guardarse en un nuevo array.
 * Por ejemplo, en un array de 10 posiciones e indicamos mostrar los n�meros acabados en 5,
 * podr�a salir 155, 25, etc.
 */

public class Ejercicio12 {

	/**
	 * @param args
	 */
	
	// Declaraci�n de constantes globales
	final static int MIN_RND_RANGE = 0;
	final static int MAX_RND_RANGE = 300;
	
	public static void main(String[] args) {
		// Declaraci�n de variables
		int[]	initialArray;
		int[]	finalArray;
		int		length;
		int		termination;
		boolean firstTry = true;
		String	string = "";
		
		// Pedir longitud de array
		length = Integer.parseInt(JOptionPane.showInputDialog("Tama�o de de array inicial?"));
		
		// Pedir terminaci�n
		do {
			if (!firstTry)
				JOptionPane.showMessageDialog(null, "ERROR: N�mero introducido incorrecto. "
						+ "Debe introducir un n�mero entre 0 y 9");
			
			firstTry = false;
			
			termination = Integer.parseInt(JOptionPane.showInputDialog("Indique la terminaci�n que desea [0-9]: "));
		} while (termination > 9 || termination < 0);
		
		// Llenar array con numeros aleatorios
		initialArray = fillRndArray(length);
		
		// Obtener nueva array con solo solo los n�meros acabados en 5
		finalArray = calculateArrayWithTermination(initialArray, termination);
		
		// Concatenamos array inicial
		string += "Array inicial: " + arrayToString(initialArray) + "\n";
		
		// Concatenamos array con enteros acabados en 5
		string += "Array final: " + arrayToString(finalArray);
		
		// Imprimimos string resultado
		JOptionPane.showMessageDialog(null, string);

	}
	
	// Devuelve un String personalizado de un array pasado
	private static String arrayToString(int[] array) {
		// Declaraci�n de variables
		String string;
		
		string = " (" + array.length + ")  [ ";
		
		if (array.length == 0) {
			string += " No hay valores a mostrar ]";
			return string;
		}
		
		// Recorremos el array y concatenamos los valores para cada �ndice
		for (int i = 0; i < array.length; i++)
			string += array[i] + " ";
		
		string += "]";
		
		return string;			
	}

	// Dado un array y una terminaci�n retornamos un array solo con los enteros que acaban en esa terminaci�n
	private static int[] calculateArrayWithTermination(int[] array, int termination) {
		// Declaraci�n de variables
		int[]	arrayAux;
		int		matches = 0;
		int		position = 0;
		
		// Recorremos el array pasado contando las coincidencias para poder crear el nuevo array
		for (int i = 0; i < array.length; i++)
			if (isNumberEndedIn(array[i], termination))
					matches++;
			
		// Damos longitud al nuevo array
		arrayAux = new int[matches];
		
		// Recorremos el array a�adiendo las coincidencias en el nuevo array
		for (int i = 0; i < array.length; i++) {
			if (isNumberEndedIn(array[i], termination)) {
				arrayAux[position] = array[i];
				position++;
			}				
		}
		
		return arrayAux;
	}
	
	// Dado un n�mero y una terminaci�n comprobamos si ese numero acaba en la terminaci�n
	private static boolean isNumberEndedIn(int num, int termination) {
		// Declaraci�n de variables
		boolean terminated = true;
		// Casteamos el entero a comprar y la terminaciones a String para poder operar m�s facilmente
		String	stringNum = Integer.toString(num);
		String stringTermination = Integer.toString(termination);
		
		// Devolvemos false si el n�mero no acaba en la terminaci�n indicada
		if (!stringNum.endsWith(stringTermination))
			terminated = false;
				
		return terminated;
	}
	
	// Devuelve un array lleno de numeros aleatorios dentro de un rango
	private static int[] fillRndArray(int length) {
		// Declaraci�n de variables
		int[] array = new int[length];
		
		// Poblamos el array con n�meros aleatorios dentro de un rango
		for (int i = 0; i < length; i++)
			array[i] = generateRndNumber();
	
		return array;
	}
	
	// Genera un entero aleatorio dentro de un rango
	private static int generateRndNumber() {
		return (int) (Math.random() * (MAX_RND_RANGE - MIN_RND_RANGE) + MIN_RND_RANGE);
	}

}
