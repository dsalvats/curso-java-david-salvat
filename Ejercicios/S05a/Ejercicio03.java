/**
 * 
 */
package Ejercicio03;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int numero;
		
		numero = Integer.parseInt(JOptionPane.showInputDialog("Entra un n�mero a comprobar si es primo"));
		
		if (isPrime(numero))
			JOptionPane.showMessageDialog(null, "El n�mero entrado es primo");
		else
			JOptionPane.showMessageDialog(null, "El n�mero entrado NO es primo");
		
	}
	
	// Comprobamos si el numero dado es primo
	public static boolean isPrime(int num) {
		boolean prime = true;
		
		if (num == 1)
			return false;
		
		for (int i = 2; i < num; i++)
			if (num % i == 0) {
				prime = false;
				break;
			}
		
		return prime;
	}

}
