/**
 * 
 */
package Ejercicio04;

/**
 * @author David Salvat Sedano
 *
 */

/*
 * Crea una aplicaci�n que nos calcule el factorial de un n�mero pedido por teclado, lo
 * realizara mediante un m�todo al que le pasamos el n�mero como par�metro. Para calcular
 * el factorial, se multiplica los n�meros anteriores hasta llegar a uno. Por ejemplo, si
 * introducimos un 5, realizara esta operaci�n 5*4*3*2*1=120.
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int numero;
		int factorial;
		
		// Pedimos al usuario que introduzca el n�mero a calcular su factorial
		numero = Integer.parseInt(JOptionPane.showInputDialog("Introduce n�mero a calcular factorial:"));
		
		// Calculamos el factorial
		factorial = calcularFactorial(numero);
		
		// Imprimimos su resultado
		JOptionPane.showMessageDialog(null, "El factorial de " + numero + " es " + factorial);

	}
	
	// Retorna el factorial del entero pasado por par�metro
	public static int calcularFactorial(int numero) {
		
		// Declaraci�n de variables
		int factorial = numero;
		
		// Recorremos decrecientemente el valor del par�metro pasado
		// hasta llegar a 2 y multiplicamos en cada proceso el valor auxiliar
		for (int i = numero - 1; i > 1; i--) {
			factorial *= i;
		}
		
		return factorial;
	}

}
