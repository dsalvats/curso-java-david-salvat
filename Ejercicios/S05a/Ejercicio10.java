/**
 * 
 */
package Ejercicio10;

import javax.swing.JOptionPane;

/**
 * @author David Salvat Sedano
 *
 */

/* Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
 * n�meros aleatorios primos entre los n�meros deseados, por �ltimo nos indicar cual es el
 * mayor de todos.
 * Haz un m�todo para comprobar que el n�mero aleatorio es primo, puedes hacer todos lo
 * m�todos que necesites.
 */

public class Ejercicio10 {

	/**
	 * @param args
	 */
	
	// Declaraci�n de constantes globales
	final static int MIN_RNDM_RANGE = 0;
	final static int MAX_RNDM_RANGE = 1000;
	
	public static void main(String[] args) {
		// Declaraci�n de variables
		int[] 	array;
		int 	length;
		int 	biggestNumber;
		
		// Pedimos longitud de array
		length = Integer.parseInt(JOptionPane.showInputDialog("Qu� longitud desea que tenga nuestra array?"));
		
		// Llenamos array con n�meros primos
		array = fillArray(length);
		
		// Conseguimos mayor n�mero dentro el array
		biggestNumber = getBiggestIntegerFromArray(array);
		
		// Imprimimos array con el n�mero mayor
		JOptionPane.showMessageDialog(null, arrayToString(array) + "El mayor n�mero es: " + biggestNumber);

	}
	
	// Pasado un array la convertimos a String con un formato personalizado
	private static String arrayToString(int[] array) {
		// Declaraci�n de variables
		String string = "";
		
		// Recorremos todo el array y vamos concatenando el �ndice y valor
		for (int i = 0; i < array.length; i++) {
			string += "[" + (i + 1) + "] " + array[i] + "\n";
		}
		
		return string;
	}
	
	// Pasado un array de enteros obtenemos el mayor valor de ellos 
	private static int getBiggestIntegerFromArray(int[] array) {
		// Declaraci�n de variables
		// Si no se comprueba lo contrario el entero mayor es el de la primera posici�n
		int biggestNumber = array[0];
		
		// Recorremos todo el array
		for (int i = 1; i < array.length; i++) {
			// Comparamos el valor del array en el indice actual
			// con el valor del valor mayor guardado
			// Si es mayor, lo guardamos
			if (array[i] > biggestNumber)
				biggestNumber = array[i];
		}
		
		return biggestNumber;
	}
	
	// Pasada una longitud creamos un array de esa logintud y la llenamos de enteros primos
	private static int[] fillArray(int lenght) {
		// Declaraci�n de variables
		int[] 	array = new int[lenght];
		int 	aux;
		
		// Repetemos el proceso de generar un entero primo tantas veces como longitud de array tengamos
		for (int i = 0; i < lenght; i++) {
			// Cogemos un entero nuevo hasta que comprovamos que este es primo
			do {
				aux = generateRndm();
			} while(!isPrime(aux));
			
			// Guardamos nuestro valor primo
			array[i] = aux;
		}
		
		return array;
	}
	
	// Comprobamos si nuestro valor pasado es primo
	private static boolean isPrime(int num) {
		// Declaraci�n de variables
		// Hasta que se demuestre lo contrario nuestro entero es primo
		boolean prime = true;
		
		// Como cualquier valor menor o igual a 1 no es primo
		// Devolvemos que el entero pasado no es primo
		if (num <= 1)
			return false;
		
		// Repetimos el proceso hasta llegar al n�mero mismo comprobando
		// que no es divisible por ninguno de ellos
		// si fuera divisible por alguno definiriamos que nuestro entero
		// pasado no es primo y saldr�amos inmediatamente del for
		for (int i = 2; i < num; i++) {
			if (num % i == 0) {
				prime = false;
				break;
			}
				
		}
		
		return prime;
	}
	
	// Generamos un entero aleatorio entre un rango de valores
	private static int generateRndm() {
		return (int) (Math.random() * (MAX_RNDM_RANGE - MIN_RNDM_RANGE + 1) + MIN_RNDM_RANGE);
	}

}
