/**
 * 
 */
package Ejercicio11;

import javax.swing.JOptionPane;

/**
 * @author David Salvat Sedano
 *
 */

/*
 * Crea dos arrays de n�meros con una posici�n pasado por teclado.
 * Uno de ellos estar� rellenado con n�meros aleatorios y el otro apuntara al array anterior,
 * despu�s crea un nuevo array con el primer array (usa de nuevo new con el primer array) con
 * el mismo tama�o que se ha pasado por teclado, rellenalo de nuevo con n�meros aleatorios.
 * Despu�s, crea un m�todo que tenga como par�metros, los dos arrays y devuelva uno nuevo
 * con la multiplicaci�n de la posici�n 0 del array1 con el del array2 y as� sucesivamente. Por
 * �ltimo, muestra el contenido de cada array. */

public class Ejercicio11 {

	/**
	 * @param args
	 */
	
	final static int MIN_RND_RANGE = 0;
	final static int MAX_RND_RANGE = 20;
	
	public static void main(String[] args) {
		// Declaraci�n de variables
		int[]	array1;
		int[]	array2;
		int[]	resultArray;
		int		length;
		String	resultString = "";
		
		// Pedimos longitud de variablres
		length = Integer.parseInt(JOptionPane.showInputDialog("Qu� longitud deseas que tengan los arrays?"));
		
		// Llenamos array1
		array1 = fillArray(length);
		
		// Llenamos array2
		array2 = fillArray(length);
		
		// Multiplicamos array1 con array2
		resultArray = arrayProduct(array1, array2);
		
		// Concatenamos array1
		resultString += "Array 1:" + arrayToString(array1) + "\n";
		
		// Concatenamos array2
		resultString += "Array 2:" + arrayToString(array2) + "\n";
		
		// Concatenamos array3
		resultString += "Array Resultante:" + arrayToString(resultArray);
		
		// Imprimimos la cadena resultante
		JOptionPane.showMessageDialog(null, resultString);

	}
	
	// Pasada un array devolvemos un String con un formato personalizado
	private static String arrayToString(int[] array) {
		// Declaraci�n de variables
		String string = "[ ";
		
		// Recorremos el array y llenamos la cadena de texto con nuestro formato
		for (int i = 0; i < array.length; i++)
			string += array[i] + " ";
			
		string += "]";
			
		return string;
	}
	
	// Calcula el array haciendo el producto en cada �ndice respecto 2 arrays pasadas
	private static int[] arrayProduct(int[] array1, int[] array2) {
		// Declaraci�n de variables
		int[] resultArray = new int[array1.length];
		
		// Recorremos el array que acabamos de declarar
		for (int i = 0; i < resultArray.length; i++)
			// Asignamos el producto de los mismo valores en X �ndice
			resultArray[i] = array1[i] * array2[i];
		
		return resultArray;
	}
	
	// Llena un array con valores aleatorios
	private static int[] fillArray(int length) {
		// Declaraci�n de variables
		int[] array = new int[length];
		
		// Recorremos el array para llenar cada �ndice
		for (int i = 0; i < length; i++) 
			// A�adimos un n�mero generado aletoriamente al �ndice correspondiente 
			array[i] = generateRnd();
		
		return array;
	}
	
	// Devuelve un entero aleatorio entre un rango
	private static int generateRnd() {
		return (int) (Math.random() * (MAX_RND_RANGE - MIN_RND_RANGE) + MIN_RND_RANGE);
	}

}
