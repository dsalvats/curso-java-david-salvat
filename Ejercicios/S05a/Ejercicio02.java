/**
 * 
 */
package Ejercicio02;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int[] array;

		// Flujo para pedir datos al usuario
		// y sacar por pantall un mensaje con N valores entre un rango X e Y
		array = fillArray(pedirCantidad(), pedirRangoAleatorio());
		JOptionPane.showMessageDialog(null, toString(array));

	}

	// Obtenemos la cantidad de n�meros a generar
	public static int pedirCantidad() {
		return Integer.parseInt(JOptionPane.showInputDialog("Cuantos n�meros quiere?"));
	}

	// Llenamos una array con n�meros aleatorios
	public static int[] fillArray(int cantidad, int[] rango) {
		int[] array = new int[cantidad];

		for (int i = 0; i < cantidad; i++) {
			array[i] = crearAleatorio(rango);
		}

		return array;
	}

	// Obtenemos el rango para crear numeros aleatorios
	public static int[] pedirRangoAleatorio() {
		int[] rango = new int[2];

		// Pedimos n�mero m�nimo del rango
		do {
			rango[0] = Integer.parseInt(JOptionPane.showInputDialog("M�nimo del rango:"));
		} while (rango[0] < 1);

		// Pedimo n�mero m�ximo del rango
		do {
			rango[1] = Integer.parseInt(JOptionPane.showInputDialog("M�ximo del rango: "));
		} while (rango[1] <= rango[0]);

		return rango;
	}

	// Generamos n�meros aleatorios dentro de un rango a -> b positivo
	public static int crearAleatorio(int[] rango) {
		return (int) ((Math.random() * ((rango[1] - rango[0]) + 1)) + rango[0]);
	}

	// Retornamos array a String
	public static String toString(int[] array) {
		String string = "";

		for (int i = 0; i < array.length; i++)
			string += array[i] + " ";

		return string;
	}

}
