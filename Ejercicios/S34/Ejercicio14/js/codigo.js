$(document).ready(function() {
	/***********************
	 * CONSTANTES GLOBALES *
	************************/
	var MAX_LENGTH = 8;
	var INPUT_REGEX = /^(?:\d{1,9}(?:\.\d{0,9})?)?$/;
	
	/**********************
	 * VARIABLES GLOBALES *
	 **********************/
	var _input_ = $("#calculadora input");
	var _numbers_ = $(".number");
	var _remove_all_ = $("#ce-button");
	var _remove_last_ = $("#retr");
	var _remove_current_ = $("#c-button");
	
	var _square_root_ = $("#square-root");
	var _division_ = $("#division");
	var _product_ = $("#product");
	var _modulo_ = $("#modulo");
	var _difference_ = $("#difference");
	var _inverse_ = $("#inverse");
	var _decimal_ = $("#decimal");
	var _plus_ = $("#plus");
	var _opposite_ = $("#opposite");
	var _result_ = $("#result");
	
	var firstValue = undefined;
	var operator = undefined;
	
	/***********
	 * MÉTODOS *
	 ***********/
	// Source: https://www.jacklmoore.com/notes/rounding-in-javascript/
	var round = function(value, decimals) {
		return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
	};
	
	// Añadimos el valor deseado al input
	var addNumber = function(element) {
		// Comprobamos si el input ha alcanzado la longitud máxima
		if (getInputLength() >= MAX_LENGTH)
			return;
		
		var number = element.text();
		_input_.val(_input_.val() + number);
	};
	
	var getInputLength = function() {
		return _input_.val().length;
	}
	
	var getValue = function() {
		return parseFloat(_input_.val());
	}
	
	var setValue = function(value) {
		if (typeof(value) === 'number')
			value = round(value, 4);
		
		_input_.val(value);
	}
	
	// Asigna la escritura al input donde  metemos los valores
	var focusInput = function() {
		_input_.focus();
	};
	
	var addDecimal = function() {
		if (_input_.val() === "")
			setValue("0");
		
		setValue(getValue() + ".");
	};
	
	var removeLast = function() {
		setValue(_input_.val().slice(0, -1));
	};
	
	var removeAll = function() {
		setValue("");
		firstValue = undefined;
		operator = undefined;
	};
	
	var removeCurrent = function() {
		setValue("");
	};
	
	/***************
	 * OPERACIONES *
	 ***************/
	 var squareRoot = function() {
		if (getValue() === "")
			return;
		 
		setValue(Math.sqrt(getValue()));
	 };
	 
	 var inverse = function() {
		if (getValue() === "")
			return;
		 
		setValue(1 / getValue());
	 }
	 
	 var opposite = function() {
		 if (getValue() === "")
			 return;
		 
		 setValue(getValue() * (-1));
	 };
	
	var plus = function() {
		if (getValue() === "")
			return;
		
		firstValue = getValue();
		operator = "+";
		setValue("");
	};
	
	var difference = function() {
		if (getValue() === "")
			return;
		
		firstValue = getValue();
		operator = "-";
		setValue("");
	};
	
	var division = function() {
		if (getValue() === "")
			return;
		
		firstValue = getValue();
		operator = "/";
		setValue("");
	};
	
	var modulo = function() {
		if (getValue() === "")
			return;
		
		firstValue = getValue();
		operator = "%";
		setValue("");
	};
	
	var product = function() {
		if (getValue() === "")
			return;
		
		firstValue = getValue();
		operator = "*";
		setValue("");
	};
	
	var result = function() {
		if (_input_.val() === "")
			return;
		
		switch(operator) {
			case "+":
				setValue(firstValue + getValue());
				break;
			case "-":
				setValue(fistValue - getValue());
				break;
			case "*":
				setValue(firstValue - getValue());
				break;
			case "/":
				setValue(firstValue / getValue());
				break;
			case "%":
				setValue(firstValue % getValue());
				break;
		};
	};
	/*********************
	 * EXECUTION METHODS *
	 *********************/
	focusInput();
	
	/***********
	 * EVENTOS *
	 ***********/
	// Detecta cuando se ha entrado alguna valor en el input
	_input_.keyup(function(value) {
		var oldValue = _input_.val().substring(0, _input_.val().length-1);
		
		if (value.keyCode === 13) {
			result();
			return;
		}
			
		
		if (_input_.val() === ".") {
			_input_.val("0.");
			return;
		}
		
		switch(value.key) {
			case "+":
				plus();
				setValue("");
				return;
				break;
			case "-":
				difference();
				setValue("");
				return;
				break;
			case "/":
				division();
				setValue("");
				return;
				break;
			case "*":
				product();
				setValue("");
				return;
				break;
			case "%":
				modulo();
				setValue("");
				return;
				break;
			case "s":
				squareRoot();
				return;
				break;
			case "i":
				inverse();
				return;
				break;
			case "o":
				opposite();
				return;
				break;
		}
		
		// Si el input ha alcanzado la longitud máxima no hacemos nada
		if (getInputLength() > MAX_LENGTH)
			setValue(oldValue);
		
		// Si el valor introducido sigue el regex se introduce
		if(!(INPUT_REGEX.test(_input_.val())))
			setValue(oldValue);
			
	});
	
	// Detecta cuando se ha pulsado algun botón de tipo número
	_numbers_.click(function() {
		// Añadimos el número al input
		addNumber($(this));
	});
	
	_decimal_.click(addDecimal);
	
	/***********************
	 * EVENTOS OPERACIONES *
	 ***********************/
	_remove_last_.click(removeLast);
	_remove_all_.click(removeAll);
	_remove_current_.click(removeCurrent);
	_square_root_.click(squareRoot);
	_inverse_.click(inverse);
	_opposite_.click(opposite);
	_plus_.click(plus);
	_difference_.click(difference);
	_division_.click(division);
	_product_.click(product);
	_modulo_.click(modulo);
	_result_.click(result);
	
	// Detecta cuando se ha pulsado alguna parte del documento
	// Esto se hace para mantener siempre el input seleccionado
	_input_.focusout(function() {
		// Seleccionamos el input
		focusInput();
	});
});



