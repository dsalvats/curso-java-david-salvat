$(document).ready(function() {
	var _estats_;
	
	var iniciar = function() {
		initializeEstats();
		populateEstats(36000);		
		showEstats();
	};
	
	var getRandomNumber = function() {
		return (Math.floor(Math.random() * 6) + 1);
	};
	
	var getThrow = function() {
		return (getRandomNumber() + getRandomNumber());
	};
	
	var initializeEstats = function() {
		_estats_ = new Object();
		
		for (var i = 2; i <= 12; i++)
			_estats_[i] = 0;
		
	};
	
	var populateEstats = function(num) {
		for (var i = 0; i < num; i++)
			_estats_[getThrow()]++;
		
	}
	
	var showEstats = function() {
		var html = "";
		
		for (var estat in _estats_)
			html += "<span>" + estat + ":  " + _estats_[estat] + " veces</span><br/>"
		
		$("#resultado").html(html);
	};
	
	iniciar();
});

