/**
 * 
 */
package Ejercicio08;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		String cadena = "";
		
		// Llenamos la variable cadena
		for(int i = 1; i <= 100;  i++) {
			cadena += i + " ";
		}	
		
		// Imprimimos resultado
		JOptionPane.showMessageDialog(null, cadena);

	}

}
