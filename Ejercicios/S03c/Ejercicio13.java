/**
 * 
 */
package Ejercicio13;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		boolean correct = true;
		int num1, num2, resultado = 0;
		String signo;
		
		// Pedimos los 2 numeros a operar y el signo aritm�tico
		num1 = Integer.parseInt(JOptionPane.showInputDialog("Introduzca primer n�mero: "));
		num2 = Integer.parseInt(JOptionPane.showInputDialog("Introduzca segundo n�mero: "));
		signo = JOptionPane.showInputDialog("Introduzca signo aritm�tico: ").trim().toLowerCase();
		
		// Comprobamos que debemos hacer en cada tipo de signo y operamos en relaci�n
		switch(signo) {
			// SUMA
			case "+":
				resultado = num1 + num2;
				break;
			// DIFERENCIA
			case "-":
				resultado = num1 - num2;
				break;
			// MULTIPLICACION
			case "*":
			case "x":
				resultado = num1 * num2;
				break;
			// COCIENTE
			case "/":
				resultado = num1 / num2;
				break;
			// EXPONENTE
			case "^":
				resultado = (int)Math.pow(num1, num2);
				break;
			// RESTO
			case "%":
				resultado = num1 % num2;
				break;
			// Signos no contemplados
			default:
				// Indicamos que no ha sido correcto
				correct = false;
				JOptionPane.showMessageDialog(null, "Signo aritm�tico no contemplado.");
		}
		
		// Si la introduccion del signo ha sido correcta
		if (correct)
			// Imprimimos resultado
			JOptionPane.showMessageDialog(null, num1 + " " + signo + " " + num2 + " = " + resultado);
			
			

	}

}
