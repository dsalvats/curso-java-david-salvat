/**
 * 
 */
package Ejercicio04;

/**
 * @author David Salvat Sedano
 *
 */
import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int radio;
		double area;
		
		// Captamos radio
		radio = Integer.parseInt(JOptionPane.showInputDialog("Que radio quiere introducir en cm.?"));

		// Calculamos area
		area = (Math.PI * Math.pow(radio, 2));
		
		// Imprimimos resultado
		JOptionPane.showMessageDialog(null, "La �rea con radio " + radio + " es: " + area + " cm.");
		
	}

}
