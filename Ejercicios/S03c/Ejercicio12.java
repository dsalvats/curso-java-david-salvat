	/**
 * 
 */
package Ejercicio12;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		String password = "maxima_seguridad";
		String input;
		
		// Damos 3 intentos para que se introduzca la contrase�a
		for (int i = 0; i < 3; i++) {
			// Pedimos la contrase�a al usuario
			input = JOptionPane.showInputDialog("Introduzca la contrase�a (" + (3 - i) + " intentos restantes): ");
			
			// Comprobamos si la contrase�a es correcta
			if (input.equals(password)) {
				// Al ser correcta la contrase�a lanzamos un mensaje de enhorabuena
				JOptionPane.showMessageDialog(null, "Enhorabuena.");
				break;
			
			// Si la contrase�a es incorrecta lanzamos un mensaje de negativa
			} else
				// Solo si es el �ltimo intento del usuario le recordamos que se ha quedado sin intentos
				if (i == 2)
					JOptionPane.showMessageDialog(null, "Contrase�a incorrecta, se ha quedado sin intentos.");
				else
					JOptionPane.showMessageDialog(null, "Contrase�a incorrecta.");
		}

	}

}
