/**
 * 
 */
package Ejercicio02;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		String nombre = "David";

		// Imprimimos el saludo
		JOptionPane.showMessageDialog(null, "Bienvenido " + nombre);
		
	}

}
