/**
 * 
 */
package Ejercicio10;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declariación de variables
		double total = 0;
		double ventaAux;
		int numVentas;
		
		// Pedimos el numero de ventas
		numVentas = Integer.parseInt(JOptionPane.showInputDialog("Cuantas ventas se debe introducir?"));
		
		// Pedimos la cantidad de cada venta
		for (int i = 0; i < numVentas; i++) {
			ventaAux = Double.parseDouble(JOptionPane.showInputDialog("Cantidad de venta en euros?"));
			total += ventaAux;
		}
			
		// Imprimimos el resultado final
		JOptionPane.showMessageDialog(null, "La venta total es de: " + total + " euros.");
		
	}

}
