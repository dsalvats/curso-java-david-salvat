/**
 * 
 */
package Ejercicio07;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int i = 1;
		String cadena = "";
		
		// Llenamos la variable cadena
		while (i <= 100) {
			cadena += i + " ";
			i++;
		}	
		
		// Imprimimos resultado
		JOptionPane.showMessageDialog(null, cadena);

	}

}
