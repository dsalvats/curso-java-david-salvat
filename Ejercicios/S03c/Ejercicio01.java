
/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Ejercicio01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		int a = 2;
		int b = 3;
		
		// Hacemos las comprobaciones necesarias
		if (a > b)
			JOptionPane.showMessageDialog(null, "El primero es el mayor valor: " + a);
		else if (b > a)
			JOptionPane.showMessageDialog(null, "El segundo es el mayor valor: " + b);
		else
			JOptionPane.showMessageDialog(null, "Los dos valores son iguales: " + a);

	}

}
