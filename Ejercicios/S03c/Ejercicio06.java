/**
 * 
 */
package Ejercicio06;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables y constantes
		final double IVA = 0.21;
		double precio;
		
		// Captamos precio producto
		precio = Double.parseDouble(JOptionPane.showInputDialog("Cual es el precio?"));
		
		// Imprimimos resultado
		JOptionPane.showMessageDialog(null, "El precio final con IVA es: " + (precio*(IVA + 1)));

	}

}
