/**
 * 
 */
package Ejercicio03;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		String nombre;
		
		// Captamos el nombre
		nombre = JOptionPane.showInputDialog("�Cu�l es su nombre?");

		// Imprimimos el saludo
		JOptionPane.showMessageDialog(null, "Bienvenido " + nombre);

	}

}
