/**
 * 
 */
package Ejercicio11;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		String dia;
		
		// Pedimos el d�a al usuario
		dia = JOptionPane.showInputDialog("Qu� d�a desea trabajar?").trim().toLowerCase();
		
		// Para la variable dia comprobamos si es un dia laborable
		// Luego imprimimos el resultado en cada caso
		switch(dia) {
			// DIAS LABORALBES
			case "lunes":
			case "martes":
			case "miercoles":
			case "jueves":
			case "viernes":
				JOptionPane.showMessageDialog(null, dia + " es un d�a laboral.");
				break;
			// DIAS NO LABORALBES
			case "sabado":
			case "domingo":
				JOptionPane.showMessageDialog(null, dia + " no es un d�a laboral.");
				break;
			// FUERA DE RANGO
			default:
				JOptionPane.showMessageDialog(null, dia + " no es un d�a!");
			
		}

	}

}
