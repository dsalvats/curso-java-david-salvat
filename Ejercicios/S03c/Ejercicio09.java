/**
 * 
 */
package Ejercicio09;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		String cadena = "";
		
		// Llenamos la variable cadena
		for (int i = 1; i <= 100; i++)
			// Comprobamos que numeros son multiples de 2 o 3
			if (i % 2 == 0 || i % 3 == 0)
				cadena += i + " ";
		
		// Imprimimos resultado
		JOptionPane.showMessageDialog(null, cadena);
		
	}

}
