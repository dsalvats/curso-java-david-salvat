/**
 * 
 */
package Ejercicio05;

/**
 * @author David Salvat Sedano
 *
 */

import javax.swing.JOptionPane;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int numero;
		
		// Captamos numero a comprobar
		numero = Integer.parseInt(JOptionPane.showInputDialog("Que n�mero desea comprobar?"));

		// Comprobamos e imprimimos resultados
		if (numero % 2 == 0)
			JOptionPane.showMessageDialog(null, "El numero " + numero + " es divisible entre 2.");
		else
			JOptionPane.showMessageDialog(null, "El numero " + numero + " no es divisible entre 2.");
		
		
		
	}

}
