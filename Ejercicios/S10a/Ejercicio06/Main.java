/**
 * 
 */
package Ejercicio06;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Creamos 2 libros
		Libro libro1 = new Libro("9781644730300", "Todo lo que sucedi� con Miranda Huff", "Javier Castillo", 448);
		Libro libro2 = new Libro("9788425357633", "Lo mejor de ir es volver", "Alber Espinosa", 193);

		// Imprimimos por termial la informaci�n de cada libro
		System.out.println(libro1.toString());
		System.out.println(libro2.toString());
		
		// Comparamos cual de los dos libros tiene m�s paginas
		if (libro1.getNumPags() > libro2.getNumPags())
			System.out.println("El libro con m�s paginas es " + libro1.getTitulo() + " con " + libro1.getNumPags() + " p�ginas");
		else if (libro1.getNumPags() < libro2.getNumPags())
			System.out.println("El libro con m�s paginas es " + libro2.getTitulo() + " con " + libro2.getNumPags() + " p�ginas");
		else
			System.out.println(libro1.getTitulo() + " y " + libro2.getTitulo() + " tienen " + libro1.getNumPags() + " p�ginas");
		
	}

}
