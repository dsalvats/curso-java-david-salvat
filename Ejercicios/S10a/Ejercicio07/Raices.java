/**
 * 
 */
package Ejercicio07;

/**
 * @author David Salvat Sedano
 *
 */
public class Raices {

	// ATRIBUTOS
	private static double a;
	private static double b;
	private static double c;
	
	// Imprime las 2 posibles soluciones
	private static void obtenerRaices() {
		// Calculamos las soluciones
		double res1 = (- getB() + Math.sqrt(getDiscriminante())) / (2 * getA());
		double res2 = (- getB() - Math.sqrt(getDiscriminante())) / (2 * getA());

		// Imprimimos soluciones
		System.out.println("Soluci�n 1: " + res1);
		System.out.println("Soluci�n 2: " + res2);
	}
	
	// Imprime �nica ra�z, que ser� cuando solo tenga una soluci�n posible.
	private static void obtenerRaiz() {
		// Calculamos soluci�n
		double res = (- getB()) / (2 * getA());
		
		// Imprime soluci�n
		System.out.println("Soluci�n: " + res);
	}
	
	// Devuelve el valor del discriminante (double), el discriminante
	// Tiene la siguiente formula, (b^2)-4*a*c
	private static double getDiscriminante() {
		return Math.pow(getB(), 2) - 4 * getA() * getC();
	}
	
	// Devuelve un booleano indicando si tiene dos soluciones, para que
	// Esto ocurra, el discriminante debe ser mayor que 0.
	private static boolean tieneRaices() {
		// Comprobamos si el discriminante es mayor que 0
		if (getDiscriminante() > 0)
			return true;
		else
			return false;
	}
	
	// Devuelve un booleano indicando si tiene una �nica soluci�n, para que
	// Esto ocurra, el discriminante debe ser igual que 0
	private static boolean tieneRaiz() {
		if (getDiscriminante() == 0)
			return true;
		else
			return false;
	}
	
	// Mostrara por consola las posibles soluciones que tiene nuestra ecuaci�n,
	// En caso de no existir soluci�n, mostrarlo tambi�n
	public static void calcular(double a, double b, double c) {
		// Asiganmos nuestras variables pasadas por par�metro a los atributos de clase
		setA(a);
		setB(b);
		setC(c);
		
		// Imprimimos la informaci�n pasada
		System.out.println("\nPara A: " + getA() + " B: " + getB() + " C: " + getC());
		
		// Comprobamos si tiene 2, 1 o ninguna soluci�n nuestro resultado y llamamos al m�todo que toca
		if (tieneRaices())
			obtenerRaices();
		else if (tieneRaiz())
			obtenerRaiz();
		else
			System.out.println("No podemos obtener el resultado de la ecuaci�n");
		
		// Espaciamos nuestro resultado
		System.out.println("______________________________________________");
		
	}

	// GETTERS Y SETTERS
	
	/**
	 * @return the a
	 */
	private static double getA() {
		return a;
	}

	/**
	 * @param a the a to set
	 */
	private static void setA(double a) {
		Raices.a = a;
	}

	/**
	 * @return the b
	 */
	private static double getB() {
		return b;
	}

	/**
	 * @param b the b to set
	 */
	private static void setB(double b) {
		Raices.b = b;
	}

	/**
	 * @return the c
	 */
	private static double getC() {
		return c;
	}

	/**
	 * @param c the c to set
	 */
	private static void setC(double c) {
		Raices.c = c;
	}
	
}
