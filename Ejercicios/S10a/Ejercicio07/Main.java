/**
 * 
 */
package Ejercicio07;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Calculamos varios ejemplos para testear nuestra clase Raices
		Raices.calcular(1, 2, 3);
		Raices.calcular(2, 3, 1);
		Raices.calcular(3, 6, 3);
		
	}

}
