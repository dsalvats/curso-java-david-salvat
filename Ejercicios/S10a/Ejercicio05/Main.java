/**
 * 
 */
package Ejercicio05;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		int sumSeriesEntregadas = 0;
		int sumVideojuegosEntregados = 0;
		Serie serieMasLarga = null;
		Videojuego videojuegoMasLargo = null;
		
		// Instanciamos array de series y videojuegos
		Serie[] series = new Serie[5];
		Videojuego[] videojuegos = new Videojuego[5];
		
		// Llenamos la array de series
		series[0] = new Serie();
		series[1] = new Serie("Juego de Tronos", 8, "Acci�n", "HBO");
		series[2] = new Serie("Friends", 10, "Comedia", "");
		series[3] = new Serie("Walking dead", 8, "Acci�n", "");
		series[4] = new Serie();
		
		// Lenamos la array de videojuegos
		videojuegos[0] = new Videojuego();
		videojuegos[1] = new Videojuego("Fortnite", 1000, "Epic Games");
		videojuegos[2] = new Videojuego("Assasin's Creed", 40, "Ubisoft");
		videojuegos[3] = new Videojuego("Fallout 4", 38);
		videojuegos[4] = new Videojuego("League of Legends", 800, "Riot Games");
		
		// Entregamos algunas series y videojuegos
		series[0].entregar();
		series[2].entregar();
		series[4].entregar();
		videojuegos[1].entregar();
		videojuegos[3].entregar();
		
		// Contamos cuantas series hay entregadas y las devolvemos
		for (Serie serie: series)
			if (serie.isEntregado()) {
				sumSeriesEntregadas++;
				serie.devolver();
			}
		
		// Imprimimos resultados
		System.out.println(sumSeriesEntregadas + " series han sido devueltas");
		
		// Contamos cuantos videosjuegos hay entregados y los devolvemos
		for (Videojuego videojuego: videojuegos)
			if (videojuego.isEntregado()) {
				sumVideojuegosEntregados++;
				videojuego.devolver();
			}
		
		// Imprimimos resultados
		System.out.println(sumVideojuegosEntregados + " videojuegos han sido devueltos");
		
		// Comprobamos la serie m�s larga		
		// Recorremos las series para comprobar si hay alguna serie m�s larga que la primera y vamos actualizando
		for (Serie serie: series)
			serieMasLarga = (Serie) serie.compareTo(serieMasLarga);
		
		// Comprobamos el videojuego m�s largo		
		// Recorremos los videojuegos para comprabar si hay algun videojuego m�s largo que el primero y vamos actualizando
		for (Videojuego videojuego: videojuegos)
			videojuegoMasLargo = (Videojuego) videojuego.compareTo(videojuegoMasLargo);
		
		// Imprimimos serie y videojuegos m�s largos
		System.out.println("La serie m�s larga es: " + serieMasLarga.toString());
		System.out.println("El videojuego m�s largo es: " + videojuegoMasLargo.toString());
		
	}

}
