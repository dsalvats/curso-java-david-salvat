/**
 * 
 */
package Ejercicio05;

/**
 * @author David Salvat Sedano
 *
 */
public class Videojuego implements Entregable {

	// CONSTANTES
	private final static String DEFAULT_TITULO = "";
	private final static int DEFAULT_HORAS_ESTIMADAS = 10;
	private final static boolean DEFAULT_ENTREGADO = false;
	private final static String DEFAULT_COMPANIA = "";
	
	// ATRIBUTOS
	private String titulo;
	private int horasEstimadas;
	private boolean entregado;
	private String compania;
	
	// CONSTRUCTORES
	
	/**
	 * @param titulo
	 * @param horasEstimadas
	 * @param entregado
	 * @param compania
	 */
	public Videojuego(String titulo, int horasEstimadas, String compania) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.compania = compania;
	}
	/**
	 * @param titulo
	 * @param horasEstimadas
	 */
	public Videojuego(String titulo, int horasEstimadas) {
		this(titulo, horasEstimadas, DEFAULT_COMPANIA);
	}
	/**
	 * 
	 */
	public Videojuego() {
		this(DEFAULT_TITULO, DEFAULT_HORAS_ESTIMADAS);
	}

	
	@Override
	public String toString() {
		return "Videojuego [titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado
				+ ", compania=" + compania + "]";
	}
	
	// Cambia el atributo prestado a true
	@Override
	public void entregar() {
		this.entregado = true;		
	}
	
	// Cambia el atributo prestado a false
	@Override
	public void devolver() {
		this.entregado = false;
	}
	
	// Devuelve el estado del atributo prestado
	@Override
	public boolean isEntregado() {
		return this.entregado;
	}
	
	// Compara las horas estimadas en los videojuegos y en las series el numero de temporadas
	@Override
	public Object compareTo(Object videojuego) {
		// Si a�n no se ha asignado ningun videojuego m�s largo devolvemos este
		if (videojuego == null)
			return this;
		
		// Comparamos las horas del videojuego pasado con la de este objeto y devolvemos el que tenga m�s
		if (((Videojuego) videojuego).getHorasEstimadas() > getHorasEstimadas())
			return videojuego;
		else
			return this;
	}
	
	// SETTERS Y GETTERS
	
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the horasEstimadas
	 */
	public int getHorasEstimadas() {
		return horasEstimadas;
	}
	/**
	 * @param horasEstimadas the horasEstimadas to set
	 */
	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}
	/**
	 * @return the compania
	 */
	public String getCompania() {
		return compania;
	}
	/**
	 * @param compania the compania to set
	 */
	public void setCompania(String compania) {
		this.compania = compania;
	}
	
}
