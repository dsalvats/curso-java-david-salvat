/**
 * 
 */
package Ejercicio05;

/**
 * @author Casa
 *
 */
public class Serie implements Entregable {

	// CONSTANTES
	private final static String DEFAULT_TITULO = "";
	private final static int DEFAULT_NUM_TEMPORADAS = 3;
	private final static boolean DEFAULT_ENTREGADO = false;
	private final static String DEFAULT_GENERO = "";
	private final static String DEFAULT_CREADOR = "";
	
	// ATRIBUTOS
	private String titulo;
	private int numTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	/**
	 * @param titulo
	 * @param numTemporadas
	 * @param entregado
	 * @param genero
	 * @param creador
	 */
	public Serie(String titulo, int numTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.numTemporadas = numTemporadas;
		this.entregado = DEFAULT_ENTREGADO;
		this.genero = genero;
		this.creador = creador;
	}
	/**
	 * @param titulo
	 * @param creador
	 */
	public Serie(String titulo, String creador) {
		this(titulo, DEFAULT_NUM_TEMPORADAS, DEFAULT_GENERO, creador);
	}
	
	/**
	 * 
	 */
	public Serie() {
		this(DEFAULT_TITULO, DEFAULT_CREADOR);
	}
	
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numTemporadas=" + numTemporadas + ", entregado=" + entregado + ", genero="
				+ genero + ", creador=" + creador + "]";
	}
	
	// Cambia el atributo prestado a true
	@Override
	public void entregar() {
		this.entregado = true;		
	}
	
	// Cambia el atributo prestado a false
	@Override
	public void devolver() {
		this.entregado = false;
	}
	
	// Devuelve el estado del atributo prestado
	@Override
	public boolean isEntregado() {
		return this.entregado;
	}
	
	// Compara las horas estimadas en los videojuegos y en las series el numero de temporadas
	@Override
	public Object compareTo(Object serie) {
		// Si a�n no se ha asignado ninguna serie m�s larga devolvemos esta
		if (serie == null)
			return this;

		// Comparamos las horas de la serie pasada con la de este objeto y devolvemos la que tenga m�s
		if (((Serie) serie).getNumTemporadas() > getNumTemporadas())
			return serie;
		else
			return this;
	}
	
	// SETTERS Y GETTERS
	
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the numTemporadas
	 */
	public int getNumTemporadas() {
		return numTemporadas;
	}
	/**
	 * @param numTemporadas the numTemporadas to set
	 */
	public void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}
	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}
	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}
	/**
	 * @return the creador
	 */
	public String getCreador() {
		return creador;
	}
	/**
	 * @param creador the creador to set
	 */
	public void setCreador(String creador) {
		this.creador = creador;
	}	
	
}
