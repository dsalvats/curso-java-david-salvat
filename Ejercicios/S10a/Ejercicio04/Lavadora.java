package Ejercicio04;

public class Lavadora extends Electrodomestico {

	// CONSTANTES
	private final static double DEFAULT_CARGA = 5.0;

	// ATRIBUTOS
	private double carga;

	// CONSTRUCTORES
	public Lavadora() {
		this(DEFAULT_PRECIO_BASE, DEFAULT_PESO);
	}

	public Lavadora(double precio, double peso) {
		this(precio, DEFAULT_COLOR, DEFAULT_CONSUMO_ENERGETICO, peso, DEFAULT_CARGA);
	}

	public Lavadora(double precioBase, String color, char consumoEnergetico, double peso, double carga) {
		super(precioBase, color, consumoEnergetico, peso);
		this.carga = carga;
	}

	// MÉTODOS

	// Retorna una string con toda la información disponible del objeto
	public String toString() {
		String string = "";
		
		string += "Tipo de electroméstico: Lavadora\n";
		string += super.toString();
		string += "Carga: " + getCarga() + "Kg.\n";
		string += "Precio final: " + precioFinal() + "€\n";
		
		return string;
	}
	
	// Recalcula el precio final dependiendo de las prestaciones de la televisión
	public double precioFinal() {
		double precioFinal = super.precioFinal();

		// Si el peso es mayor de 30gk incrementamos 50 euros al precio
		if (getPeso() > 30.0)
			precioFinal += 50.0;

		return precioFinal;
	}

	/**
	 * @return the carga
	 */
	public double getCarga() {
		return carga;
	}

	/**
	 * @param carga the carga to set
	 */
	public void setCarga(double carga) {
		this.carga = carga;
	}

}
