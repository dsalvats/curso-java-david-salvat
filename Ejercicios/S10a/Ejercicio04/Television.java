package Ejercicio04;

public class Television extends Electrodomestico {

	// CONSTANTES
	private final static double DEFAULT_RESOLUCION = 20.0;
	private final static boolean DEFAULT_SINTONIZADOR_TDT = false;

	// ATRIBUTOS
	private double resolucion;
	private boolean sintonizadorTDT;

	// CONSTRUCTORES
	public Television() {
		this(DEFAULT_PRECIO_BASE, DEFAULT_PESO);
	}

	public Television(double precio, double peso) {
		this(precio, DEFAULT_COLOR, DEFAULT_CONSUMO_ENERGETICO, peso, DEFAULT_RESOLUCION, DEFAULT_SINTONIZADOR_TDT);
	}

	public Television(double precioBase, String color, char consumoEnergetico, double peso, double resolucion,
			boolean sintonizadorTDT) {
		super(precioBase, color, consumoEnergetico, peso);
		this.resolucion = resolucion;
		this.sintonizadorTDT = sintonizadorTDT;
	}
	
	// MÉTODOS
	
	// Retorna una string con toda la información disponible del objeto
	public String toString() {
		String string = "";

		string += "Tipo de electroméstico: Televisión\n";
		string += super.toString();
		string += "Resolución: "  + getResolucion() + "\"\n";
		string += "TDT integrada: ";
		// Comprobamos si esta televisión dispone de sintonizador TDT
		if (isSintonizadorTDT())
			string += "Sí\n";
		else
			string += "No\n";
		string += "Precio final: " + precioFinal() + "€\n";
		
		return string;
	}
	
	// Recalcula el precio final dependiendo de las prestaciones de la televisión
	public double precioFinal() {
		double precioFinal = super.precioFinal(); // Llamamos al resultado que ya teniamos de Electrodoméstico
		
		// Si la resolución es mayor de 40 pulgadas incrementa un 30% su precio
		if (getResolucion() > 40.0)
			precioFinal *= 1.3;
		
		// Si tiene TDT incrementa el precio final en 50 euros
		if (isSintonizadorTDT())
			precioFinal += 50.0;
		
		return precioFinal;
	}

	/**
	 * @return the pulgadas
	 */
	public double getResolucion() {
		return resolucion;
	}

	/**
	 * @param pulgadas the pulgadas to set
	 */
	public void setResolucion(double resolucion) {
		this.resolucion = resolucion;
	}

	/**
	 * @return the sintonizadorTDT
	 */
	public boolean isSintonizadorTDT() {
		return sintonizadorTDT;
	}

	/**
	 * @param sintonizadorTDT the sintonizadorTDT to set
	 */
	public void setSintonizadorTDT(boolean sintonizadorTDT) {
		this.sintonizadorTDT = sintonizadorTDT;
	}
	
	

}
