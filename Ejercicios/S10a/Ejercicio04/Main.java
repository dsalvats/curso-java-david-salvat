/**
 * 
 */
package Ejercicio04;

/**
 * @author David Salvat Sedano
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaración de variables
		Electrodomestico[] electrodomesticos;
		double[] sums;	// Creamos un array de doubles. [0] Sum lavadora [1] Sum televisión [2] Sum electrodoméstico

		// Llenamos el array de electrodomésticos con lavadoras y televisiones
		// Entradas diractamente desde código
		electrodomesticos = fillElectrodomesticos();
		// Imprimimos por terminal el array de electrodomésticos con toda su información disponible
		printElectrodomesticos(electrodomesticos);

		// Calculamos los sumatorios de precios de lavadoras, televisiones y electrodomésticos
		sums = getSums(electrodomesticos);
		// Imprimimos los sumatorios de lavadoras, televisiones y electrodomésticos
		printSums(sums);
	}
	
	// Imprimime los sumatorios de lavadoras, televisiones y electrodomésticos
	private static void printSums(double[] sums) {
		System.out.println("Precio total lavadoras: " + sums[0] + "€\n" // Imprime el sumatorio de las lavadoras
						+ "Precio total televisiones: " + sums[1] + "€\n" // Imprime el sumatorio de las televisiones
						+ "Precio total electrodomésticos: " + sums[2] + "€"); // Imprime el sumatorio de los electrodomésticos
	}
	
	// Devuelve un array de 3 posiciones con los sumatorios de precios finales de lavadoras, televisiones y electrodomésticos
	private static double[] getSums(Electrodomestico[] electrodomesticos) {
		// Declaración de variables
		double[] sums = { 0.0, 0.0, 0.0 }; // Array de doubles para guardar los sumatorios
		double auxPrecioFinal; // Double para guardar con el precio final con el que operaremos
		
		// Recorremos todos los electrodomésticos pasados por parámetro
		for (Electrodomestico electrodomestico: electrodomesticos) {
			// Obtenemos el valor del precio final del electrodoméstico con el que operamos
			auxPrecioFinal = electrodomestico.precioFinal();
			
			// Comprobamos si ese electrodoméstico es una lavadora
			if (electrodomestico instanceof Lavadora)
				// Si es una lavadora lo sumamos en la posición de las lavadoras
				sums[0] += auxPrecioFinal;
			// Comprobamos si ese electrodoméstico es una televisión
			else if (electrodomestico instanceof Television)
				// Si es una televisión lo sumamos en la posición de las televisiones
				sums[1] += auxPrecioFinal;
			
			// Sumamos al precio final de los electrodomésticos
			sums[2] += auxPrecioFinal;
		}
		
		return sums;
	}
	
	// Imprime la información para cada electrodoméstico de los electrodomésticos pasados por parámetro
	private static void printElectrodomesticos(Electrodomestico[] electrodomesticos) {
		// Declaración de variables
		int i = 1; // Índice para saber en que posición nos encontramos
		
		// Recorremos el array de electrodomésticos
		for(Electrodomestico electrodomestico: electrodomesticos) {
			String string = "";
			
			string += "Electrodoméstico nº: " + i++ + "\n"; // Imprime en que posición de electrodoméstico nos encontramos
			string += electrodomestico.toString(); // Obtenemos la String con toda la información disponible del electrodoméstico
			string += "_____________________________\n";
			
			System.out.println(string);
		}
	}
	
	private static Electrodomestico[] fillElectrodomesticos() {
		// Declaración de variables
		Electrodomestico[] electrodomesticos = new Electrodomestico[10];
		
		// Llenamos el array de electrodomésticos
		electrodomesticos[0] = new Lavadora();
		electrodomesticos[1] = new Lavadora(120, 20.0);
		electrodomesticos[2] = new Lavadora(200, 30.0);
		electrodomesticos[3] = new Lavadora(150.0, "NEGRO", 'B', 25.0, 4.5);
		electrodomesticos[4] = new Lavadora(250.0, "ROSA", 'C', 30.5, 7.5);
		electrodomesticos[5] = new Television();
		electrodomesticos[6] = new Television(200.0, 10.2);
		electrodomesticos[7] = new Television(300.0, 12.8);
		electrodomesticos[8] = new Television(200.0, "ROJO", 'A', 10.2, 35.0, false);
		electrodomesticos[9] = new Television(400.0, "FUCSIA", 'A', 90.0, 45.5, true);
		
		return electrodomesticos;
	}

}
