package Ejercicio04;

public class Electrodomestico {

	// CONSTANTES
	protected final static double DEFAULT_PRECIO_BASE = 100.0;
	protected final static String DEFAULT_COLOR = "BLANCO";
	protected final static char DEFAULT_CONSUMO_ENERGETICO = 'F';
	protected final static double DEFAULT_PESO = 5.0;
	
	protected final static char[] POSIBLES_CONSUMO_ENERGETICO = {'A', 'B', 'C', 'D', 'E', 'F'};
	protected final static String[] POSIBLES_COLOR = {"BLANCO", "NEGRO", "ROJO", "AZUL", "GRIS"};

	// ATRIBUTOS
	protected double precioBase;
	protected String color;
	protected char consumoEnergetico;
	protected double peso;

	// CONSTRUCTORES
	public Electrodomestico() {
		this(DEFAULT_PRECIO_BASE, DEFAULT_PESO);
	}

	public Electrodomestico(double precioBase, double peso) {
		this(precioBase, DEFAULT_COLOR, DEFAULT_CONSUMO_ENERGETICO, peso);
	}

	public Electrodomestico(double precioBase, String color, char consumoEnergetico, double peso) {
		this.precioBase = precioBase;
		comprobarColor(color);
		comprobarConsumoEnergetico(consumoEnergetico);
		this.peso = peso;
	}

	// M�TODOS

	// Retorna una string con toda la informaci�n disponible del objeto
	public String toString() {// Declaraci�n de variables
		String string = "";
		
		// Vamos concatenando toda la informaci�n que tenemos del electrodomestico a la string
		string += "Precio base: " + getPrecioBase() + "�\n";
		string += "Color: " + getColor() + "\n";
		string += "Tipo consumo energ�tico: " + getConsumoEnergetico() +  "\n";
		string += "Peso: " + getPeso() + "Kg.\n";
		
		return string;
	}
	
	// Comprueba que la letra es correcta
	// Sino es correcta usara la letra por defecto
	private void comprobarConsumoEnergetico(char consumoEnergetico) {
		// Recorremos los consumos energeticos disponibles
		// Y vamos comparando intentando encontrar una coincidencia
		// En caso de coincidencia asignamos el consumo energ�tico y salimos
		for (char consumoEnergeticoAux: POSIBLES_CONSUMO_ENERGETICO)
			if (consumoEnergeticoAux  == consumoEnergetico) {
				this.consumoEnergetico = consumoEnergetico;
				return;
			}
		
		// Si no se ha encontrado ninguna coincidencia asignamos por defecto
		this.consumoEnergetico = DEFAULT_CONSUMO_ENERGETICO;
	}

	// Comprueba que el color es correcto,
	// Sino lo es usa el color por defecto
	private void comprobarColor(String color) {
		// Recorremos los colores disponibles
		// Y vamos comparando intentando encontrar una coincidencia
		// En caso de coincidencia asignamos el color
		for (String colorAux: POSIBLES_COLOR)
			if (colorAux.equals(color.toUpperCase())) {
				this.color = color.toUpperCase();
				return;
			}
		
		// Si no se ha encontrado ninguna coincidencia asignamos por defecto
		this.color = DEFAULT_COLOR;
	}

	// Seg�n el consumo energ�tico, aumentara su precio, y seg�n su tama�o, tambi�n
	public double precioFinal() {
		double precioFinal = getPrecioBase();

		// Incrementar precio final dependiendo0 de su consumo energ�tico
		switch (getConsumoEnergetico()) {
		case 'A':
			precioFinal += 100.0;
			break;
		case 'B':
			precioFinal += 80.0;
			break;
		case 'C':
			precioFinal += 60.0;
			break;
		case 'D':
			precioFinal += 50.0;
			break;
		case 'E':
			precioFinal += 30.0;
			break;
		case 'F':
			precioFinal +=  10.0;
			break;
		}
		
		// Incrementar precio final dependiendo de su peso
		if (getPeso() < 20)
			precioFinal += 10.0;
		else if (getPeso() >= 20.0 && getPeso() < 50.0)
			precioFinal += 50.0;
		else if (getPeso() >= 50.0 && getPeso() < 79.0)
			precioFinal += 80.0;
		else
			precioFinal += 100.0;
		
		return precioFinal;
	}

	/**
	 * @return the precioBase
	 */
	public double getPrecioBase() {
		return precioBase;
	}

	/**
	 * @param precioBase the precioBase to set
	 */
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the consumoEnergetico
	 */
	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	/**
	 * @param consumoEnergetico the consumoEnergetico to set
	 */
	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}

	/**
	 * @return the peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}

}
