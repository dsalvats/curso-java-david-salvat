package eXercice_Cine;

import java.util.ArrayList;

public class Asiento {

	// Constantes default
	private final static Espectador DEFAULT_ESPECTADOR = null;
	private final static int DEFAULT_FILA = 0;
	private final static char DEFAULT_COLUMNA = 'A';

	// Atributos
	private Espectador espectador;
	private int fila;
	private char columna;

	// Constructores

	/**
	 * @param espectador
	 * @param fila
	 * @param columna
	 */
	public Asiento(Espectador espectador, int fila, char columna) {
		this.espectador = espectador;
		this.fila = fila;
		this.columna = columna;
	}

	/**
	 * @param fila
	 * @param columna
	 */
	public Asiento(int fila, char columna) {
		this(DEFAULT_ESPECTADOR, fila, columna);
	}

	/**
	 * 
	 */
	public Asiento() {
		this(DEFAULT_FILA, DEFAULT_COLUMNA);
	}

	// M�todos

	// Devuelve si nuestro asiento esta vac�o
	public boolean isVacio() {
		if (getEspectador() == null)
			return true;
		else
			return false;
	}

	// Asigna un espectador al asiento
	public boolean asignarEspectador(Espectador espectador) {
		if (isVacio()) {
			this.espectador = espectador;
			return true;
		} else
			return false;

	}

	// Devuelve el numero de asientos vacios en la array list
	public static int numAsientosVacios(ArrayList<Asiento> asientos) {
		// Declaraci�n de variables
		int sum = 0;

		// Recorremos los asientos comprobando cuales estan ocupados
		for (Asiento asiento : asientos)
			if (asiento.isVacio())
				sum++;

		return sum;
	}

	//Devuelve una string con la informaci�n de asiento
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String string = "Fila: " + getFila() + "\n"
				+"Columna: " + getColumna() + "\n";
		if (getEspectador() == null)
			string += "Asiento vac�o";
		else
			string += getEspectador().toString();

		return string;
	}

	// Getters & Setters

	/**
	 * @return the espectador
	 */
	public Espectador getEspectador() {
		return espectador;
	}

	/**
	 * @param espectador the espectador to set
	 */
	public void setEspectador(Espectador espectador) {
		this.espectador = espectador;
	}

	/**
	 * @return the fila
	 */
	public int getFila() {
		return fila;
	}

	/**
	 * @param fila the fila to set
	 */
	public void setFila(int fila) {
		this.fila = fila;
	}

	/**
	 * @return the columna
	 */
	public char getColumna() {
		return columna;
	}

	/**
	 * @param columna the columna to set
	 */
	public void setColumna(char columna) {
		this.columna = columna;
	}

}
