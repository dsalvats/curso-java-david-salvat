package eXercice_Cine;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Cine {

	// Constantes
	private final static double DEFAULT_PRECIO_ENTRADA = 6.5;

	// Atributos
	private ArrayList<Sala> salas;
	private double precioEntrada;
	private static ArrayList<Pelicula> peliculasDisponibles;
	private double facturacion;
	private int entradasVendidas;

	// Constructores
	public Cine(double precioEntrada) {
		this.precioEntrada = precioEntrada;
		this.facturacion = 0.0;
		this.entradasVendidas = 0;
		salas = new ArrayList<>();
		peliculasDisponibles = inicializarPeliculas();
	}

	public Cine() {
		this(DEFAULT_PRECIO_ENTRADA);
	}

	// M�TODOS

	// Consultamos que salas hay para consultar
	public void consultarSalas() {
		// Declaramos variables
		int addReserva;
		
		// Pedimos al usuario que sala quiere consultar
		Sala sala = pedirSala();
		Asiento asiento = pedirAsiento(sala);
		
		// Generamos la opci�n de generar una reserva al mostrar la sala
		if (asiento.isVacio()) {
			addReserva  = JOptionPane.showConfirmDialog(null, asiento.toString() 
					+ "\n\nReservar este asiento?", null, JOptionPane.YES_NO_OPTION);
			
			// Si confirma la reserva...
			if (addReserva == 0) {
				Espectador espectador = new Espectador();
				// Se muestra la informaci�n del espectador aleatorio asignado
				sala.hacerReserva(espectador, false, true, asiento);
				// A�adimos una entrada mas vendida
				realizarVenta();
				JOptionPane.showMessageDialog(null, "Asiento reservado:\n" + asiento.toString());
			}
		} else {
			JOptionPane.showMessageDialog(null, asiento.toString());
		}

	}
	
	// Retorna el asiento seleccionado
	public Asiento pedirAsiento(Sala sala) {
		// Declaramos variables
		boolean correcto;
		String posicion = "";
		Object[] posicionFinal = { null, null };
		Asiento asiento = null;
		
		// Mostramos la sala y generamos la opci�n de consultar un asiento 
		do {
			correcto = true;
			try {
				// Leemos qu� asiento desea consultar
				posicion = JOptionPane.showInputDialog(sala.drawAsientos() + "\n\n"
						+ "Que asiento desea consultar?").toUpperCase().trim();
				
				// Datos de fila y columna
				posicionFinal[0] = Integer.parseInt(posicion.substring(0, posicion.length() - 1));
				posicionFinal[1] = posicion.charAt(posicion.length() -  1);
				
				//Si el asiento no existe lanzamos error
				if (!sala.asientoExists(posicionFinal))
					throw new Exception();
				
			// Si los datos introducidos son err�neos se muestra el mensaje de error
			} catch (Exception e) {
				System.out.println(posicion + "\n" + posicionFinal[0] + "\n" + posicionFinal[1]);
				JOptionPane.showMessageDialog(null, "La posici�n introducida es incorrecta.");
				correcto = false;
			}
		} while (!correcto);
		
		// Recorremos los asientos hasta que encontramos el asiento introducido previamente
		for (Asiento asientoAux: sala.getAsientos())
			if (asientoAux.getFila() == (int) posicionFinal[0] && asientoAux.getColumna() == (char) posicionFinal[1])
				asiento = asientoAux;
		
		return asiento;
		
	}
	
	// Pide al usuario la sala a mostrar
	public Sala pedirSala() {
		// Declaraci�n de variables
		boolean correcto = true;
		int id = 0;

		// Conseguimos el n�mero de la sala
		do {
			correcto = true;
			try {
				id = Integer.parseInt(JOptionPane.showInputDialog(mostrarSalas() + "\n\n"
						+ "Introducir id de la sala a mostrar?"));
				// En caso de error se muestra este mensaje
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "La id debe ser num�rica entre las opciones mostradas.");
				correcto = false;
			}
			// En caso de error se muestra este mensaje
			if (id < 1 || id > getSalas().size()) {
				JOptionPane.showMessageDialog(null, "La id debe ser num�rica entre las opciones mostradas.");
				correcto = false;
			}
		} while (!correcto);

		return getSalas().get(id - 1);
	}
	
	// Devuelve una string con las salas y sus peliculas creadas
	public String mostrarSalas() {
		// Declaraci�n de variables
		String string = "";
		int i = 1;
		
		// Mostramos la informaci�n de la sala y la informaci�n de las pel�culas creadas
		for (Sala sala: getSalas()) {
			string += i++ + ") " + sala.toCompactString() + "\n";
		}
		
		return string;
	}
	
	public void printVentas() {
		// Mostramos la recaudaci�n y entradas vendidas
		JOptionPane.showMessageDialog(null, "Se han vendido: " + getEntradasVendidas() + " entradas\n"
				+ "Se han recaudado: " + getFacturacion() + "�");
	}
	
	// Realizamos proceso de venta de entrada
	public void realizarVenta() {
		// A�adimos una venta
		entradasVendidas++;

		// A�adimos el precio de la entrada en la facturaci�n total del cine
		setFacturacion(getFacturacion() + getPrecioEntrada());
	}

	// Devolvemos un array list con peliculas predefinidas
	public ArrayList<Pelicula> inicializarPeliculas() {
		// Creamos una arraylist de pel�culas
		ArrayList<Pelicula> peliculas = new ArrayList<>();

		// PELICULA 1
		peliculas.add(new Pelicula("Los vengadores", 189, 13, "Stan Lee"));

		// PELICULA 2
		peliculas.add(new Pelicula("El Se�or de los Anillos", 148, 18, "Petter Jackson"));

		// PELICULA 3
		peliculas.add(new Pelicula("Blancanieves", 121, 7, "Disney"));
		
		// PELICULA 4
		peliculas.add(new Pelicula("Jumanji", 78, 0, "Joe Johnson"));

		return peliculas;
	}

	// Creamos una sala pidiendo la informaci�n al usuario
	public void crearSala() {
		// Declaraci�n de variables
		int numFilas = Sala.pedirNumFilas(); // Guardamos las filas que la sala tendr�
		int numColumnas = Sala.pedirNumColumnas(); // Guardamos las columnas que la sala tendr�
		int numSala = Sala.pedirNumSala(); // Guardamos el n�mero que la sala tendr�
		Pelicula pelicula = pedirPelicula(); // Guardamos la pel�cula que la sala reproducir�

		// Creamos una sala y la a�adimos a nuestra array list de salas
		Sala sala = new Sala(numSala, numFilas, numColumnas, pelicula);
		salas.add(sala);
	}

	// Pedimos al usuario la pelicula a reproducir
	public static Pelicula pedirPelicula() {
		// Declaraci�n de variables
		boolean correcto = true;
		int id = 0;

		// Pedimos al usuario que introduzca la pel�cula 
		do {
			correcto = true;
			try {
				id = Integer.parseInt(JOptionPane.showInputDialog(Pelicula.toString(getPeliculasDisponibles()) + "\n"
						+ "Introducir id de pel�cula a reproducir?"));
				
			//En caso de error se muestra el mensaje
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "La id debe ser num�rica entre las opciones mostradas.");
				correcto = false;
			}

			// Si se introduce un valor inv�lido se muestra el error
			if (id < 1 || id > getPeliculasDisponibles().size()) {
				JOptionPane.showMessageDialog(null, "La id debe ser num�rica entre las opciones mostradas.");
				correcto = false;
			}
		} while (!correcto);

		return getPeliculasDisponibles().get(id - 1);
	}

	// SETTERS & GETTERS

	/**
	 * @return the salas
	 */
	public ArrayList<Sala> getSalas() {
		return salas;
	}

	/**
	 * @param salas the salas to set
	 */
	public void setSalas(ArrayList<Sala> salas) {
		this.salas = salas;
	}

	/**
	 * @return the precioEntrada
	 */
	public double getPrecioEntrada() {
		return precioEntrada;
	}

	/**
	 * @param precioEntrada the precioEntrada to set
	 */
	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	/**
	 * @return the peliculasDisponibles
	 */
	public static ArrayList<Pelicula> getPeliculasDisponibles() {
		return peliculasDisponibles;
	}

	/**
	 * @param peliculasDisponibles the peliculasDisponibles to set
	 */
	public void setPeliculasDisponibles(ArrayList<Pelicula> peliculasDisponibles) {
		Cine.peliculasDisponibles = peliculasDisponibles;
	}

	/**
	 * @return the facturacion
	 */
	public double getFacturacion() {
		return facturacion;
	}

	/**
	 * @param facturacion the facturacion to set
	 */
	public void setFacturacion(double facturacion) {
		this.facturacion = facturacion;
	}

	/**
	 * @return the entradasVendidas
	 */
	public int getEntradasVendidas() {
		return entradasVendidas;
	}

	/**
	 * @param entradasVendidas the entradasVendidas to set
	 */
	public void setEntradasVendidas(int entradasVendidas) {
		this.entradasVendidas = entradasVendidas;
	}

}
