package eXercice_Cine;

import com.github.javafaker.Faker;

public class Espectador {

	// Constantes
	private final static int MAX_RND_AGE = 70;
	private final static int MIN_RND_AGE = 5;

	// Atributos
	private String nombre;
	private int edad;

	/**
	 * @param nombre
	 * @param edad
	 */
	public Espectador(String nombre, int edad) {
		this.nombre = nombre;
		this.edad = edad;
	}

	/**
	 * @param nombre
	 */
	public Espectador(String nombre) {
		this.nombre = nombre;
		this.edad = generarEdadAleatoria();
	}

	/**
	 * @param edad
	 */
	public Espectador(int edad) {
		this.nombre = generarNombreAleatorio();
		this.edad = edad;
	}

	/**
	 * 
	 */
	public Espectador() {
		this.nombre = generarNombreAleatorio();
		this.edad = generarEdadAleatoria();
	}

	// Métodos

	// Devuelve un booleano para saber si el espectador es apto
	// para ver la pelicula pasada por parámetro
	public boolean comprobarEdad(Pelicula pelicula) {
		if (getEdad() >= pelicula.getEdadMinima())
			return true;
		return false;
	}

	// Devuelve un nombre generado aleatoriamente
	private String generarNombreAleatorio() {
		return new Faker().name().fullName();
	}

	// Retorna una edad aleatoria entre las constantes asignadas al objeto
	private int generarEdadAleatoria() {
		return (int) (Math.random() * (MAX_RND_AGE - MIN_RND_AGE) + MIN_RND_AGE);
	}

	// Generamos un espectador aleatorio
	public static Espectador generarEspectador() {
		return new Espectador();
	}

	// Devuelve una string con la información del espectador
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Nombre: " + getNombre() + "\n" + "Edad: " + getEdad();
	}

	// Getters & Setters

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}

	/**
	 * @param edad the edad to set
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}

}
