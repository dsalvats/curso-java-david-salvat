package eXercice_Cine;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Simulador {

	// Atributos
	ArrayList<Espectador> espectadoresAleatorios;
	Cine cine;

	public void iniciar() {

		// Crear cine
		cine = new Cine();

		// Crear sala
		cine.crearSala();

		// Crear N espectadores aleatorios
		setEspectadoresAleatorios(fillEspectadoresAleatorios(pedirEspectadoresAleatorios()));

		// Reservar aleatoriamente espectador a asiento
		rellenarAsientos();

		// Pedir acci�n
		do {
			menuHandler(printMenu());
		} while (true);

	}

	// Reservar aleatoriamente espectador a asiento
	private void rellenarAsientos() {
		// Declaramos variables
		int code;
		
		// Recorremos espectadores aleatorios
		for (Espectador espectador : getEspectadoresAleatorios()) {
			// Asignar espectador a asiento aleatorio disponible
			code = cine.getSalas().get(0).hacerReserva(espectador, true, false, null);
			if (code == 0)
				// Sumar facturaci�n de entrada
				cine.realizarVenta();
			else if (code == 2)
				// Si no se ha podido realizar la operaci�n salimos
				break;
		}
	}
	
	// Llenamos de espectadores nuestra array list de espectadores aleatorios
	private ArrayList<Espectador> fillEspectadoresAleatorios(int num) {
		// Inicializamos arraylist de espectadores aleatorios
		ArrayList<Espectador> espectadoresAleatorios = new ArrayList<>();
		
		// A�adimos cantidad de espectadores pasada por par�metro a la arraylist
		for (int i = 0; i < num; i++)
			espectadoresAleatorios.add(Espectador.generarEspectador());

		return espectadoresAleatorios;
	}

	// Imprime el men� y retorna la opci�n introducida por el usuario
	private int printMenu() {
		// Declaraci�n de variables
		int opcion = -1;

		// Mostrar men� infinito
		do {
			try {
				opcion = Integer.parseInt(JOptionPane.showInputDialog(""
						+ "1) Consultar estado de la sala\n"
						+ "2) Consultar facturaci�n\n" 
						+ "\n" 
						+ "0) Salir"));
			} catch (Exception e) {
			} finally {
				if (opcion > 3 || opcion < 0)
					JOptionPane.showMessageDialog(null, "La opci�n introducida debe ser entre las posible mostradas.");
			}
		} while (opcion == -1);

		return opcion;
	}

	// Controla las opciones entradas en el men�
	private void menuHandler(int opcion) {

		switch (opcion) {
		// CONSULTAR SALA
		case 1:
			cine.consultarSalas();
			// Preguntar codigo a consultar
			break;
		// CONSULTAR FACTURACI�N
		case 2:
			cine.printVentas();
			break;
		// SALIR
		case 0:
			Runtime.getRuntime().exit(0);
			break;
		}

	}

	// Pedir n�mero de espectadores aleatorios
	public static int pedirEspectadoresAleatorios() {
		// Declaraci�n de variables
		boolean correcto = true;
		int espectadoresAleatorios = 0;

		// Pedimos n�mero de espectadores aleatorios
		do {
			correcto = true;

			try {
				espectadoresAleatorios = Integer.parseInt(JOptionPane.showInputDialog(""
						+ "Cuantos espectadores aleatorios quieres generar?"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"El n�mero de espectadores aleatorios debe ser num�rico.");
				correcto = false;
			}
			
		} while (!correcto);

		return espectadoresAleatorios;
	}
	
	// GETTERS & SETTERS

	/**
	 * @return the espectadoresAleatorios
	 */
	private ArrayList<Espectador> getEspectadoresAleatorios() {
		return espectadoresAleatorios;
	}

	/**
	 * @param espectadoresAleatorios the espectadoresAleatorios to set
	 */
	private void setEspectadoresAleatorios(ArrayList<Espectador> espectadoresAleatorios) {
		this.espectadoresAleatorios = espectadoresAleatorios;
	}

}
