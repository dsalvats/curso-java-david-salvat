package eXercice_Cine;

import java.util.ArrayList;

public class Pelicula {

	// Atributos
	private String titulo;
	private int duracion;
	private int edadMinima;
	private String director;

	// Constructores
	public Pelicula(String titulo, int duracion, int edadMinima, String director) {
		this.titulo = titulo;
		this.duracion = duracion;
		this.edadMinima = edadMinima;
		this.director = director;
	}

	// Metodos
	// Mostramos la información en una String
	public String toString() {
		return getTitulo() + " - " + getDuracion() + "m. (" + getEdadMinima() + ")";
	}

	// Devolvemos una string con las películas pasadas por parámetro
	public static String toString(ArrayList<Pelicula> peliculas) {
		// Declaración de variables
		String string = "";
		int i = 1;

		// Recorremos las peliculas pasadas por parámetro y las concatenamos a la string
		for (Pelicula pelicula : peliculas)
			string += i++ + ") " + pelicula.toString() + "\n";

		return string;
	}

	// Getters & Setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getEdadMinima() {
		return edadMinima;
	}

	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

}
