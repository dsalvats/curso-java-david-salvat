package eXercice_Cine;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Sala {

	// Constantes DEFAULT
	private final static int DEFAULT_NUM_SALA = 1;
	private final static int DEFAULT_NUM_FILAS = 11;
	private final static int DEFAULT_NUM_COLUMNAS = 15;
	private final static Pelicula DEFAULT_PELICULA = null;

	// Constantes MINIMOS y MAXIMOS
	private final static int MIN_FILAS = 1;
	private final static int MIN_COLUMNAS = 1;
	private final static int MIN_NUM_SALA = 1;
	private final static int MAX_FILAS = 25;
	private final static int MAX_COLUMNAS = 25;
	private final static int MAX_NUM_SALA = 25;

	// Atributos
	private ArrayList<Asiento> asientos;
	private int numSala;
	private Pelicula pelicula;

	// Constructores
	public Sala(int numSala, int numFilas, int numColumnas, Pelicula pelicula) {
		this.numSala = numSala;
		this.pelicula = pelicula;
		asientos = new ArrayList<>();
		addAsientos(numFilas, numColumnas);
	}

	// Acabar de implementar constructor
	public Sala(ArrayList<Asiento> asientos, int numSala) {
		this.asientos = asientos;
		this.numSala = numSala;
	}

	public Sala() {
		this(DEFAULT_NUM_SALA, DEFAULT_NUM_FILAS, DEFAULT_NUM_COLUMNAS, DEFAULT_PELICULA);
	}

	// M�TODOS

	// Hacemos el proceso de una reserva para un espectador y una sala pasados
	public int hacerReserva(Espectador espectador, boolean aleatorio, boolean imprimir, Asiento asiento) {
		// Si no hay asientos libres se mostrar� este mensaje
		if (!(Asiento.numAsientosVacios(asientos) > 0)) {
			if (imprimir)
				JOptionPane.showMessageDialog(null, "La sala tiene todos los asientos reservados");
			return 2;
		}

		// Comprobamos si el espectador puede ver la pelicula por su edad
		if (!espectador.comprobarEdad(getPelicula())) {
			if (imprimir)
				JOptionPane.showMessageDialog(null,
						"La pel�cula '" + getPelicula().getTitulo() + "' (" + getPelicula().getEdadMinima()
								+ ") no es apta para el espectador (" + espectador.getEdad() + ")");
			return 1;
		}

		if (aleatorio)
			// Asignamos el espectador que hace la reserva en un asiento aleatorio vacio
			asignarEspectadorEnAsientoAleatorio(espectador);
		else
			// Si no lo asignamos, le asignamos el asiento introducido
			asignarEspectadorEnAsiento(espectador, asiento);

		return 0;
	}

	// Asignamos aleatoriamente un espectador en un asiento vac�o de la sala
	private void asignarEspectadorEnAsiento(Espectador espectador, Asiento asiento) {
		// Intentamos asignar el espectador en una posici�n de la sala
		// hasta que se encuentre un asiento vac�o
			if (asiento.isVacio())
				asiento.asignarEspectador(espectador);
				
	}

	// Asignamos aleatoriamente un espectador en un asiento vac�o de la sala
	private void asignarEspectadorEnAsientoAleatorio(Espectador espectador) {
		// Declaraci�n de variables
		int numAleatorio = 0;
		boolean ocupado = true;

		// Intentamos asignar el espectador en una posici�n de la sala
		// hasta que se encuentre un asiento vac�o
		do {
			numAleatorio = (int) (Math.random() * (getAsientos().size()));

			if (getAsientos().get(numAleatorio).isVacio()) {
				ocupado = false;
				getAsientos().get(numAleatorio).asignarEspectador(espectador);
			}

		} while (ocupado);
	}

	// Pedimos al usuario la fila donde sentarse
	public Object[] pedirPosicion() {
		// Declaraci�n de variables
		boolean correcto;
		String posicion;
		Object[] posicionFinal = { null, null };

		// Conseguimos el n�mero de filas de la sala
		do {

			correcto = true;

			try {
				// Pedimos al usuario que introduzca el asiento deseado
				posicion = JOptionPane.showInputDialog(drawAsientos() + "\n\nEn que posici�n desea sentarse?")
						.toUpperCase().trim();

				posicionFinal[0] = posicion.charAt(posicion.length() - 1);
				posicionFinal[1] = Integer.parseInt(posicion.substring(0, posicion.length() - 2));

				// Si el asiento introducido no existe ejecuta el mensaje de error
				if (!asientoExists(posicionFinal))
					throw new Exception();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "La posici�n introducida es incorrecta.");
				correcto = false;
			}

		} while (!correcto);

		return posicionFinal;
	}

	// Dibuja los asientos en la sala
	public String drawAsientos() {
		// Declaraci�n de variables
		String string = "<html>"; // Vamos a concatenar el c�digo html que generemos
		int ultimaFila = 0; // Guardaremos la �ltima fila procesada
		String filaAux;
		int sumVacios = Asiento.numAsientosVacios(asientos);

		// Y dibujamos los asientos:
		string += "N�mero asientos disponibles: <font color='green'>" + sumVacios + "</font><br/>";
		string += "N�mero asientos ocupados: <font color='red'>" + (getAsientos().size() - sumVacios)
				+ "</font><br/><br/><hr/>";
		string += "<font color='blue'><center>PANTALLA</center></font><hr/>";

		// Abrimos tabla
		string += "<table>";

		// Recorremos los asientos para ir imprimimendo su informaci�n en string
		for (Asiento asiento : getAsientos()) {
			// Comprobamos si debemos a�adir un 0 al n�mero de fila
			if (asiento.getFila() < 10)
				// A�adimos un 0 a la fila
				filaAux = "0" + asiento.getFila();
			else
				filaAux = "" + asiento.getFila();

			// Comprobamos si debemos hacer un salto de l�nea
			if (asiento.getFila() > ultimaFila) {
				// Generamos salto de linea
				string += "</tr><tr>|";
				ultimaFila = asiento.getFila();
			}

			// Comprobamos si el asiento esta vac�o
			if (asiento.isVacio())
				// A�adimos el asiento en color verde
				string += "<td><font color='green'>" + filaAux + asiento.getColumna() + "</font></td>|";
			else
				// A�adimos el asiento en color rojo
				string += "<td><font color='red'>" + filaAux + asiento.getColumna() + "</font></td>|";
		}

		// Cerramos la tabla que abrimos
		string += "</table><hr/>";

		return string;
	}

	// Comprueba si un asiento existe
	public boolean asientoExists(Object[] posicion) {
		// Comprovamos la posici�n de los asientos
		for (Asiento asiento : getAsientos())
			if (asiento.getFila() == (int) posicion[0] && asiento.getColumna() == (char) posicion[1])
				return true;

		return false;
	}

	// Pedimos al usuario el n�mero de columnas
	public static int pedirNumColumnas() {
		// Declaraci�n de variables
		boolean correcto = true;
		int numColumnas = 0;

		// Conseguimos el n�mero de columnas
		do {
			correcto = true;

			// Pedimos el n�mero de columnas deseadas
			try {
				numColumnas = Integer.parseInt(JOptionPane.showInputDialog("Cuantas columnas tiene la sala?"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"El n�mero columnas debe ser num�rico entre " + MIN_COLUMNAS + " y " + MAX_COLUMNAS + ".");
				correcto = false;
			}

			// Si el valor introducido es err�neo ejecutamos el erorr
			if (numColumnas < MIN_COLUMNAS || numColumnas > MAX_COLUMNAS) {
				JOptionPane.showMessageDialog(null,
						"El n�mero columnas debe ser num�rico entre " + MIN_COLUMNAS + " y " + MAX_COLUMNAS + ".");
				correcto = false;
			}
		} while (!correcto);

		return numColumnas;
	}

	// Pedimos al usuario el n�mero de filas
	public static int pedirNumFilas() {
		// Declaraci�n de variables
		boolean correcto = true;
		int numFilas = 0;

		// Conseguimos el n�mero de filas de la sala
		do {
			correcto = true;
			try {
				numFilas = Integer.parseInt(JOptionPane.showInputDialog("Cuantas filas tiene la sala?"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"El n�mero de filas debe ser num�rico entre " + MIN_FILAS + " y " + MAX_FILAS + ".");
				correcto = false;
			}

			if (numFilas < MIN_FILAS || numFilas > MAX_FILAS) {
				JOptionPane.showMessageDialog(null,
						"El n�mero de filas debe ser num�rico entre " + MIN_FILAS + " y " + MAX_FILAS + ".");
				correcto = false;
			}
		} while (!correcto);

		return numFilas;
	}

	// Pedimos al usuario el n�mero de sala
	public static int pedirNumSala() {
		// Declaraci�n de variables
		boolean correcto = true;
		int numSala = 0;

		// Conseguimos el n�mero de la sala
		do {
			correcto = true;
			try {
				numSala = Integer.parseInt(JOptionPane.showInputDialog("Que n�mero de sala quieres?"));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,
						"El n�mero de la sala debe ser num�rico entre " + MIN_NUM_SALA + " y " + MAX_NUM_SALA + ".");
				correcto = false;
			}

			if (numSala < MIN_NUM_SALA || numSala > MAX_NUM_SALA) {
				JOptionPane.showMessageDialog(null,
						"El n�mero de la sala debe ser num�rico entre " + MIN_NUM_SALA + " y " + MAX_NUM_SALA + ".");
				correcto = false;
			}
		} while (!correcto);

		return numSala;
	}

	// A�adimos todos los asientos hasta llenar el n�mero de filas y columas pasados
	public void addAsientos(int numFilas, int numColumnas) {
		for (int i = 0; i < numFilas; i++) {
			for (int j = 0; j < numColumnas; j++) {
				asientos.add(new Asiento(i + 1, (char) (j + 65)));
			}
		}
	}

	// Devuelve la informaci�n de la sala en string
	public String toString() {
		String string = "";

		string += "N�mero de sala: " + getNumSala() + "\n";
		string += "N�mero de asientos: " + getAsientos().size() + "\n";
		string += "Pelicula reproducida: " + getPelicula().toString() + "\n";

		return string;
	}

	// Devuelve la inforamci�n de la sala en una sola linea
	public String toCompactString() {
		return "Sala " + getNumSala() + ": " + getPelicula().getTitulo();
	}

	// Getter && Setters
	public ArrayList<Asiento> getAsientos() {
		return asientos;
	}

	public void setAsientos(ArrayList<Asiento> asientos) {
		this.asientos = asientos;
	}

	public int getNumSala() {
		return numSala;
	}

	public void setNumSala(int numSala) {
		this.numSala = numSala;
	}

	/**
	 * @return the minFilas
	 */
	public static int getMinFilas() {
		return MIN_FILAS;
	}

	/**
	 * @return the minColumnas
	 */
	public static int getMinColumnas() {
		return MIN_COLUMNAS;
	}

	/**
	 * @return the minNumSala
	 */
	public static int getMinNumSala() {
		return MIN_NUM_SALA;
	}

	/**
	 * @return the maxFilas
	 */
	public static int getMaxFilas() {
		return MAX_FILAS;
	}

	/**
	 * @return the maxColumnas
	 */
	public static int getMaxColumnas() {
		return MAX_COLUMNAS;
	}

	/**
	 * @return the maxNumSala
	 */
	public static int getMaxNumSala() {
		return MAX_NUM_SALA;
	}

	/**
	 * @return the pelicula
	 */
	public Pelicula getPelicula() {
		return pelicula;
	}

	/**
	 * @param pelicula the pelicula to set
	 */
	public void setPelicula(Pelicula pelicula) {
		this.pelicula = pelicula;
	}

}
