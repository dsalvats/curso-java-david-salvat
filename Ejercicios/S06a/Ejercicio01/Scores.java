package Ejercicio01;

import java.util.ArrayList;
import java.util.Iterator;

// Clase wrapper para poder almacenar el conjunto de notas para un alumno y su nota media actualizada
public class Scores {
	
	// ATRIBUTOS
	public ArrayList<Double> scores;
	public double averageScore;;
	
	// CONSTRUCTORES
	public Scores(Double[] scores) {
		// Llamamos a nuestro propio constructor default
		this();
		
		// Recorremos las notas pasadas por par�metro y las vamos a�adiendo
		for (int i = 0; i < scores.length; i++)
			this.scores.add(scores[i]);
		
		// Recalculamos las notas media del estudiante
		this.recalculateAvgScore();
	}
	
	public Scores() {
		this.scores = new ArrayList<Double>();
		this.averageScore = 0.0;
	}
	
	// Recalcula las notas media del estudiante
	public void recalculateAvgScore() {
		// Declaraci�n de variables
		double sum = 0.0;
		
		// Creamos un iterador para ir recorriendo las notas del estudiante
		Iterator<Double> it = this.scores.iterator();
		
		// Recorremos las notas hasta que no haya mas
		while (it.hasNext())
			// A�adimos la nota seleccionada al sumatorio
			sum += it.next();
		
		// Calculamos la nota media con el sumatorio y la cantidad de notas introducidas y lo guardamos
		this.averageScore = sum / this.scores.size();
		
	}
	
}
