/**
 * 
 */
package Ejercicio01;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.JOptionPane;

/* Crea una aplicaci�n que calcule la nota media de los
 * alumnos pertenecientes al curso de programaci�n. Una vez
 * calculada la nota media se guardara esta informaci�n en un
 * diccionario de datos que almacene la nota media de cada
 * alumno. Todos estos datos se han de proporcionar por
 * pantalla.
 */

/**
 * @author David Salvat Sedano
 *
 */
public class Ejercicio01 {

	// Declaraci�n de variables 
	private static Hashtable<String, Scores> students = new Hashtable<String, Scores>();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		/* **********************************************************************************************************
		 * En este ejercicio se ha querido mostrar que el preprocesamiento es importante para la escalabilidad
		 * de un programa o conjunto de ellos.
		 * En este caso se ha utilizado para controlar los I/O de las notas de los estudiantes e ir 
		 * actualizando las notas media de estos mismo para no tener que hacer cada vez que accediamos 
		 * a ellos o quer�amos calcular las notas de toda la clase
		 * **********************************************************************************************************/

		// Llenamos inicialmente de alumnos nuestro Hashtable students globales con valores introducidos desde c�digo
		testPopulation();
		
		// Creamos un bucle para que el programa no finalice nunca
		while (true)
			// Imprimimos men� y obtenemos opci�n introducida por el usuario
			// Y controlamos su opci�n seleccionada
			handler(getMenuOption());
		
	}
	
	// Imprimimos el men� con las opciones disponibles y retornamos la opci�n introducida por el usuario
	private static int getMenuOption() {
		return Integer.parseInt(JOptionPane.showInputDialog(""
				+ "Qu� desea hacer? [0-4]\n"
				+ "\n"
				+ "1) A�adir un alumno\n"
				+ "2) A�adir notas a un alumno\n"
				+ "3) Modificar notas a un alumno\n"
				+ "4) Eliminar notas a un alumno\n"
				+ "5) Imprimir los nombres de todos los alumnos\n"
				+ "6) Imprimir las notas de un alumno espec�fico\n"
				+ "7) Imprimir notas media de todos los alumnos\n"
				+ "8) Imprimir la nota media de toda la clase\n"
				+ "\n"
				+ "0) Salir\n\n"));
	}
	
	// Controlamos la opci�n introducida por el usuario y llamamos a los m�todos necesarios
	private static boolean handler(int opcion) {
		boolean correct = true;
		
		switch (opcion) {
			// A�ADE UN ALUMNO
			case 1:
				addStudent();
			break;
			// PIDE A�ADIR M�S NOTAS PARA UN ALUMNO
			case 2:
				addStudentScores();
				break;
			// PIDE QUE NOTAS MODIFICAR A UN ALUMNO
			case 3:
				updateScore();
				break;
			// PIDE QUE NOTAS ELIMINAR A UN ALUMNO
			case 4:
				deleteScore();
				break;
			// MUESTRA LOS ALUMNOS INTRODUCIDOS
			case 5:
				printStudentNames();
				break;
			// IMPRIME NOTAS ACTUALES PARA UN ALUMNO ESPECIFICO
			case 6:
				printStudent();
				break;
			// IMPRIME ALUMNOS ACTUALES Y SUS NOTAS MEDIA
			case 7:
				printStudents();
				break;
			// IMPRIME LA NOTA MEDIA DE TODA LA CLASE
			case 8:
				printClassRoom();
				break;
			// SALIR
			case 0:
				Runtime.getRuntime().exit(0);
				break;
			// OPCI�N NO CONTEMPLADA 
			default:
				JOptionPane.showMessageDialog(null, "ERROR: Valor introducido incorrecto. [0-4]");
		}
		
		return correct	;	
	}

	// Imprime todos los nombres de los estudiantes guardados
	private static void printStudentNames() {
		JOptionPane.showMessageDialog(null, getStudentNames());
	}
	
	// Retorna en una string todos los nombres de los estudiantes guardados
	private static String getStudentNames() {
		Enumeration<String> enumeration = 	students.keys();	// Creamos un enumeration para poder recorrer los estudiantes guardados
		String 								string = "";		// String donde iremos concatenando los nombres de los estudiantes
		int 								i = 1;				// �ndice para saber en que posici�n de alumno estamos
		
		// Recorremos los estudiantes guardados
		while(enumeration.hasMoreElements())
			// Concatenamos la posici�n del estudiante y su nombre
			string += "[" + i++ + "] " + enumeration.nextElement() + "\n";
		
		return string;
	}

	// Eliminamos una nota espec�fica para un alumno en espec�fico
	private static void deleteScore() {
		String 				fullName;				// Nombre del alumno a borrarle una nota
		String 				scoresString 	= "";	// String donde concatenaremos las notas disponibles para alumno en espec�fico
		ArrayList<Double> 	scores;					// Notas disponibles para alumno en espec�fico
		int 				i				= 1;	// �ndice para saber en que posici�n de nota estamos
		int 				index;					// �ndice de la nota a borrar
		double				aux;					// Variable auxiliar para guardar la nota que borraremos
		
		// Intentamos obtener el nombre del alumno hasta que comprobamos que ese existe
		do
			fullName = JOptionPane.showInputDialog("A qu� alumno desea borrar una nota?\n\n" + getStudentNames() + "\n\n");
		while (!studentFullNameExists(fullName));
		
		// Obtenemos todas las notas para el estudiante seleccionado
		Scores student = students.get(fullName);
		scores = student.scores;
		
		// Creamos iterador para recorrer las notas del estudiante seleccionado
		Iterator<Double> it = scores.iterator();
		
		// Recorremos todas las notas hasta que no encontremos m�s
		while(it.hasNext()) {
			// Concatenamos la posici�n y el valor de cada nota en una string
			scoresString += "[" + i++ + "] " + it.next() + "\n";
		}
		
		// Intentamos obtener del usuario el �ndice de la nota a borrar hasta que este sea v�lido
		do
			index = Integer.parseInt(JOptionPane.showInputDialog("Que nota desea borrar?\n\n" + scoresString));
		while (index < 1 || index > scores.size());
		
		// Borramos de las notas la nota seleccionada y recalculamos la media de nota del estudiante
		aux = scores.get(index - 1);
		scores.remove(index - 1);
		student.recalculateAvgScore();
		
		// Imprimimos un mensaje de confirmaci�n de que la nota ha sido borrada satisfactoriamente
		JOptionPane.showMessageDialog(null, "La nota [" + index + "] " + aux + " se ha borrado correctamente.");
	}
	
	// Modificamos una nota espec�fica para un alumno en espec�fico
	private static void updateScore() {
		String 				fullName;				// Nombre del alumno a modificar nota
		String 				scoresString 	= "";	// String donde concatenaremos las notas disponibles para alumno en espec�fico
		ArrayList<Double> 	scores;					// Notas disponibles para alumno en espec�fico
		int 				i				= 1;	// �ndice para saber en que posici�n de nota estamos
		int 				index;					// �ndice de la nota a modificar
		double				aux;					// Variable auxiliar para guardar la nota que modificaremos
		double				nota;					// Nueva nota a introducir

		// Intentamos obtener el nombre del alumno hasta que comprobamos que ese existe
		do {
			fullName = JOptionPane.showInputDialog("A qu� alumno desea modificar una nota?\n\n" + getStudentNames() + "\n\n");
		} while(!studentFullNameExists(fullName));

		// Obtenemos todas las notas para el estudiante seleccionado
		Scores student = students.get(fullName);
		scores = student.scores;

		// Creamos iterador para recorrer las notas del estudiante seleccionado
		Iterator<Double> it = scores.iterator();

		// Recorremos todas las notas hasta que no encontremos m�s
		while(it.hasNext())
			// Concatenamos la posici�n y el valor de cada nota en una string
			scoresString += "[" + i++ + "] " + it.next() + "\n";

		// Intentamos obtener del usuario el �ndice de la nota a modificar hasta que este sea v�lido
		do
			index = Integer.parseInt(JOptionPane.showInputDialog("Que nota desea modificar?\n\n" + scoresString));
		while (index < 1 || index > scores.size());

		// Intentamos obtener del usuario la nueva nota a introducir hasta que este sea v�lida
		do
			nota = Double.parseDouble(JOptionPane.showInputDialog("Que nota desea entrar?\n\n"));
		while (nota < 0.0 || nota > 10.0);

		// Setteamos la nueva nota introducida y recalculamos la media de nota del estudiante
		aux = scores.get(index - 1);
		scores.set(index - 1, nota);
		student.recalculateAvgScore();
		
		// Imprimimos un mensaje de confirmaci�n con la nota anterior modificada y la nueva nota introducida
		JOptionPane.showMessageDialog(null, "La nota [" + index + "] " + aux + " se ha actualizado correctamente por " + "[" + index + "] " + nota + ".");
	}
	
	// A�adimos un estudiante al Hashtable de nuestros estudiantes
	private static void addStudent() {
		String	fullName;		// Nombre del estudiante a guardar
		String	aux;			// Variable para obtener si el usuario quiere introducir un nuevo alumno
		boolean	newAdd = true;	// Variable para comprobar si el usuario quiere introducir un nuevo alumno 
		
		// Vamos introduciendo alumnos hasta que el usuario no quiera introducir ning�n alumno m�s
		while(newAdd) {
			// Obtenemos el nombre del alumno que usuario quiere introducir
			fullName = JOptionPane.showInputDialog("Introducir nombre de alumno nuevo:");
		
			// Comprobamos si el alumno ya existe
			if (!studentFullNameExists(fullName))
				// Si no existe lo introducimos e inicializamos sus notas
				students.put(fullName, new Scores());
			else
				// Si existe mandamos mensaje de error
				JOptionPane.showMessageDialog(null, "Ese alumno ya existe.");
			
			// Obtenemos si el usuario quiere introducir nuevos alumnos
			aux = JOptionPane.showInputDialog("Desea introducir un nuevo alumno? [Si / No]").trim().toUpperCase();
			
			// Comprobamos la decisi�n de introducir un nuevo alumno
			if (aux.equals("NO"))
				// Si el usuario no quiere introducir m�s alumnos setteamos newAdd como false y el bucle while parar� de recorrerse
				newAdd = false;
		}
	}
	
	// Comprobamos si el nombre del estudiante pasado por par�metro existe o no
	private static boolean studentFullNameExists(String fullName) {
		// Declaraci�n de variables
		Enumeration<String> enumeration = students.keys(); // Generemoa un enumeration para poder recorrer los estudiantes
		
		// Recorremos los estudiantes guardados
		while (enumeration.hasMoreElements())
			// Comprobamos si el nombre del estudiante pasado por par�metro es el que tenemos seleccionado
			if (fullName.equals(enumeration.nextElement()))
				// Si es el mismo nombre devolvemos true y salimos del bucle
				return true;
		
		// Si no se ha encontramo ninguna coincidencia devolvemos false
		return false;
	}
	
	// A�ade notas a un estudiante en espec�fico
	private static void addStudentScores() {
		String	fullName; 		// Nombre del estudiante a a�adir notas
		int		aux;			// Variable para obtener si el usuario quiere introducir m�s notas
		double	nota;			// Nueva nota a entrar al estudiante
		boolean newAdd = true;	// Variable para comprobar si el usuario quiere introducir m�s notas
		
		// Vamos introduciendo notas hasta que el usuario decida que no
		while(newAdd) {
		
			// Intentamos obtener nombre del alumno hasta comprobar que este existe
			do
				// Obtenemos nobmre del alumno a a�adirle notas
				fullName = JOptionPane.showInputDialog(getStudentNames() 
						+ "\n\nA qu� alumno desea a�adirle notas?");
			while (!studentFullNameExists(fullName));
			
			// Intentamos obtener notas del usuario hasta comprobar que �s valida
			do {
				nota = Double.parseDouble(JOptionPane.showInputDialog("Nota a a�adir [0-10]:"));
			} while(nota < 0.0 || nota > 10.0);
				
			// Obtenemos notas del alumno
			Scores score = students.get(fullName);
			// A�adimos nueva nota introducida a sus notas
			score.scores.add(nota);
			// Recalculamos la nota media para ese alumno
			score.recalculateAvgScore();
			
			// Obtenemos si el usuario desea introducir m�s notas al alumno
			aux = JOptionPane.showConfirmDialog(null, "Desea introducir una nueva nota?", "Informaci�n", JOptionPane.YES_NO_OPTION);
			
			if (aux != 0)
				newAdd = false;
			
		}
	}

	// Imprimime la nota media de toda la clase
	private static void printClassRoom() {
		// Declaraci�n de variables
		double sum = 0.0; // Sumatorio de las notas de toda la clase
		
		// Creamos un enumeration con las notas de los estudiantes para poder recorrer
		Enumeration<Scores> scores = students.elements();
		
		// Mientras tengamos m�s notas seguimos recorriendo
		while(scores.hasMoreElements())
			// Sumamos la nota media que hab�amos preprocesado previamente
			sum += scores.nextElement().averageScore;
		
		// Imprimimos la nota media de toda la clase
		JOptionPane.showMessageDialog(null, "La nota media de toda la clase es: " + (sum / students.size()));
		
	}

	// Imprimime toda la informaci�n disponible para un estudiante
	private static void printStudent() {
		ArrayList<Double>	scores;					// Notas del estudiante seleccionado
		String				fullName;				// Nombre del estudiante a seleccionar
		String				string		= "[ ";		// String donde concataremos todaa la informaci�n del estudiante
		double				avgScore;				// Nota media del alumno seleccionado
		
		// Obtenemos nombre del estudiante a imprimir
		fullName = JOptionPane.showInputDialog(null, getStudentNames() 
				+ "\n\nDe que alumno quiere ver sus notas?");
		
		// Obtenemos las notas del estudiante seleccionado
		scores = students.get(fullName).scores;
		
		// Creamos iterador para poder recorrer las notas del estudiante
		Iterator<Double> 	it = scores.iterator();
		
		// Recorremos las notas del estudiante hasta que no encontremos m�s
		while (it.hasNext())
			// Concatenamos la nota seleccionada a las dem�s
			string += " " + it.next();
		
		// Obtenemos la nota media preprocesada del estudiante seleccionado
		avgScore = students.get(fullName).averageScore;
		// Concatenamos la nota media a las demas
		string += " ]\nNota media: " + avgScore;
		
		// Imprimimos la informaci�n para el estudiante
		JOptionPane.showMessageDialog(null, string);
	}

	private static void printStudents() {
		String 	aux;					// Variable para almacenar los estudiantes que recorremos
		String 	string			= "";	// String para ir concatenando toda la informaci�n
		double 	sum				= 0.0;	// Sumatorio de la nota media de toda la clase
		double	auxAvgScore;			// Variable para almacenar la nota media del estudiante que recorremos
		double 	totalAvgScore	= 0.0;	// Nota media de toda la clase
		int		i				= 1;	// �ndice para saber en que posici�n esta cada estudiante
		
		// Creamos un enumeration para recorrer los estudiantes
		Enumeration<String> enumeration = students.keys();
		
		// Recorremos los estudiantes hasta que no encontremos m�s
		while (enumeration.hasMoreElements()) {
			// Guardamos en una variable auxiliar el estudiante con el que operar
			aux = enumeration.nextElement();
			// Nota media del estudiante con el que operamos
			auxAvgScore = students.get(aux).averageScore;
			
			// A�adimos esa nota al sumatorio de la nota media de clase
			sum += auxAvgScore;
			
			// Concatenamos la informaci�n del estudiante
			string += "[" + i + "] " + aux + ": " + auxAvgScore + "\n";
			
			// Incrementamos en uno el �ndice de la posici�n del estudiante
			i++;
		}
		
		// Calculamos la nota media de la clase con el sumatorio y la cantidad de alumnos
		totalAvgScore = sum / students.size();
		
		// Concatenamos esa nota media final de clase
		string += "\nNota media de toda la clase: " + totalAvgScore + "\n\n";
		
		// Imprimimos toda la informaci�n obtenida previamente
		JOptionPane.showMessageDialog(null, string);
	}
	
	// Llena inicialmente el Hashtable de estudiantes
	private static void testPopulation() {
		students.put("David", 	new Scores(new Double[] { 6.0, 7.0, 8.0 }));
		students.put("Fran", 	new Scores(new Double[] { 2.0, 3.0, 4.0 }));
		students.put("Alicia", 	new Scores(new Double[] { 4.0, 5.0, 6.0 }));
	}

}
