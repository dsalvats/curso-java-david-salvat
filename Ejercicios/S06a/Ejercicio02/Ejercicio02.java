/**
 * 
 */
package Ejercicio02;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JOptionPane;

/**
 * @author Team 2
 *
 */

/*
 * Crea una aplicaci�n que gestione el flujo de ventas de una caja de
 * supermercado. El programa guardara la cantidades del carrito de compra dentro
 * de una lista. Mostrar� por pantalla la siguiente informacion: � IVA aplicado
 * (21% o 4%) � precio total bruto y precio mas IVA. � Numero de art�culos
 * comprados. � Cantidad pagada. � Cambio a devolver al cliente.
 */

public class Ejercicio02 {

	// Declaraci�n de variables globales
	private static ArrayList<Hashtable<String, String>> cart = new ArrayList<>();
	private static ArrayList<Hashtable<String, String>> list = new ArrayList<>();
	private static DecimalFormat df2 = new DecimalFormat("#.##");

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declaraci�n de variables
		double cambio = 0.0;
		double pagado = 0.0;
		String listString;

		// A�adir productos a carrito
		addInitialProducts();

		// Recorrer carrito
		fillList();

		// Conseguimos pasar los productos de lista a string en formato ticket
		listString = listToArray();

		// Imprimos el ticket
		// Pedir dinero a pagar
		pagado = Double.parseDouble(JOptionPane.showInputDialog(listString + "\n\nCuanto desea pagar?"));

		// Mostrar cambio
		cambio = calcularCambio(pagado);
		JOptionPane.showMessageDialog(null, "El cambio es de: " + df2.format(cambio) + " euros");

	}

	// A�adimos inicialmente X productos al carrito 
	private static void addInitialProducts() {
		// Declaraci�n de constantes
		final String IVA_4 = "4";
		final String IVA_21 = "21";
		
		// Declaraci�n de variables
		Hashtable<String, String> product = new Hashtable<String, String>();

		// PRODUCTO TEST 1
		product.put("id", "1");
		product.put("nombre", "Pan");
		product.put("IVA", IVA_4);
		product.put("precio", "0.75");

		cart.add(product);
		
		// PRODUCTO TEST 2
		product = new Hashtable<String, String>();

		product.put("id", "2"); 
		product.put("nombre", "Huevos");
		product.put("IVA", IVA_21);
		product.put("precio", "1.5");
		
		cart.add(product);

		// PRODUCTO TEST 3
		product = new Hashtable<String, String>();

		product.put("id", "3"); 
		product.put("nombre", "Leche");
		product.put("IVA", IVA_4); 
		product.put("precio", "1.13");
		
		cart.add(product);

		// PRODUCTO TEST 4
		product = new Hashtable<String, String>();

		product.put("id", "4"); 
		product.put("nombre", "Tomates");
		product.put("IVA", IVA_21);
		product.put("precio", "0.17");
		
		cart.add(product);
		
		
	}

	// Preguntamos las cantidades de los proudctos del carrito
	// Y los vamos a�adiendo a la lista final con sus cantidades
	private static void fillList() {
		// Declaraci�n de variables
		int cantidad = 0;
		int id = 0;
		Hashtable<String, String> producto;
		
		// Mientras la id seleccionada no corresponda a la opci�n de salir
		do {
			// Imprimir producto
			// Preguntar id a a�adir cantidad
			id = Integer.parseInt(JOptionPane.showInputDialog(cartToString() + "\n\n"
					+ "[0] Salir\n\n"
					+ "Introduce la ID del producto a a�adir cantidad 0\n\n"));
			
			if (id != 0) { 
				// Seleccionamos el producto y lo guardamos en una variable auxiliar
				producto = cart.get(id - 1);
				
				// Preguntar cantidades
				cantidad = Integer.parseInt(JOptionPane.showInputDialog("Cuantos '" + producto.get("nombre") + "' has comprado?\n\n"
						+ "Si no has comprado pon 0\n\n"));
				// A�adir producto y cantidades a lista
				producto.put("num", String.valueOf(cantidad));
				list.add(producto);
			}
		} while (id != 0);
	}

	// Pasamos el carrito a string con nuestro formato
	private static String cartToString() {
		// Declaraci�n de variables
		String string = "";
		
		
		// Recorremos toda el carrito
		for(Hashtable<String, String> product: cart) {
			// Concatenamos el producto como string
			string += "[" + product.get("id") + "]: " + product.get("nombre") + "\n"; 
		}
		
		return string;
	}
	
	// Pasamos la lista a string con formato ticket
	private static String listToArray() {
		// Declaraci�n de variables
		String string = "";
		double sumBruto = 0.0;
		double sumIVA = 0.0;
		int numProds = 0;
		
		// Recorremos toda la lista guardada anteriormente
		for(Hashtable<String, String> product: list) {
			// Hacemos los sumatorios del precio bruto e IVA
			sumBruto +=  Double.valueOf(product.get("num")) * Double.valueOf(product.get("precio"));
			sumIVA += sumBruto * Double.valueOf(product.get("IVA")) / 100;
			numProds += Double.valueOf(product.get("num"));
			
			// Concatenamos el producto como string
			string += product.get("nombre") + " " + product.get("num") + " x " 
			+ df2.format(Double.parseDouble(product.get("precio"))) + " (" 
			+ product.get("IVA") + "%)\n"; 
		}
		
		// Concatenamos precios finales
		string += "_______________________\n\n"
				+ "N�mero de productos total: " + numProds + " productos\n"
				+ "Precio total bruto: " + df2.format(sumBruto) + " euros\n"
				+ "Precio total IVA: " + df2.format(sumIVA) + " euros\n"
				+ "Precio TOTAL: " + df2.format(sumBruto + sumIVA) + " euros";
		
		return string;
	}
	
	// Retorna el cambio dado una cantidad pagada
	private static double calcularCambio(double pagado) {
		// Declaraci�n de variables
		double cambio;
		double sumBruto = 0.0;
		double sumIVA = 0.0;
		
		// Recorremos los productos de la lista para poder tener un valor total
		for(Hashtable<String, String> product: list) {
			sumBruto +=  Double.valueOf(product.get("num")) * Double.valueOf(product.get("precio"));
			sumIVA += sumBruto * Double.valueOf(product.get("IVA")) / 100;
		}
			
		// Calculamos el cambio de retornar
		cambio = pagado - (sumBruto + sumIVA);
		
		return cambio;
	}

}