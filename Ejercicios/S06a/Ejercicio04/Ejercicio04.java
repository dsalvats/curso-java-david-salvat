/**
 *
 */
package Ejercicio04;
 
import javax.swing.JOptionPane;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
 
/**
 * @author Rub�n Jim�nez, Adam Vives, David Salvat
 *
 */
public class Ejercicio04 {
 
	// Declaramos constantes globales
	private final static int IVA_SUPERREDUCED = 4;
	private final static int IVA_REDUCED = 10;
	private final static int IVA_GENERAL = 21;
	
	// Declaramos variables globales
    private static ArrayList<Hashtable<String, Object>> stockProducts = new ArrayList<>();
    private static ArrayList<ArrayList<Hashtable<String, Object>>> purchaseListHistory = new ArrayList<>();
    private static double totalCash = 0.0;
    private static int maxProductId = -1;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
   
    /**
     * @param args
     */
    public static void main(String[] args) {
 
        // Crear productos iniciales
        createInitialStockProducts();
       

        while(true) {
            // Imprime men� principal
            mainHandler(mainMenu());
        }
 
    }
   

	// Crea los productos iniciales en stockProducts
    private static void createInitialStockProducts() {
        // Declaraci�n de variables
    	Hashtable<String, Object> product;
    	
    	// PRODUCTO 1
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "TOMATE");
    	product.put("price", 0.17);
    	product.put("iva", IVA_SUPERREDUCED);
    	product.put("num", 245);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 2
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "TOMATE CHERRY");
    	product.put("price", 0.12);
    	product.put("iva", IVA_SUPERREDUCED);
    	product.put("num", 881);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 3
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "PAN");
    	product.put("price", 0.81);
    	product.put("iva", IVA_SUPERREDUCED);
    	product.put("num", 42);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 4
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "GAFAS DE SOL");
    	product.put("price", 20.95);
    	product.put("iva", IVA_GENERAL);
    	product.put("num", 8);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 5
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "GAFAS GRADUADAS");
    	product.put("price", 79.95);
    	product.put("iva", IVA_REDUCED);
    	product.put("num", 4);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 6
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "ACEITE DE OLIVA");
    	product.put("price", 3.59);
    	product.put("iva", IVA_GENERAL);
    	product.put("num", 56);
    	
    	stockProducts.add(product);
    	
    	// PRODUCTO 7
    	product = new Hashtable<>();
    	
    	product.put("id", nextProductId());
    	product.put("name", "BOLSA DE PLASTICO");
    	product.put("price", 0.10);
    	product.put("iva", IVA_GENERAL);
    	product.put("num", 5412);
    	
    	stockProducts.add(product);
    }
 
    // Controla las opciones introducidas en el men� principal
    private static void mainHandler(int option) {
        int auxOption;
       
        switch(option) {
            // ACCEDER MEN� ALMAC�N
            case 1:
                do {
                    auxOption = warehouseMenu();
                    warehouseHandler(auxOption);
                } while (auxOption != 0);
                break;
            // ACCEDER MEN� CAJA REGISTRADORA
            case 2:
                do {
                    auxOption = cashRegisterMenu();
                    cashRegisterHandler(auxOption);
                } while (auxOption != 0);
                break;
            // SALIR
            case 0:
                break;
            // FUERA DE RANGO
            default:
                JOptionPane.showMessageDialog(null, "Opci�n no contemplada.");
        }
       
    }
   
    // Controla las opciones introducidas en el men� del almac�n
    private static void warehouseHandler(int option) {
        switch (option) {
            // CONSULTAR PRODUCTO
            case 1:
                queryProduct();
                break;
            // CONSULTAR TODOS LOS PRODUCTOS
            case 2:
                checkAllProducts();
                break;
            // CREAR PRODUCTO
            case 3:
                addProduct();
                break;
            // MODIFICAR PRODUCTO
            case 4:
                updateProduct();
                break;
            // BORRAR PRODUCTO
            case 5:
                deleteProduct();
                break;
            // ATRAS
            case 0:
                break;
            // FUERA DE RANGO
            default:
                JOptionPane.showMessageDialog(null, "Opci�n no contemplada");
        }
    }
   
    private static void deleteProduct() {
    	// Declaraci�n de variables
    	Hashtable <String, Object> product;
    	int confirm;
    	
    	// Seleccionamos producto a eliminar
    	product = selectProduct(null);
    	
    	// Pedimos confirmaci�n al usaurio
    	confirm = JOptionPane.showConfirmDialog(null, "Desea eliminar " + product.get("name"), null, JOptionPane.YES_NO_OPTION);
    	
    	if (confirm == 0)
	    	// Eliminamos producto seleccionado
			stockProducts.remove(product);
	}

	private static void updateProduct() {
    	// Declaraci�n de variables
		Hashtable<String, Object> product;
		int updateElement;
		String updateInfo;
		
		// Seleccionamos producto a modificar
		product = selectProduct(null);
		
		if (product == null)
			return;
		
		// Pedir elemento a modificar
		updateElement = Integer.parseInt(JOptionPane.showInputDialog(""
				+ "1) Nombre\n"
				+ "2) Precio\n"
				+ "3) IVA\n"
				+ "4) Cantidad\n"
				+ "\n"
				+ "0) Salir"));
		
		// Comprobamos elemento seleccionado a modificar
		// Pedimos nueva informaci�n
		// Y la modificamos en el producto
		switch (updateElement) {
			// MODIFICAR NOMBRE
			case 1:
				updateInfo = JOptionPane.showInputDialog(product.get("name") + " es el nombre actual.\n\n"
						+ "Introduce nuevo nombre:\n\n").trim().toUpperCase();
				
				product.put("name", updateInfo);
				break;
			// MODIFICAR PRECIO
			case 2:
				updateInfo = JOptionPane.showInputDialog(product.get("price") + " es el precio actual.\n\n"
						+ "Introduce nuevo precio:\n\n").trim().toUpperCase();
				
				product.put("price", Double.parseDouble(updateInfo));
				break;
			// MODIFICAR IVA
			case 3:
				updateInfo = JOptionPane.showInputDialog(product.get("iva") + " es el IVA actual.\n\n"
						+ "Introduce nuevo IVA:\n\n").trim().toUpperCase();
				
				product.put("iva", updateInfo);
				break;
			// MODIFICAR CANTIDAD EN STOCK
			case 4:
				updateInfo = JOptionPane.showInputDialog(product.get("num") + " es la cantidad actual.\n\n"
						+ "Introduce nueva cantidad:\n\n").trim().toUpperCase();
				
				product.put("num", updateInfo);
				break;
			// SALIR MENU
			case 0:
				return;
			// OPCI�N NO CONTEMPLADA
			default:
				JOptionPane.showMessageDialog(null, "Elemento no contemplado.");
				return;
		}		
		
	}

	private static void checkAllProducts() {
		// Declaraci�n de variables
    	String string = "";
    	
    	// Recorremos todos los productos en stock
    	// Y vamos concatenando su informaci�n
    	for(Hashtable<String, Object> product: stockProducts)
    		string += productToReducedString(product);
    	
    	// Imprimimo la string concatenado con toda la informaci�n del stock
    	JOptionPane.showMessageDialog(null, string);
		
	}

	// Consultamos la informaci�n de un �nico producto
    private static void queryProduct() {
        // Seleccionamos el producto deseado
        Hashtable<String, Object> product = selectProduct(null);
       
        // En el caso de que hayamos encontrado alg�n producto
        if (product != null)
        // Imprimimos el producto seleccionado
            JOptionPane.showMessageDialog(null, productToString(product));
    }
   
    // Seleccionamos un �nimo producto por ID, Nombre completo o parte del nombre
    private static Hashtable<String, Object> selectProduct(String stringProducts) {
        // Declaraci�n de variables
        ArrayList<Hashtable<String, Object>> auxProducts;
        Hashtable<String, Object> product;
        String input;
       
        // Pedir ID, Nombre o parte del nombre
        if (stringProducts == null)
            input = JOptionPane.showInputDialog("Introduce la ID, Nombre completo o parte del nombre del producto:\n\n"+"Escribe SALIR para ir al carrito").trim().toUpperCase();
        else
            input = JOptionPane.showInputDialog(stringProducts + "\n\nIntroduce la ID, Nombre completo o parte del nombre del producto:\n\n").trim().toUpperCase();        
       
        // Si es num�rico, buscamos como ID
        if (isNumeric(input)) {
            product = getProductById(Integer.parseInt(input));
           
            if (product == null)
                JOptionPane.showMessageDialog(null, "No se ha encontrado ning�n producto con esa ID.");
           
        // Si es string buscamos cuantos productos contienen esa parte de nombre
        } else {
            auxProducts = getProductsByName(input);
           
            // Si solo tenemos 1 lo seleccionamos automaticamente
            if (auxProducts.size() == 1) {
                product = auxProducts.get(0);
               
            // Si tenemos varios imprimimos con sus ids
            } else if (auxProducts.size() > 1) {
                stringProducts = "";
 
                // Recorremos todos los prooductos encontrado que contienen parte de ese nombre
                // Y vamos concatenando su ID y nombre
                for(Hashtable<String, Object> auxProduct: auxProducts)
                    stringProducts += "[" + auxProduct.get("id") + "] " + auxProduct.get("name") + "\n";
               
                stringProducts += "\n";
               
                // Llamamos otra vez a esta funci�n
                product = selectProduct(stringProducts);
            } else if (input.equals("SALIR")) {
            	product = null;
            } else {
            	JOptionPane.showMessageDialog(null, "No se ha encontrado ning�n producto con ese nombre.");
                product = null;
            }
        }
       
        return product;
    }
   
    private static ArrayList<Hashtable<String, Object>> getProductsByName(String productName) {
        // Declaraci�n de variables
        ArrayList<Hashtable<String, Object>> products = new ArrayList<>();
       
        // Recorremos todos los productos en stock
        // Y a�adimos aquellos que contienen en su nombre el par�metro pasado
        for (Hashtable<String, Object> product: stockProducts)
            if (String.valueOf(product.get("name")).contains(productName))
                products.add(product);
       
        return products;
    }
 
    private static Hashtable<String, Object> getProductById(int productId) {       
        // Recorremos todos los productos en stock
        // Y cuando encontramos
        for (Hashtable<String, Object> auxProduct: stockProducts)
            if ((int) auxProduct.get("id") == productId)
                return auxProduct;
           
        return null;
    }
 
    // Comprueba si una string es num�rica
    private static boolean isNumeric(String string) {
        // Declaraci�n de variables
        boolean result;
 
        try {
            Integer.parseInt(string);
            result = true;
        } catch (NumberFormatException excepcion) {
            result = false;
        }
       
        return result;
    }
 
    private static void addProduct() {
        // Declaraci�n de variables
        Hashtable<String, Object> product = new Hashtable<>();
        String productName;
        double productPrice;
        int productIva;
        int initialProductStock;
        int addMoreProducts;
        int addProduct;
       
        // Pedir nombre
        productName = JOptionPane.showInputDialog("Nombre del producto:\n\n").trim().toUpperCase();
       
        // Comprobar si ese nombre ya existe
        if (!checkProductName(productName)) {
            product.put("name", productName);
           
            // Pedir precio
            productPrice = Double.parseDouble(JOptionPane.showInputDialog("Precio del producto:\n\n"));
            product.put("price", productPrice);
           
            // Pedir IVA
            productIva = Integer.parseInt(JOptionPane.showInputDialog("Tipo de IVA del producto:\n\n"));
            product.put("iva", productIva);
           
            // Pedir cantidad inicial en stock (sino 0)
            initialProductStock = Integer.parseInt(JOptionPane.showInputDialog("Cantidad inicial de stock:\n\n"));
            product.put("num", initialProductStock);
           
            // Autogeneramos una ID incremental para el producto
            product.put("id", nextProductId());
           
            // Imprimimos producto y preguntamos confirmaci�n
            addProduct = JOptionPane.showConfirmDialog(null, productToString(product) + "\n\nDesea a�adir el producto?", null, JOptionPane.YES_NO_OPTION);
           
            // A�adir producto a stock si hemos aceptado
            if (addProduct == 0)
                stockProducts.add(product);
            else
                maxProductId--;
       
        // En caso de que ya exista el nombre
        } else
            JOptionPane.showMessageDialog(null, "El nombre de ese producto ya existe.");
       
        // Preguntar si queremos a�adir otro producto
        addMoreProducts = JOptionPane.showConfirmDialog(null, "Desea a�adir m�s productos?", null, JOptionPane.YES_NO_OPTION);
       
        // En caso afirmativo, llamamos otra vez a la funci�n
        if (addMoreProducts == 0)
            addProduct();
       
    }
 
    // Convertimos un producto a string con nuestro formato
    private static String productToString(Hashtable<String, Object> product) {
        // Declaraci�n de variables
        String string = "";
       
        // Vamos concatenado todas las entradas de product en string
        string += "ID: " + product.get("id") + "\n";
        string += "Nombre: " + product.get("name") + "\n";
        string += "Precio: " + product.get("price") + "\n";
        string += "IVA: " + product.get("iva") + "\n";
        string += "Cantidad en stock: " + product.get("num") + "\n\n";
       
        return string;
    }
 
    // Convertimos un producto a string con nuestro formato
    private static String productToReducedString(Hashtable<String, Object> product) {       
        // Vamos concatenado todas las entradas de product en string
        String string = "[" + product.get("id") + "] " + product.get("name") 
        		+ " - " + product.get("price") + "� (" +product.get("iva") + "%) "
        		+ " x " +  product.get("num") + " u.\n";
       
        return string;
    }
 
    private static int nextProductId() {
        // Incrementamos
        maxProductId++;
       
        // Devolvemos la id ya incrementada
        return maxProductId;
    }
 
    // Comprobamos si el nombre pasado existe en los productos en stock
    private static boolean checkProductName(String productName) {
        // Recorremos todos los productos y comprobamos que el nombre sea exactamente igual
        for (Hashtable<String, Object> product: stockProducts)
            if (productName.equals(product.get("name")))
                return true;
       
        return false;
    }
 
    // Controla las opciones introducidas en el men� de la caja registradora
    private static void cashRegisterHandler(int option) {
       
        switch (option) {
            // NUEVA COMPRA
            case 1:
                newPurchase();
                break;
            // CONSULTAR �LTIMO TICKET
            case 2:
                checkLastTicket();
                break;
            // CONSULTAR DINERO
            case 3:
                checkCash();
                break;
            // ATRAS
            case 0:
                break;
            default:
                JOptionPane.showMessageDialog(null, "Opci�n no contemplada.");
        }
       
    }
    
    private static void checkCash() {
		JOptionPane.showMessageDialog(null, "Dinero total facturado: " + df2.format(totalCash) + "�");
	}


	private static void checkLastTicket() {
    	JOptionPane.showMessageDialog(null, purchaseListToString(purchaseListHistory.get(purchaseListHistory.size() - 1)));
	}


	// Lista de compra a ticket
	private static String purchaseListToString(ArrayList<Hashtable<String, Object>> purchaseList) {
		// Declaraci�n de variables
		String string = "";
		double sumBeforeTax = 0.0;
		double sumIVA = 0.0;
		int numProds = 0;
		
		// Recorremos toda la lista guardada anteriormente
		for(Hashtable<String, Object> product: purchaseList) {
			// Hacemos los sumatorios del precio bruto e IVA
			sumBeforeTax +=  (int) product.get("num") * (double) product.get("price");
			sumIVA += sumBeforeTax * (int) product.get("iva") / 100;
			numProds += (int) product.get("num");
			
			// Concatenamos el producto como string
			string += product.get("name") + " " + product.get("num") + " x " 
			+ df2.format((double) product.get("price")) + " (" 
			+ product.get("iva") + "%)\n"; 
		}
		
		// Concatenamos precios finales
		string += "_______________________\n\n"
				+ "N�mero de productos total: " + numProds + " productos\n"
				+ "Precio total bruto: " + df2.format(sumBeforeTax) + " euros\n"
				+ "Precio total IVA: " + df2.format(sumIVA) + " euros\n"
				+ "Precio TOTAL: " + df2.format(sumBeforeTax + sumIVA) + " euros";
		
		return string;
	}
	
	// Nueva compra
    private static void newPurchase() {
		// Declaraci�n de variables
    	ArrayList<Hashtable<String, Object>> purchaseList = fillPurchaseList(); // Llenamos la lista de compra;
    	String ticket;
    	double change;
    	double paid;
    	
    	// Pasamos la lista a formato ticket
    	ticket = purchaseListToString(purchaseList);
    
		// Imprimos el ticket
		// Pedir dinero a pagar
    	paid = Double.parseDouble(JOptionPane.showInputDialog(ticket + "\n�Cuanto desea pagar?"));
    	
    	// Mostrar cambio
    	change = calculateChange(paid, purchaseList);
		JOptionPane.showMessageDialog(null, "El cambio es de: " + df2.format(change) + " euros");
				
		// A�adimos el ticket al historial
		purchaseListHistory.add(purchaseList);
		
	}

	// Retorna el cambio dado una cantidad pagada
 	private static double calculateChange(double paid, ArrayList<Hashtable<String, Object>> purchaseList) {
 		// Declaraci�n de variables
 		double change;
 		double sumBeforeTax = 0.0;
 		double sumIVA = 0.0;
 		double total;
 		
 		// Recorremos los productos de la lista para poder tener un valor total
 		for(Hashtable<String, Object> product: purchaseList) {
 			sumBeforeTax +=  (int) product.get("num") * (double) product.get("price");
 			sumIVA += sumBeforeTax * (int) product.get("iva") / 100;
 		}
 			total = sumBeforeTax + sumIVA;
 		// Calculamos el cambio de retornar
 		change = paid - total;
 		
 		// A�adimos facturado al total
 		totalCash += total;
 		
 		return change;
 	}
 	
 	
	// Preguntamos las cantidades de los proudctos del carrito
	// Y los vamos a�adiendo a la lista final con sus cantidades
	private static ArrayList<Hashtable<String, Object>> fillPurchaseList() {
		// Declaraci�n de variables
		int amount = 0;
		int confirm = 1;
		Hashtable<String, Object> product;
		Hashtable<String, Object> auxProduct;
		ArrayList<Hashtable<String, Object>> purchaseList = new ArrayList<>();
		boolean correctStock;
		
		// Mientras la id seleccionada no corresponda a la opci�n de salir
		do {
			// Imprimir producto
			// Preguntar id a a�adir cantidad
			auxProduct = selectProduct(null);
			product = new Hashtable<>();
			
			// Comprobamos si hemos seleccionado un producto
			// Si no hemos seleccionado ning�n producto, nos pide para acabar la compra
			if (auxProduct == null)
				confirm = JOptionPane.showConfirmDialog(null, "�Quiere acabar su compra aqu�?", null, JOptionPane.YES_NO_OPTION);
			// Si hemos seleccionado el producto, operamos
			else {
				// Copiamos el producto
				product.put("id", auxProduct.get("id"));
				product.put("name", auxProduct.get("name"));
				product.put("price", auxProduct.get("price"));
				product.put("iva", auxProduct.get("iva"));
				
				
				// Preguntar cantidades
				do {				
					amount = Integer.parseInt(JOptionPane.showInputDialog("Cuantos '" + product.get("name") + "' quieres comprar?\n\n"
							+ "Si no quieres comprar pon 0\n\n"));
										
					// Comprobamos si la cantidad introducida supera el stock
					if (amount > (int) auxProduct.get("num")) {
						JOptionPane.showMessageDialog(null, "La cantidad introducida supera el n�mero de existencias.");
						correctStock = false;
					} else {
						correctStock = true;
						
						// A�adir producto y cantidades al producto auxiliar
						if (amount >= 1) {
							product.put("num", amount);
							
							// A�adimos el producto auxiliar con su cantidad a la lista
							purchaseList.add(product);
							
							// Recalculamos tock
							auxProduct.put("num", (int) auxProduct.get("num") - amount);
						}
					}
				} while(!correctStock);
				
			}
			
		} while (confirm != 0);
		
		return purchaseList;
	}

	// Imprime el men� principal
    private static int mainMenu() {
        return Integer.parseInt(JOptionPane.showInputDialog(""
                + "1) Gestionar almac�n\n"
                + "2) Operar m�quina registradora\n"
                + "\n"
                + "0) Salir\n\n"));
    }
 
    // Imprime el men� para la gesti�n de almac�n
    private static int warehouseMenu() {
        return Integer.parseInt(JOptionPane.showInputDialog(""
                + "1) Consultar producto\n"
                + "2) Consultar todos los productos\n"
                + "3) Crear producto\n"
                + "4) Modificar stock\n"
                + "5) Borrar producto\n"
                + "\n"
                + "0) Atr�s\n\n"));
    }
 
    // Imprime el men� para la caja registradora
    private static int cashRegisterMenu() {
        return Integer.parseInt(JOptionPane.showInputDialog(""
                + "1) Nueva compra\n"
                + "2) Consultar �ltimo ticket\n"
                + "3) Consultar dinero en caja\n"
                + "\n"
                + "0) Atr�s\n\n"));
    }
 
}