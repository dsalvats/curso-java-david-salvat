package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JFrame;

import models.Modelo;
import views.Vista;

public class Controlador implements ActionListener {
	
	/*************
	 * ATRIBUTOS *
	 *************/
	private Modelo modelo;
	private Vista vista;
	private double cantidad;
	
	/***************
	 * CONSTRUCTOR *
	 ***************/
	public Controlador(Modelo modelo, Vista vista) {
		this.modelo = modelo;
		this.vista = vista;
		this.vista.btnEurAUsd.addActionListener(this);
		this.vista.btnUsdAEur.addActionListener(this);
	}

	/***********
	 * M�TODOS *
	 ***********/
	public void iniciarVista() {
		vista.setTitle("Conversor");
		vista.setResizable(false);
		vista.setBounds(100, 100, 251, 138);
		vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		vista.setVisible(true);
	}
	
	/***********
	 * EVENTOS *
	 ***********/
	@Override
	public void actionPerformed(ActionEvent event) {
		// Si hemos apretado el boton EUR a USD
		if ( vista.btnEurAUsd == event.getSource()) {
			try {
				// Consigue la cantidad del textField
				cantidad = Double.parseDouble(vista.textResultado.getText());
				// Pasamos la cantidad a nuestro objeto modelo
				modelo.setCantidad(cantidad);
				// Llamamos a nuestra funci�n para convertir euros a dolares
				modelo.convertirEuroADolar();
				// Imprimimos el resultado redondeado a 2 decimales
				vista.lblResultado.setText(new DecimalFormat("#.##").format(modelo.getResultado()));
			} catch (Exception e) {
				vista.lblResultado.setText("Introduzca una cantidad v�lida...");
			}
		// Si hemos apretado el bot�n EUR a  USD
		} else if (vista.btnUsdAEur == event.getSource()) {
			try {
				// Consigue la cantidad del textField
				cantidad = Double.parseDouble(vista.textResultado.getText());
				// Pasamos la cantidad a nuestro objeto modelo
				modelo.setCantidad(cantidad);
				// Llamamos a nuestra fucni�n para  converir dolares a euros
				modelo.convertirDolarAEuro();
				// Imprimimos el resultadol redondeado a 2 decimales
				vista.lblResultado.setText(new DecimalFormat("#.##").format(modelo.getResultado()));
			} catch (Exception e) {
				vista.lblResultado.setText("Introduzca una cantidad v�lida...");
			}
		}
	}

}
