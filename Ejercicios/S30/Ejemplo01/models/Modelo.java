package models;

public class Modelo {
	
	/**************
	 * CONSTANTES *
	 **************/
	private final static double CONV_EUR_USD = 0.896361;

	/*************
	 * ATRIBUTOS *
	 *************/
	private double cantidad;
	private double resultado;

	/***********
	 * M�TODOS *
	 ***********/
	public void convertirDolarAEuro() {
		resultado = cantidad * (1 / CONV_EUR_USD);
	}
	
	public void convertirEuroADolar() {
		resultado = cantidad * CONV_EUR_USD;
	}
	
	/*********************
	 * GETTERS & SETTERS *
	 *********************/

	/**
	 * @return the cantidad
	 */
	public double getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @return the resultado
	 */
	public double getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(double resultado) {
		this.resultado = resultado;
	}
	
}
