/**
 * 
 */
package AppMain;

import java.awt.EventQueue;

import controllers.Controlador;
import models.Modelo;
import views.Vista;

/**
 * @author David Salvat
 *
 */
public class ConversorApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Modelo modelo = new Modelo();
					Vista vista = new Vista();
					Controlador controlador = new Controlador(modelo, vista);
					controlador.iniciarVista();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
