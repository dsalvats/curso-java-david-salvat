package views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class Vista extends JFrame {

	/*************
	 * ATRIBUTOS *
	 *************/
	private static final long serialVersionUID = -4510625221147619851L;
	private JPanel contentPane;
	public JTextField textResultado;
	public JLabel lblResultado;
	public JButton btnEurAUsd; 
	public JButton btnUsdAEur;
	
	/***************
	 * CONSTRUCTOR *
	 ***************/
	public Vista() {
		// A�adimos panel donde pondremos todos los elementos de la aplicaci�n
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// A�adimos textField donde introduciremos la cantidad a convertir
		textResultado = new JTextField();
		textResultado.setBounds(10, 11, 225, 20);
		contentPane.add(textResultado);
		textResultado.setColumns(10);
		
		// A�adismo el label donde imprimiremos el resultado
		lblResultado = new JLabel("Resultado:");
		lblResultado.setBounds(10, 42, 225, 14);
		contentPane.add(lblResultado);
		
		// A�adimos el bot�n para convertir de euros a dolares
		btnEurAUsd = new JButton("EUR a USD");
		btnEurAUsd.setBounds(10, 67, 106, 23);
		contentPane.add(btnEurAUsd);
		
		// A�adimos el bot�n para convertir de dolares a euros
		btnUsdAEur = new JButton("USD a EUR");
		btnUsdAEur.setBounds(126, 67, 109, 23);
		contentPane.add(btnUsdAEur);
	}
	
	
	
}
