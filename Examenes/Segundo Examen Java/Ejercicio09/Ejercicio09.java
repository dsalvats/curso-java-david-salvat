package Ejercicio09;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejercicio09 extends JFrame {

	private JPanel contentPane;
	private ControladorCartas controlador;
	private ArrayList<String> colores = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			// Corremos aplicaci�n
			public void run() {
				try {
					// Invocamos y creamos ventana
					Ejercicio09 frame = new Ejercicio09();
					// Hacemos visible la ventana
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio09() {	
		// TODO: Implementar
		colores.add("BLACK");
		colores.add("Verde");
		Collections.shuffle(colores);
		
		// Creamos objeto controlador de cartas
		controlador = new ControladorCartas();
		
		// Creamos ventana
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 247, 269);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Creamos set de cartas
		JToggleButton carta1 = new JToggleButton("");
		carta1.setSelected(true);
		carta1.setDoubleBuffered(true);
		carta1.setBackground(Color.RED);
		carta1.setBounds(0, 0, 60, 60);
		contentPane.add(carta1);
		
		JToggleButton carta5 = new JToggleButton("");
		carta5.setSelected(true);
		carta5.setDoubleBuffered(true);
		carta5.setBackground(Color.ORANGE);
		carta5.setBounds(0, 57, 60, 60);
		contentPane.add(carta5);
		
		JToggleButton carta9 = new JToggleButton("");
		carta9.setSelected(true);
		carta9.setDoubleBuffered(true);
		carta9.setBackground(Color.GREEN);
		carta9.setBounds(0, 114, 60, 60);
		contentPane.add(carta9);
		
		JToggleButton carta13 = new JToggleButton("");
		carta13.setSelected(true);
		carta13.setDoubleBuffered(true);
		carta13.setBackground(Color.CYAN);
		carta13.setBounds(0, 171, 60, 60);
		contentPane.add(carta13);
		
		JToggleButton carta2 = new JToggleButton("");
		carta2.setSelected(true);
		carta2.setDoubleBuffered(true);
		carta2.setBackground(Color.BLACK);
		carta2.setBounds(57, 0, 60, 60);
		contentPane.add(carta2);
		
		JToggleButton carta6 = new JToggleButton("");
		carta6.setSelected(true);
		carta6.setDoubleBuffered(true);
		carta6.setBackground(Color.MAGENTA);
		carta6.setBounds(57, 57, 60, 60);
		contentPane.add(carta6);
		
		JToggleButton carta10 = new JToggleButton("");
		carta10.setSelected(true);
		carta10.setDoubleBuffered(true);
		carta10.setBackground(Color.CYAN);
		carta10.setBounds(57, 114, 60, 60);
		contentPane.add(carta10);
		
		JToggleButton carta14 = new JToggleButton("");
		carta14.setSelected(true);
		carta14.setDoubleBuffered(true);
		carta14.setBackground(new Color(128, 0, 0));
		carta14.setBounds(57, 171, 60, 60);
		contentPane.add(carta14);
		
		JToggleButton carta3 = new JToggleButton("");
		carta3.setSelected(true);
		carta3.setDoubleBuffered(true);
		carta3.setBackground(Color.RED);
		carta3.setBounds(114, 0, 60, 60);
		contentPane.add(carta3);
		
		JToggleButton carta4 = new JToggleButton("");
		carta4.setSelected(true);
		carta4.setDoubleBuffered(true);
		carta4.setBackground(Color.BLACK);
		carta4.setBounds(171, 0, 60, 60);
		contentPane.add(carta4);
		
		JToggleButton carta7 = new JToggleButton("");
		carta7.setSelected(true);
		carta7.setDoubleBuffered(true);
		carta7.setBackground(Color.ORANGE);
		carta7.setBounds(114, 57, 60, 60);
		contentPane.add(carta7);
		
		JToggleButton carta8 = new JToggleButton("");
		carta8.setSelected(true);
		carta8.setDoubleBuffered(true);
		carta8.setBackground(Color.MAGENTA);
		carta8.setBounds(171, 57, 60, 60);
		contentPane.add(carta8);
		
		JToggleButton carta11 = new JToggleButton("");
		carta11.setSelected(true);
		carta11.setDoubleBuffered(true);
		carta11.setBackground(Color.BLUE);
		carta11.setBounds(114, 114, 60, 60);
		contentPane.add(carta11);
		
		JToggleButton carta12 = new JToggleButton("");
		carta12.setSelected(true);
		carta12.setDoubleBuffered(true);
		carta12.setBackground(new Color(128, 0, 0));
		carta12.setBounds(171, 114, 60, 60);
		contentPane.add(carta12);
		
		JToggleButton carta15 = new JToggleButton("");
		carta15.setSelected(true);
		carta15.setDoubleBuffered(true);
		carta15.setBackground(Color.GREEN);
		carta15.setBounds(114, 171, 60, 60);
		contentPane.add(carta15);
		
		JToggleButton carta16 = new JToggleButton("");
		carta16.setSelected(true);
		carta16.setDoubleBuffered(true);
		carta16.setBackground(Color.BLUE);
		carta16.setBounds(171, 171, 60, 60);
		contentPane.add(carta16);
		
		// A�adimos listeners a las cartas y las controlar�
		// nuestro objeto controlador de cartas
		carta1.addActionListener(controlador);
		carta2.addActionListener(controlador);
		carta3.addActionListener(controlador);
		carta4.addActionListener(controlador);
		carta5.addActionListener(controlador);
		carta6.addActionListener(controlador);
		carta7.addActionListener(controlador);
		carta8.addActionListener(controlador);
		carta9.addActionListener(controlador);
		carta10.addActionListener(controlador);
		carta11.addActionListener(controlador);
		carta12.addActionListener(controlador);
		carta13.addActionListener(controlador);
		carta14.addActionListener(controlador);
		carta15.addActionListener(controlador);
		carta16.addActionListener(controlador);
	}
}
