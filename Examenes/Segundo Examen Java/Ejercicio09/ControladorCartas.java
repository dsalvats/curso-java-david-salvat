package Ejercicio09;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

public class ControladorCartas implements ActionListener {

	// Declaramos variables globales
	private int clickedButtons;
	private int correctCount;
	private JToggleButton[] cards = { null, null };
	
	public ControladorCartas() {
		clickedButtons = 0;
		correctCount = 0;
	}
	
	@Override
	public void actionPerformed(ActionEvent event) {	
		// Obtenemos la carta clickada
		JToggleButton card = (JToggleButton) event.getSource();
		
		// A�adimos un click para controlar acciones posteriores
		clickedButtons++;	
		
		// Si tenemos un click solo, a�adimos la carta a la posici�n 0 de nuestro array de cartas a comparar
		if (clickedButtons == 1) {
			cards[0] = card;
		// Si tenemos dos clicks, a�adimos la carta a la posici�n 1 de nuestro array de cartas a comparar
		} else if (clickedButtons == 2) {
			
			cards[1] = card;

			// Si las cartas son iguales y llevamos 7 aciertos quiere decir que hemos ganado
			// Imprimimos mensaje de enhorabuena y cerramos aplicaci�n
			if (cards[0].getBackground().equals(cards[1].getBackground()) && correctCount == 7) {
				JOptionPane.showMessageDialog(null, "Enhorabuena has ganado!");
				Runtime.getRuntime().exit(1);
			}
		// Si tenemos tres clicks comparamos cartas
		} else if (clickedButtons == 3){
			
			// Si los colores de las cartas son iguales
			if (cards[0].getBackground().equals(cards[1].getBackground())) {
				// A�adimos acierto
				correctCount++;
				
				// Quitamos las cartas del juego
				cards[0].setVisible(false);
				cards[1].setVisible(false);
				
			// Si los colores de las cartas son diferentes
			} else {
				// Giramos las cartas
				cards[0].setSelected(true);
				cards[1].setSelected(true);
			}
			
			// A�adimos la nueva carta donde hemos apretado
			cards[0] = card;
			cards[1] = null;

			// A�adimos el click que acabamos de hacer
			clickedButtons = 1;
		}
			
	}

}
